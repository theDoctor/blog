---
title: "First Impression of the Steam Deck"
date: 2022-09-27T11:16:29+02:00
ShowToc: true
TocOpen: false
tags: ["Gaming"]
aliases:
  - "/posts/gaming/steamdeck"

---

I received my Steam Deck two weeks ago after a wait of roughly half a year.
Thus, I wanted to write a blog entry to jot down my first impressions of the
device.

If you don't know anything about it yet: the Steam Deck is a PC running a fork
of Arch Linux with a read-only file system in the form factor of a handheld
console. It's tightly integrated with the Steam platform and offers a gaming
mode in which you can seamlessly buy, install, manage and play games from your
library. Furthermore, the Deck offers to possibility to enter desktop mode which
boots up a regular KDE Plasma desktop which you can then use as you would a
regular PC. Since the file system is read-only, package management works via
Flatpaks. You can circumvent this but have to do so at your own peril.

# Background 

First some background. I've been a gamer since my early childhood and have
almost exclusively been using a Windows PC for playing games. I've only ever
owned one console, namely the original XBox, which I bought used many years
after its release to get to play some console-exclusive titles.

I started using Linux some four years ago and immediately took a liking to it. I
won't elaborate here but I made to switch to Linux only a while ago which
includes my gaming. This was relatively painless because of a few things that
work in my favor:

- I only play single-player games
- I tend not to play the latest AAA titles 
- I like older games and indie titles
- I tend to replay games I already own

As a result my gaming habits lend themselves fairly well to Linux gaming, at
least in my experience.

# Why

My reasoning for buying a Steam Deck was the following: my current gaming device
is an aging desktop PC (see [here]({{< ref "uses.md" >}})) which is
struggling more and more. Since it's over 7 years old now, an upgrade of sorts
seemed necessary. Furthermore, my gaming habits have changed. My favorite genre
is RPGs which usually require a lot of time. Time that I simply do not have
anymore, at least not in one sitting. Also, my desktop PC sits in my bedroom
which is increasingly occupied by someone taking a nap, making it impossible for
me to use it for gaming. For a time I circumvented this by streaming games via
Steam to a laptop in my living room but this solution is not quite ideal. In
total, the Steam Deck seemed like a good fit for my needs so I ordered one a
week after its release.

# Getting Started

I didn't record an unboxing video because they're pointless. Seriously. The
device comes with a case, a microfiber cloth for cleaning the display and a
charging cable. The Steam Deck comes in three variations which differ (mostly) in their
storage size. I bought the 512 GB variant which is the most expensive one at ca.
675 €. This also comes with an antiglare screen, although I don't know what
difference that makes. I mostly chose this one because I didn't want to go and
buy an SD card right away but install my stuff on the internal hard drive. 

When I first fired the Deck up I was prompted to log in to Steam. Since my
password is some 20 characters of random stuff, this is tedious to enter
manually. So I tried to enter desktop mode right away, install the Bitwarden
desktop client via Flatpak, log in to my instance and get the password from
there. Long story short: this did not work as I intended. The desktop mode
definitely feels rough around the edges and presents users such as myself with
certain roadblocks but more on that later.

Thus I decided to bite the bullet and enter my password in gaming mode via the
on-screen keyboard built in to the Deck and whereas this is definitely annoying,
it works.

Side note: the Deck supports Bluetooth so you can, of course, just connect it to
a Bluetooth mouse and keyboard to circumvent such annoyances. I don't own a
Bluetooth mouse so there's that. Also, sometimes you just want to quickly do
something without having to sit down at your desk and connect a bunch of
peripherals.

# Gaming Mode

Gaming mode is basically something like Steam Big Picture Mode which is meant to
be navigated with a controller. After the first release of the Deck the
reviewers agreed that it was an amazing device but still unfinished (e.g.
[here](
https://youtu.be/kXIOuUUZO2s)). There were certain complaints about navigating
the UI, performance with large libraries and so on and so forth. 

From my point of view, the experience has been smooth so far. I like the design,
it looks very modern and works intuitively, I think. I installed a bunch of my
games, let them download, fired them up, all worked fine. Some of my games run
natively, some via Proton but the difference wasn't even noticeable in the
interface. I wouldn't have realized, if I hadn't known what to look for.

There are a couple of UI bugs that I came across though. For example there's an
issue when looking at game reviews in the store where the cursor is taken back
all the way to the top after looking at a review in detail instead of just
continuing on. This and one or two other, fairly minor issues were things I
encountered myself.

So how about the actual games?

# Game Ratings

At this point the work Valve have done with the rating of games becomes
apparent. All games you view have a badge which tells you whether this
particular game has been tested on the Steam Deck and if yes, how well it
works. If it isn't straight up verified (which means no issues at all), you get
an explanation of the reasoning behind it. For example, if a third-party
launcher is necessary, entering of text via on-screen keyboard, if the UI is a
bit small or something of the sort, the game is rated "playable" and the reason
why is listed. This is quite useful for deciding which games to buy and install.
I'd also recommend having a look at [ProtonDB](https://protondb.com) where
people give reports on game performance on Linux in general and Steam Deck in
particular with more detailed information.

With that said, there have been reports of games that are listed as "unplayable"
but work fine ("Batman: Arkham Knight" comes to mind. Tested it myself, didn't
have issues) or games rated as "verified" but actually have issues. I read that
"Horizon: Zero Dawn" works but has issues later in the game when the areas
become larger and more demanding on the hardware. Other times people report
small in-game text although the game is also listed as "verified" or that
there's a substantial amount of tinkering necessary to get the game to perform
well enough. That sort of thing. It doesn't hurt to do your own research and
ProtonDB is your friend here.

# Controls

Before (or during) playing anything you get to choose the control scheme. The
Steam Deck offers plenty of input options. The base layout is similar to an XBox
controller but in addition it features two trackpads and four grip buttons (not
sure of that's what they're officially called). For every game there's an
officially recommended layout (if available) that you can choose and/or modify
as you see fit. There's also a bunch of presets to build on. For example, I
like to map two of the grip buttons to pressing the joysticks because in hectic
situations in game pressing the joystick is awkward for me.
Additionally, there are community layouts you can look up from within the Gaming
Mode and download with the click of a button. This is highly useful, especially
for games that are unrated, or don't or only partially support controller input.

# Steam Gaming

The first game I wanted to play was "Jedi: Fallen Order". I've owned this for a
while and already played it but my desktop PC isn't really up to the task. I had
to manually edit config files to lower the graphics settings in order to get to
playable frame rates. The Steam Deck is capable of running it all full details
and highest settings which I love because the game is gorgeous. With that said,
it should be noted that it also sucks a lot of battery and I limited it to 30
frames for that reason and for consistency. 

And with that we come to another feature that is important for the Deck. It
comes with a context menu you can pull up any time, even within games, to
quickly change some settings. You can activate/deactivate WiFi or Bluetooth,
that sort of thing, but most importantly, there is a performance submenu where
you can do a couple of things.

First of all it lets you add an overlay that measures battery consumption, CPU
and GPU temperatures, CPU and GPU clock speeds, frame rate and that kind of
thing. There are then a few settings that you can change to balance out
performance and battery usage to match what fits your needs best:

- Limit frame rates to a whole number fraction of the screen refresh rate 
- Change the screen refresh rate
- Limit processor power
- Manually fix GPU clock
- Apply a shading filter (including FSR)

The first point off that list is probably the most-used setting. Like I
mentioned above, I capped the frame rate of the game at 30 (the default is 60)
so the frame rate is more consistent throughout the game. It also conserves
power so I can get about two hours of battery out of it with this game while not
negatively affecting my gaming experience. I'm not a sucker for astronomical
frame rates but your mileage may vary.

FSR is also useful but I found that it only makes a noticeable difference if the
game offers a resolution that's lower than 720p and conserves the aspect ratio.
Most of the games where I bothered to look don't do this. Maybe it would have
been possible to somehow manually change the resolution to 540p so FSR makes
more of a difference but that's a level of tinkering I couldn't be bothered
with.

Conveniently, you can change these settings on a per-game basis so you can set
it once for every game and forget about it afterwards. However, it's not yet
possible to download community settings for a specific game in order to save
yourself the time and effort to figure out the optimal settings yourself. I
think that's understandable, though, because the optimal settings are not
necessarily the same for everyone. Often, you'll want a balance between
performance and battery life. Some AAA titles can be played at max settings and
60 FPS but will give you a battery life of below one hour. If you tweak the
settings, you can extend that to two or maybe three hours, depending on your
needs and willingness to compromise. Indie games or generally less demanding
games will give you more play time still, some 4-6 hours maybe, depending.

# Non-Steam Gaming

So far, so great. As I mentioned before, the experience of Gaming Mode is pretty
seamless and polished (at least I think so). However, the desktop mode
experience is less so. One might wonder why this is important but this naturally
comes into play once you want to install and play a game that isn't on Steam.
In my case, I have a bunch of games I bought on GOG that I might want to
install. Also, I read about emulation on the Deck and wanted to try a few games
I haven't been able to play so far because I don't have the console for them.

The workflow is then as follows: you need to enter desktop mode, somehow install
your game, optionally add it to the Steam client as a non-Steam game so it's
accessible in Gaming Mode and then play it. Whereas this sounds simple, it's not
necessarily so.

Although the most obvious choice for installing GOG games (Lutris) is now
available as a Flatpak, it's mostly the little things that make the experience a
bit cumbersome.

- Navigating the cursor with he trackpad takes a bit getting used to
- The on-screen Steam keyboard doesn't work well in desktop mode (This was an
  issue initally but was fixed in the meantime)
- Installed games may not launch (possibly because it was from a Flatpak? Who
  knows?)
- No Artwork for installed non-Steam game (How do I even add one?)
- Performance of GameCube emulation with Dolphin is terrible

These are some things I noticed myself, I'm sure there are many more. What I'm
really trying to say is: using the desktop mode to achieve anything related to
gaming usually requires a bit of tinkering. I know, this is still a Linux
machine so tinkering is basically baked-in, no? Well, yes and no. 

I mean, I'm just as happy to tinker with my stuff as the next person but this is
a handheld gaming console and I actually just want to get stuff installed and
play it. I also want it to look nice and work well. I know how to get some stuff
done in Linux but I don't have the time and energy to fix all the little (and
not so little) issues that crop up when trying to get the software to do what I
want. 

An example: I own "The Witcher 3" on GOG and I didn't want to buy the Steam
version just because of the convenience. I downloaded Lutris as a Flatpak and
installed it. No issues so far. I tried to launch the game in desktop mode or
Gaming Mode (after adding it to the client) but it just wouldn't start. Weird.
The game has a platinum rating on ProtonDB and I never had any issues with it in
the slightest. I couldn't figure out what was wrong. Then I tried the Heroic
launcher. It installed and started then but never proceeded past the first
loading screen, no matter what I did. Even more weird. Eventually, I ended up
adding the executable (that came from the Lutris installation) to Steam manually
by browsing the wine prefix and choosing the proper .exe file. After this the
game started and loaded fine. No idea why. However, the Artwork was missing of
course. I went to [SteamgridDB](https://www.steamgriddb.com/) to find some,
found a guide online that instructed me on how to accomplish it: you need to
isntall SGDBoop as a Flatpak, activate it in your browser, choose an Artwork and
press a button. Sounded simple, didn't work.
I went to the troubeshooting section and I tried another browser, restarted
everything a bunch of times. Finally, I had to restart `xdg-desktop-portal` via
a `systemctl` command in the command line to finally make it work. And even
after all that there's one artwork (of the several that Steam uses) missing in
Gaming Mode. Transferring my mods and savegames was also a hassle because you
have to identify the correct wine prefix in the Steam folder and insert the
files in the correct spot but since I know how and where to do that it was
relatively easy.

All of this was necessary to get to (almost) the same spot I could have gotten
to by just buying the game on Steam and installing it through there. And of
course I don't have access to the Steam features for this title, namely
achievements (for which I care nothing but others do) and, more importantly,
custom control layouts and the like. Not a big issue for this one title since it
has controller support out of the box but for other games this would be
inconvenient. Maybe there's a way to deal with this but it would be yet another
step of tinkering.

The bottom line with this is basically the following: Steam games work with very
little friction if any, non-Steam games not so much. It depends on the game of
course but in some cases you have to put in quite a bit of work. Another
takeaway, however, is this: there's loads of community tools and tips to help
you out. Considering that the Steam Deck is still pretty new, there's already a
vibrant community around it with dedicated tools for adding non-Steam games to
Steam in an automated fashion, adding or changing artworks, community plugins
(haven't tried those) and more. This is something I really like and which, I
think, is reminiscent of the best part of the Linux community.

# Emulation

Next up, I tried to get some emulation going. Mostly, I wanted to play some
games that I never could because I don't own the console for it. I didn't try a
whole lot in this regard but first went looking for a one-click solution that
works without too much work on my part. I decided on
[EmuDeck](https://www.emudeck.com/) which is basically a shell script that
installs all the different emulators in existence and configures everything
automatically. You then add the ROMs to the appropriate newly created folders,
run Steam ROM manager (which is also installed) which parses your ROMs, looks up
artworks for them and adds everything to your Steam library so you can
conveniently run all the games from within Gaming Mode. And yes, I know running
random scripts from the web is a bad idea. Sue me.

In principle, it all worked fine. I installed a few N64 and GameCube titles and
fired them up just to see if everything was okay. The N64 titles displayed some
weird border artwork instead of just filling the remaining space that doesn't
fit with the aspect ratio with black. This annoys me.
The GameCube titles basically don't work at all or the performance is terrible.
Apparently, this is more pronounced for some of the games than for others but in
all cases I noticed severe dips in frame rates or even crashes which render the
games unplayable. I have no idea why that is. I don't know much about emulation,
maybe it's an issue than can be dealt with using the right settings or it's an
issue with the ROM files. I was hoping I wouldn't have to troubleshoot myself, I
wanted everything to just work out of the box, how hard can that be?

It's probably too much to ask. Unfortunately, I have no idea how to improve on
this and I plan to just ask people online since it seems likely that someone
else already had and solved these issues. Still, it annoys me that I have to
jump through additional hoops when my hope was to just click-and-play this
stuff. Perhaps that was unrealistic.

# Wrapping Up

So what's the bottom line here? Of course this is only my personal opinion so
make of it what you will. I think the Deck is an impressive device. It's
powerful, yet portable device with an extensive catalog of available games.
Technically, you can play anything that will run on a Linux PC, which is quite a
lot nowadays. As explained above, a few compromises may have to be made and your
willingness to tinker or lack thereof may also limit the selection of games you
can play but that doesn't detract much from the fact that the thing is still
pretty awesome. As far as I know, a few thousand games are playable on the Deck
which is more than for any other console (I think).

Add to that the fact that you can do emulation and play actual console games and
you have a great gaming device, indeed. What's more, since it's actually a fully
functioning desktop PC, you could use it for productivity tasks, install
whatever you want on it (even Windows) and basically do whatever with it,
regardless of its intended purpose. This open nature appeals to me greatly, but
I'm a Linux person after all so this seems hardly surprising.

Last but not least, the price is quite decent. I paid 675 € for the premium
model but I could also have gotten the 400 € one with the caveat that I would
have had to get an SD card immediately because the 64 GB of disk space is
basically nothing for games. I don't think that any other device can compete
with Valve here in terms of value for this money. 

As stated in the beginning, I had specific reasons for getting the Deck, some of
which come from my current situation. In the 2-3 weeks since it arrived I have
been gaming much more than I otherwise would have (probably close to zero
actually) and it fulfilled my expectations in this regard beautifully. I can
walk around my apartment or sit on the couch with the Deck in hand and play. I
didn't think I'd enjoy this so much but, in fact, I do.

So if you're like me, I highly recommend this device, it's pretty sweet. If not,
do your own research and come to your own conclusions. I still recommend it,
though.
