---
title: "Redoing my CV and Cover Letter for Job Applications"
date: 2022-05-04T13:38:39+02:00
tags: ["Programming", "Personal"]
aliases:
  - "/posts/coding/cv"
---

I have recently decided to quit my current job and pursue a career as a
developer. This decision has been in the making for a while and only matured a
short time ago, still I feel like it's the right thing for me to do.

Obviously, I'll need to apply for jobs and for that I need to get the paperwork
in order. I need to update my CV to make sense when applying for coding jobs (as
opposed to scientific positions in academia) and I need a cover letter for every
job I'm applying to although a basic template that I can edit as the need arises
should be sufficient.

Of course I already had a CV and creating a formal letter doesn't strike me as
particularly challenging. In the past I've used LaTeX for this kind of
undertaking as it's a typesetting system that's well suited for it. Right.

At this point I should probably confess that I'm not used to creating LaTeX
documents from scratch, much less writing templates, classes, styles and what
have you. I mostly scour the web for templates I like and then adapt them to my
needs.

This worked decently well for me in the past and this is what I did. However, I
hit a few roadblocks. I had downloaded a couple of templates for CV and cover
letter each and inserted my text, shifted things around a bit only to realize
that the layout doesn't match very well. Of course this is to be expected when
you use two completely unrelated templates but I was worried this would look
unprofessional. I'm still convinced it would. There was also the issue that some
templates would require PdfLaTeX, others XeLaTex (whatever that even is),
(re)positioning things is not straightforward, that kind of thing.

I couldn't have that so I decided to make some changes to the templates I had.
The most noticeable difference between the documents I had at this point was the
typography. Font, font size, this kind of thing. No matter (or so I thought), 
just change it.

After a while searching online for solutions I realized that it is apparently
not at all trivial to define a custom font for a LaTeX document. Sure, you can
choose between the built-in serif or sans-serif font but it just so happened
that I wanted a custom one and I couldn't get that to work.

A few hours and much frustration later I decided to rethink my approach. After
all, there's loads of tools for this kind of job. Some dedicated office suite
like LibreOffice would of course work, some people apparently use markdown,
squish that through some LaTeX template and convert everything with pandoc, you
get the idea. 

A brief exchange on Mastodon gave me the idea to use HTML and CSS and convert
the result to PDF. At first this seemed like a strange idea, I mean these are
technologies for websites, right? On the other hand, it's just a markup language
enhanced by styling and positioning which is pretty much exactly what I wanted
in the first place. Sure, my HTML/CSS skills aren't great (yet) but I felt
reasonably confident that I could do it. Also, I had existing templates that I
could recreate, I didn't have to reinvent the wheel from scratch.

So that's what I did. The biggest issue I came across is that HTML is not really
suited to pagination. CSS has the `@page` rule which you can use to define page
measurements and in principle it can also be used to deal with page breaks but I
couldn't get that to work well. But since I only had a single page break I
decided to go the hacky route and do it manually. I stole someone else's idea to
just use an empty `div` with a `page-break` class for which I define a page
break and padding in the style sheet to emulate an actual page break.
At some point I may figure out how to do it more elegantly, hopefully.

I also decided to appropriate the font Kev Quirk uses on his personal
[homepage](https://kevq.uk) because it's hands-down the most beautiful
typography I've ever consciously perceived and I wanted some of that in my
documents. Yeah, I like pretty, shiny things, sue me. Fortunately, downloading
and installing the font, adding it to a style sheet and applying it to my HTML
document is pretty straightforward. There you go LaTeX, that's how it's done.

Next thing I had to wrestle with was displaying elements next to each other
horizontally. Luckily, there's flexbox nowadays which provides a flexible
framework for this kind of task. Not gonna lie, I copied a lot of stuff from the
web and messed with keywords until it looked like I wanted it to but at least it
worked and I'm happy with the results (at least visually). Also, at least on one
occasion, I made a conscious choice to set a CSS property in order to align the
bottom of three elements arranged in columns in a flexbox. I felt smart after I
got that to look like I wanted.

At the end I even got fancy and embedded Fontawesome icons to add a playful note
to my personal information, address, phone number, email and so on. Naturally, I
didn't create an account or anything, I just downloaded the whole folder and
included the appropriate `all.css` as a style sheet in my document. Done.

The final thing left to do was converting the HTML document to something I could
actually use in an application, i.e. PDF. I wanted something simple that could 
also make use of my CSS and I found `wkhtmltopdf` for this. A simple command
does all I need:

```
wkhtmltopdf --enable-local-file-access input.html output.pdf
```

The `--enable-local-file-access` is necessary to allow access to the CSS, JS and
font files I embedded.

I still have much to learn but I enjoy the fact that I was able to create these
two documents in HTML and CSS from scratch (with help from random internet
strangers of course) so they now match and even look good, at least to me. I
count that as a win. Now I only have to actually use these to apply to a job and
get hired. Easy.

P.S. If I get hired, I'm gonna go ahead and assume that it was because of the
beautiful typography. In that case I may need to buy Kev a cup of coffee...
