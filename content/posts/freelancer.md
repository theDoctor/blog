---
title: "How To: Run Freelancer on Linux"
date: 2023-11-08T13:19:56+01:00
tags: ["How To", "Gaming", "Linux"]
aliases:
  - "/posts/gaming/freelancer"
---

Here's another piece of gaming nostalgia: Freelancer. It's celebrating its 20th birthday this year
which seems like a good opportunity to me to get it up and running again. 

### Prerequisites

Although this can probably achieved in any number of ways here is what I used:

* Lutris (flatpak or native), the version used here was `0.5.14` from Flathub
* Freelancer install script for Lutris (the following is a customized version 
  of [this](https://lutris.net/games/install/11013/view) which contains various fixes including a no-CD fix). 
  This script contains a custom return code after the initial installation. This 
  is necessary because this step does not exit cleanly resulting in the Lutris installation crashing.
{{< includecode desc="Expand to see script" path="assets/install_scripts/freelancer.json" lang="json">}}
    
* A copy of the game (CD-ROM or image thereof)

That should do it. 

**Note**: I tried the installation with Bottles first but when trying to run the setup executable nothing happens.
I tried various wine runners but eventually had to give up and turned to Lutris.

### Installation

* Start Lutris and add a new game
* Choose "Install from a local install script"
* Feed your install script (see above) to the installer
* (Optional) Mount the CD-ROM image, if you have one
  * Fedora has the necessary utilities preinstalled. You can just choose "Open with Disk Image Mounter" from the context menu of the `.iso` file.
* If it's not detected automatically, choose the location of the `setup.exe` file in the root directory of your CD/image
* Choose "Express Installation"
* After the installation close the launcher manually
* Proceed with the installation dialogs of the various things popping up
* (Optional) Install the HD mod from [here](https://github.com/BC46/freelancer-hd-edition/releases) (simply run
  as executable inside the prefix). This
  includes nicer visuals, fixes for widescreens and a few other things you can choose during installation.

  **Note**: If you do install this, you have to change the executable of the game as the mod installs into
  its own separate location.

This should set you up for a nice trip down memory lane. You can of course choose one of the other available installation
scripts from Lutris and install additional mods. I understand that there are quite a few out there. I, however, quite
like the vanilla game and also don't care about multiplayer but YMMV, as always.

Happy space exploring!

