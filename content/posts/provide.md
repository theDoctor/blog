---
title: "How To: Find Which Package Provides a Given File"
date: 2022-11-02T16:40:15+01:00
tags: ["How To", "Linux"]
aliases:
  - "/posts/howto/provide"
---

Don't you just hate it when you're in your terminal and want to run some
commands, only to discover that you don't have the appropriate package installed
(yet)? Maybe you recently reinstalled your machine and haven't fully set it up
yet or there is a mismatch between your personal and work machine. 

Whatever the case, you quickly want to install the package and get on with it.
But what if the name of the command isn't the name of the package? In this case
you need to query the repos. What's the command for doing that again? I keep
forgetting it. 

I know it's in the docs and you can find it out by calling the `--help` option
but I don't wanna dig through all that every time. So once and for all:

`dnf provides [value]`

If you're not on Fedora/CentOS/Red Hat, well go figure it out yourself!
