---
title: "Second Impression of the Steam Deck"
ShowToc: true
TocOpen: false
tags: ["Gaming"]
date: 2023-01-03T20:54:09+01:00
aliases:
  - "/posts/gaming/steamdeck2"
---

It's been a few months so I thought an update on my [last post on the Steam
Deck]({{< ref "steamdeck.md" >}}) is in order. So let's
get to it, shall we?

# The Good

First off, the things I liked about the Deck the first time around are still
valid. I still think the experience of gaming mode is very cohesive, the option
to choose and modify control schemes is extremely handy, I can run AAA games at
quite decent frame rates and settings which makes for a beautiful experience on
an 800p display.

Some people complain about battery life and I think that's fair. Depending on
what you play, you can end up with 1-2 hours of it which isn't a lot. Normally
though, I'm never far from a wall socket so it's not an issue for me.

# The Better

So what's new? You might recall that I had a few issues with the Deck, namely
the integration of third party games and some of the tooling. Also, emulation
didn't really click with me for a couple of reasons.

On both fronts, I'm happy to say that improvements have been made or the issue
completely resolved.

## Emulation

I kept reading about what I did wrong last time and learned that I had
apparently been pretty unlucky in my choice of game. It turns out that the one
or two games that I tried playing were also ones that others reported to have
issues with. Stuttering, audio crackling, that sort of thing. It seems this is
an issue with the game or ROM and not of the emulator or the Deck. A bummer,
really, but nothing too problematic. Other games I seem to recall to have tried
suddenly worked flawlessly on my second try. I was also able to find out how to
remove the spurious bezels on the sides which kept irritating me (turns out this
was simple to do). On two instances I noticed that the horizontal axis of the
right stick didn't do what I expected it to do but, thankfully, this is simple
to resolve. Simply reverse the axis input in the Steam control settings and
you're good. I love how easy this is!

Bottom line: emulation is fine, apart from some games that make problems which
is rather their fault than the Deck's. In other news:
[EmuDeck](https://www.emudeck.com/) which I use for the task recently got a big
update streamlining and prettifying the whole experience. I haven't used it much
since then but it does look good!

## Third Party Things

Last time I had problems to get The Witcher 3 to work properly. I own it on GOG
and didn't have a good time trying to get it to work on the Deck. This is
something I haven't really resolved. I suspect an issue with Lutris as I was
having problems on my desktop PC as well (stuff wouldn't install properly).
However, in light of the imminent release of the Next Gen update for the game I
bought it (again) for a few bucks on Steam when it was on sale. I know this is
not the most satisfying solution but it was cheap and I wanted to get on with my
life. It's just easier this way.

You could argue that this very behaviour is probably what Valve is going for.
Playing third party games on the Deck is connected to enough friction that many
people just won't bother. On the other hand, you could say that gaming mode is
seamless enough that it entices users to just never leave it, effectively
creating a walled garden of sorts. If you think that, fair point. But it doesn't
bother me too much. At the end of the day it's a matter of convenience and I
very much appreciate the *possibility* for people to use the Deck how they see
fit since it's an open platform.

Another thing I had trouble with ties in neatly with the next point: customizing
artworks. This was possible but a *tremendous* pain. I had to enter desktop
mode, install a flatpak, mess with a website that wouldn't properly work unless
I performed some command line fu before and even then I couldn't figure out how
to edit **all** the artworks I wanted. Fun. Enter the...

## Community

The Deck has a vibrant community which I love. For example, people have made
[DeckyLoader](https://github.com/SteamDeckHomebrew/decky-loader) which is a
plugin installer. This enables you to install plugins for the Deck from within
gaming mode from a kind of store. There's a whole bunch of plugins available
now, only some of which I installed, ranging from more settings for power users
to small quality of life improvements. Especially the latter were interesting to
me. I found a Bluetooth plugin that enables me to connect to already known
devices more quickly without having to enter the settings. Neat. There's one
that shows me how much time other people took to finish games I have in my
library or one that displays ProtonDB badges for them. My favourite one lately
is one that enables me to change artworks for all of my games effortlessly from
within gaming mode, even for non-Steam games. This is just stellar and removes
any of the friction I had experienced with this earlier.

But also apart from this there's loads of tutorials, nifty tools and that kind
of stuff around. Taking a peek at /r/SteamDeck is sometimes very helpful for
this. Also, there are a couple of content creators on YouTube creating very
helpful videos explaining settings and hardware or taking deep dives into how to
achieve optimal performance, visuals or battery life for specific games. This is
very useful and saves me tons of time. I recently installed Cyberpunk 2077 and
the Next Gen update for The Witcher 3 and I gotta say 40 FPS while still looking
pretty is a real treat for these games.

## Updates

Not only is the community busy, Valve itself is also not idle. The pace at which
they churn out updates is staggering. Even though I strictly stick to the stable
branch, I frequently receive updates, many of which introduce substantial
improvements. I really do get the feeling that Valve is deeply invested in this
project and as far as I can see it's paid off. The Deck was wildly successful
and it has already been hinted that a second version of is coming (at some
point).

Granted, the UI bugs and inconsistencies I mentioned in my previous post are
still present (at least from what I've read) but these seem to concern mostly
the store and don't bother me much.

Apart from such things, the desktop experience is somewhat rocky still. The
buggy on-screen keyboard has been fixed for it but some things are still a bit
awkward. For example the fact that Steam needs to be open for the controls to
work properly. Thus, sometimes the triggers will work as mouse input and
sometimes they won't. This is not great, I admit but not a deal breaker as far
as I'm concerned. Also, it's of course possible to connect keyboard and mouse to
make using the desktop more comfortable. But as I'm not really using it for
anything I don't care much.

# The Best

You wanna know what is unironically the best thing about the Deck? That I can
use it. Since I received it I haven't switched on my desktop PC once. Since it
was only used for gaming, the Deck has replaced it completely for the time
being. And for good reason: the Deck reduces or completely removes the friction
of switching on the device and starting a game that this already makes the
difference between gaming and not gaming. Also, I can sit on the couch while
doing this which is even more important because my bedroom (where the desktop PC
is sitting) is usually occupied and thus off-limits. So far I have not played in
docked mode except as a quick proof of concept because I find handheld mode just
more comfortable for the time being. Also it puts less strain on the hardware. 

All of this is remarkable to me, considering that I'd never owned a handheld
gaming device in my life and had almost always played games on a desktop PC. I
think this already says a lot about the Deck.

# Conclusion

There's almost nothing left to say. Except that I really love the Steam Deck.
It's not perfect but the issues it does have are either not important to me or
they are vastly outweighed by its benefits. New games are verified all the time,
new updates and helpful tools released so the experience get even better over
time. I think the money I spent on it were well justified and I'm excited to see
what the future holds for it.
