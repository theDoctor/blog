---
title: "How playing with Duplo led to some recreational maths"
date: 2024-05-27T08:12:16+02:00
tags: ["Personal"]
render_math: true
---
This is probably going to be the weirdest blog post I have ever written. If you're up for it, let's do this!

I recently played with LEGO Duplo. Yes I am an adult, go figure. While building random stuff I came across something interesting (to me). I had a large green board and a small fence. The fence has two points at either end where it can be connected to the board and has a length of five.

**Side note**: As an elementary unit I'm assuming the distance between two adjacent (not diagonal) duplo nubs. The blog post is unitless but assumes this unit implicitly.

It looks something like this:

![Initial board](/assets/duplo/duplo1.svg)

I idly played around with it, detached on of the fence's ends from the board and swung it around in a circle. I noticed that there are certain positions in which the fence can be reattached to the board diagonally:

![Diagonal 1](/assets/duplo/duplo2.svg)

and another:

![Diagonal 2](/assets/duplo/duplo3.svg)

After turning 90° the fence as again parallel to the nubs on the board and can, naturally, also be attached to it:

![Final turn](/assets/duplo/duplo4.svg)

And this made me wonder. It seemed odd to me that it just so happened that a diagonal position of the fence was possible and I suspected that this is not "just" coincidence.

So first of all, I wanted to find out whether this could indeed be a coincidence or sloppy manufacturing or if there was a system behind this. Therefor I needed to know whether the distance between the nubs the fence was attached to is exactly equal in a parallel or diagonal position. Let me make it more formal:

![Measurement](/assets/duplo/duplo5.svg)

The distance between the two nubs the fence is attached to in the initial parallel position is obviously exactly 5. As can be seen from the last image above, what I called \\(a\\) and \\(b\\) form the catheti (the shorter two sides) of a right-angled triangle. Consequently, we can calculate the length of the hypotenuse using the pythagorean theorem. In this particular case this would be:

$$ \sqrt{3^2 + 4^2} = \sqrt{9 + 16} = \sqrt{25} = 5 $$

So the length is exactly the same as in the initial parallel position of the   fence. Under which circumstance does this happen? Is there a general rule to this? Note that the squares of contiguous numbers appear here: 3,4 and 5. I decided to investigate this further. The above equation can be generalized as the following:

$$ x^2 = (x-1)^2 + (x-2)^2 $$

with \\(x\\) as a variable for the length of the fence. If substituted for 5, this would yield the previous equation. Solving for \\(x\\) might provide more insight. Expanding (using the binomial theorem) and reordering gives us this:

$$ x^2 -6x + 5 = 0 $$

This is a standard reduced quadratic equation and can be solved by using the standard formula for this which gives:

$$ x_{1/2} = 3 \pm 2 $$

Since this was a quadratic equation to begin with there are, naturally, two solutions. One of them is 5 which we know from above, the other is 1. This seems rather unhelpful because it would mean a solution to our problem is a fence length of 1 which does not make much sense to us. We therefore discard this solution.

This avenue of investigation didn't yield any new insight, it seems. The next line of reasoning follows from the thought that the occurence of contiguous numbers above may very well just be coincidence. We can generalize our last equation to look like this:

$$ x^2 = (x-n)^2 + (x-m)^2 $$

This means, that the fence length for which diagonal positions are possible could be related to (theoretically) arbitrary positions (as signified by using variables \\(n\\) and \\(m\\) instead of numbers. These newly introduced variables represent the position of the fence on the board, or more precisely, the coordinates of the far end of the fence in a cartesian coordinate system in which the near end defines the origin. We shall see whether there are any solutions to this which satisfy the following:

$$ x, n, m \in \mathbb{N} $$

as all variables must be natural in order to make sense. Can't play Duplo with fractional distances, can I?

Expansion and reordering leads us to:

$$ x^2 - (2n + 2m)x + n^2 + m^2 = 0 $$

Whereas this look slightly weird it can still be plugged into the standard equation for solving quadratics which leads to:

$$ x_{1/2} = n + m \pm \sqrt{(n+m)^2 - (n^2 + m^2)} $$

After further expansion almost all terms withing the square root can be eliminated which leaves us with:

$$ x_{1/2} = n + m \pm \sqrt{2nm} $$

As we noted before, only one of the solutions seems to be valid or make sense so we drop the second one:

$$ x = n + m + \sqrt{2nm} $$

This is our master equation.

As a sanity check, we can substitute in 1 and 2 for \\(n\\) and \\(m\\) and verify that it evaluates to 5 (which it does).

Now what does that tell us? We now know that there is a relatively simple relationship between the length of the fence and the valid positions it can assume in diagonal which should be generally valid. We also know that all variables need to be natural numbers. This naturally means that:

$$\sqrt{2nm} = y$$
$$nm = y^2/2$$

This is a way of stating that the product of \\(n\\) and \\(m\\) *must* be a natural squared number divided by two, for example 2 (which we get by substituting in 1 and 2, our inital values from above). The product itself must be a natural number. This leads to a fairly simple way of finding all values of \\(x\\), \\(n\\) and \\(m\\):

1. Go along the list of all even natural numbers \\(y\\) (because the result is divided by two and the square of an uneven number divided by two can not yield a natural number)
2. Substitute \\(y\\) in \\( nm = y^2/2 \\) for the chosen number and expand
3. Factorize the right hand side of this equation into primes
4. If there are more than two prime factors, multiply them until only two are numbers are left (the various combinations that are possible each represent distinct possible values for \\(n\\) and \\(m\\) and distinct solutions). These two numbers can be chosen as values for \\(n\\) and \\(m\\).
5. Substitute \\(n\\) and \\(m\\) in our master equation for the two thus obtained numbers and solve

As an exercise, let's do this with 6 as \\(y\\):

First:

$$ 6^2/2 = 18 $$

Prime factorization:

$$ 18 = 2 * 3 * 3 $$

This gives two combinations containing two factors each:
1. \\( 2 * 9 \\)
2. \\( 6 * 3 \\)

Both are valid. Let's try inserting each into the master equation:

$$ x = 2 + 9 + \sqrt{2 * 2 * 9} = 11 + 6 = 17 $$

$$ x = 6 + 3 + \sqrt{2 * 6 * 3} = 9 + 6 = 15 $$

There we go. This means that, if we have a Duplo fence of length 17, we will be able to attach it to a board in a diagonal position, if we position the far end of it 8 (\\(x-n\\)) up and 15 (\\(x-m\\)) across from the near end (or vice versa). Similarly, this will be true for fence length 15 and positions 9 and 12.

Voilà. We have found a general way to find all fence lengths and positions which permit diagonal placement of the fence on a Duplo board.

After thinking about it some more I realized that the whole premise could also be formulated as follows:

> Given a (quarter of a) circle of radius \\(r \in \mathbb{N}\\), are there any points on its circumference describable by coordinates \\(x,y \in \mathbb{N}\\)? If so, how many and which?

As we now know, there are, in fact, not many of these points. We also know now that such points can only exist for natural values of \\(r\\).

If you've made it this far, you've got your daily dosage of pointless but possibly interesting trivia for the day. Carry on!

Yes, I may be a weird person. Deal with it.
