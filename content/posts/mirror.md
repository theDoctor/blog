---
title: "How To: Create a Digital Frame with MagicMirror"
date: 2023-02-08T22:38:58+01:00
tags: ["How To", "Personal", "Software", "Privacy"]
aliases:
  - "/posts/howto/mirror"
---

I've wanted a digital frame at home for a while now. I'm notoriously bad at
organizing pictures, even worse at sorting, printing, arranging or displaying
them. So when I found out that digital frames were a thing it seemed like this
would be a natural solution to this problem. 

Unfortunately, the commercially available ones either don't have the features
that I want (e.g. some way to connect with my images stored in my Nextcloud) or
feature worrying amounts of integration with various third-party services,
clouds, phone apps and whatnot. I'm absolutely not inclined to yield control of
my pictures to some provider who is supposed to sell me a device for displaying
them but maybe that's just me.

So I went looking for some kind of middle ground. I quickly realized that a DIY
solution might be the only option that ticks all the right boxes but I hesitated
to jump right into that project, mostly because of tight constraints on my time
and energy. Some commercial vendors actively [recommend
against](https://www.pix-star.com/blog/digital-frames/diy-digital-photo-frame/)
such and undertaking (which I find hilariously transparent) although they do
have a point: constructing everything on your own is not exactly trivial,
especially if you're not a very technical person. Luckily I am reasonably
technical so I wasn't daunted and did at least some research to see what options
I could find.

At first I thought it shouldn't be too hard to just set up one of my Raspberry
Pis, connect it to my Nextcloud instance, plug in a monitor and display the
images in some fashion with some simple tool like `feh` or so. I quickly
abandoned that line of thought after reading some more and realizing that this
would become much more complicated very fast. After some time I rediscovered a
project I had known about for some time: [MagicMirror](https://magicmirror.builders/).
It's a `nodejs` app meant for use with semitransparent displays so you can
construct a smart mirror that can display all kinds of things like a weather
forecast and the like. It also has a vibrant community with loads of third-party
modules to add functionality. In particular, there are several modules to
display images from various sources as background images in a slideshow. So far
so good, I thought.

Installing the software is easy enough. You start from vanilla Raspberry Pi OS
(not the lite version, though), pull the code from its git repository and
install the dependencies via `npm install`. Of course you need `nodejs` and
`git` first. The configuration of everything is done in a JSON file. I hooked
the Pi up with my desktop monitor and started looking into the various modules
that were capable of showing slideshows. Admittedly, my use case isn't exactly
what these were made for but I found an appropriate module that fit the bill. I
removed any transparency or other filters, tweaked the CSS a bit and, voilà, I
had a pretty slideshow going. I mounted my Nextcloud instance to my Pi via
`davfs2`, pointed the config at the appropriate folder and that was that. As a
bonus, the dev of the module was kind enough to add recursing into local folders
as a feature after I asked.

The only thing left to do was to decide how to deploy everything to production.
Or more precisely: on what kind of monitor to display it and how to connect it.
There are two ways of doing this: physically connecting the Pi to a monitor,
like I had done for testing, or get a device that has a browser buit in and
point that to the URL of the Pi since the node server runs on the local network
and can be reached that way. I considered getting a cheap-ish tablet, putting it
on some wall or somewhere else and just pointing the browser in fullscreen mode
at the URL of the mirror. But I was hesitant to follow through because it seemed
like a waste to get such a sophisticated device just for showing pictures. And
having it on the network would mean that it could phone home, unless I could use
something with a custom ROM or block its attempts to phone home otherwise. A bit
too much hassle for my taste. I searched online for a while until I landed on a
nice 15 inch monitor that looked like what I wanted. It has a stand that makes
it kind of look like an actual frame, it has an HDMI input port and all the IO
is at the back so it's possible to hide the cables from view.

I was positively giddy when the monitor arrived. I quickly connected it and
already beautiful pictures were showing up. Lovely! I had taken the liberty of
adding a few modules to the mix because, for one they were useful but also
because otherwise a lot of screen space would be wasted due to mismatch in
aspect ratios of the monitor and the pictures and also different orientation. So
now I have a digital frame that shows random images from my Nextcloud, the time,
when the sun rises and sets, the current weather and my work calendar entries,
all of which is actually helpful to me.

I even found a module that lets me control module parameters and other things
via browser from my phone. And if I need to restart the thing I can quickly fire
up [termux](https://github.com/termux/termux-app), log in via SSH and issue a quick shell command. And thanks to
Tailscale I can do that wherever, not only on my home network. This way of
configuration remains somewhat hacky and is not exactly something I would
entrust to a complete noob but for now it's enough for my needs.

This project turned out to be very satisfying and resulted in something that's
both useful and pretty. I've put the monitor and the Pi in the hallway and
whenever I come by I take a look and have fond memories of past experiences. Or
a good laugh. That's definitely a win in my book.
