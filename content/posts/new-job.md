---
title: "My New Job"
date: 2023-01-03T20:19:01+01:00
tags: ["Personal"]
aliases:
  - "/posts/personal/new_job"
  - "/posts/personal/new-job"
  - "/posts/new_job"
---

I've got a new job. I started working as a developer for a software consulting
company last October and wanted to finally take the opportunity to write about
it all.

My last job didn't end particularly well. I had been doing it for two years and
became increasingly frustrated with my work without ever quite realizing it or
why that was. I was in a position where I was unable to take a step back and
reflect on my situation, why I was unhappy with it or what needed to change. I
just noted that my motivation became less and less.

At last my contract came to a regular end and was not extended. Back then I was
unsure whether that was a good thing or not but I realized that I couldn't keep
working like I had been so on the day that the decision was made (not
exclusively by me) it felt like a weight off my chest.

I thought about my options, career-wise, and decided to actually go and make
money with what I had been doing on the side for quite a while and was much more
enthusiastic about than my job: software development. Not too long ago that was
a fleeting thought that I was sure I would never act on but only a few months
later the situation had somehow drastically changed.

I applied for a couple of jobs, was invited to a couple of interviews, was
nervous for each of them but quickly realized that the people who I had these
with were not out to get me, quite the contrary.

It didn't take long until I was offered a job and I promptly took it. I was
relieved and also very proud of myself. I had no real idea of what exactly I
could expect of my new job but that was of little concern to me then.

My "office" is in another town quite some distance from where I live so it isn't
feasible for me to commute. Instead, I decided to mostly work from home for the
time being and maybe move closer to the location of the office if I found I
liked the job enough. For my first days I made the trip and stayed for a week to
get acquainted with my new colleagues the office itself, the surroundings and
whatnot.

Something that I really liked from the beginning is the concept of tutorship.
Every new hire gets a tutor for the location where they work to show them
around, introduce them to people, explain to them how things work, where to get
food and the like. Also, there's a project tutor who gets you set up with access
credentials, software, introduction to appropriate people and communication
channels and generally serves as a mentor for that project. This is really
awesome and reduced the friction of starting a new job with all that entails a
great deal. After I got set up and completed most of my onboarding tasks
(company-internal stuff) we did some pair-programming (which was a first for me)
and it was honestly very enjoyable. Not only did I learn a lot of stuff (this
was an Angular project which I had never seen or touched before), I felt like a
part of the team and was able to provide meaningful contributions almost from
the start.

A few days after I started we had an all-hands meetup with workshops,
socializing food, drink and even a party afterwards. A terrific opportunity to
meet new people, get in touch with colleagues I'd only known digitally and get
into the company. I didn't stay too long because I can take only so much social
interaction on a given day (especially with people I don't know) but it was
great.

I could go on like this for a while but it basically boils down to this: I felt
like I was given the help I needed and valued as a colleague pretty much from
the very beginning. That alone goes a long way to making me feel at home at my
new job.

I frequently have moment when I can hardly believe how much of a contrast there
is between my current and my last job and how happy I am to have this new one.
Of course not everything is awesome but the general sentiment still stands.

On the flip side, the biggest issue so far is that I still have no team or
project I belong with for the longer term. So far, everything I have worked on
has been temporary. Also, my company subscribes to the idea of agile development
and tries to organize people in Scrum teams. So far I don't have one because of
a shortage of projects to assign people to. This has led to some frustration as
I had situations where I was asked to help out or work on something on short
notice without knowing much about it or having worked with the involved
technologies but also without a team. Since I'm used to this kind of thing, it
wasn't too big of a deal but also not ideal. Still, I've been able to learn lots
from it all.

Learning new things, especially in terms of software, is always fun for me and I
was happy to be able to do just that quite a bit so far. Here's a list of
technologies I have worked with or learned about in the short time I've had this
job:

- TypeScript
- NodeJS
- NestJS
- Jest
- AWS
- CI/CD
- Terraform
- Angular
- Docker/Docker-Compose

I'm confident more things are to follow. In the near future I will be
(hopefully) obtaining an AWS Developer Associate Certificate as we do a lot of
stuff in the Cloud and specifically with AWS. Also, I'll be giving a Rust
workshop in about two months which I'm really excited about. I've never done
something of the sort and it'll be interesting how it turns out.

Lots to do and look forward to. I'm looking forward to it!
