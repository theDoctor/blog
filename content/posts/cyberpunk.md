---
title: "Review: Cyberpunk 2077"
date: 2023-09-18T16:00:34+02:00
tags: ["Gaming"]
aliases:
  - "/posts/gaming/cyberpunk"
---

This game is an interesting one. It was infamous after its release and I haven't
been in the habit of paying attention to new game releases for a long time. For
the most past that's because for the longest time in my life I didn't have a
powerful enough gaming PC and my disposal (also no gaming console) so I couldn't
have run them anyway. Now that I own a Steam Deck I'm in a sort of narrow window
where somewhat recent AAA games are available to me which is why I tend to look
around the Steam Store a bit more nowadays.

I'd been reading about Cyberpunk 2077 on and off for a while and similar to No
Man's Sky the launch was a disaster but apparently the developers kept at it and
released patches and fixed until the games became playable and, reputedly, even
good. Note that I played the version 1.6 of the game. Also, the Deckverse dude
has a [video](https://youtu.be/XJ4uPLZ2-Jw) about getting good performance out
of the game. All requirements were met so I decided to go ahead. Oh yes, and it
was on sale. No point in throwing good money out the window, right?

> DISCLAIMER: This will contain spoilers. You have been warned.

## First impression

Cyberpunk 2077 is a first-person shooter/RPG in a cyberpunk setting (who
would've thought). You notice from the start when creating the character with
all kinds of options for implants, tattoos, funky hairstyles and whatnot. Oh and
you also get to decide on which genitals you want. Nice.

I'm a stranger to the cyberpunk genre in any form and the game is not great at
explaining things. You get thrown into the game in medias res and have to take
it from there. There's a tutorial to explain the basics to you: combat,
movement, stealth, hacking. The lore and world building are never really
explained to you. There's even a bunch of funny words and idioms the characters
keep on using which are never explained. You can figure it out usually but the
game is rather unapologetic in this way.

Another thing that stand out is the visuals. The game looks beautiful. Sure,
it's flashy, garish and plain strange at times but beautiful. Some of the vistas
of the city you get are nothing short of stunning.

## Gameplay

After some ramp-up time you get into the rhythm quickly enough, I felt. You are
introduced to a few characters, like your new best buddy Jackie, get a tutorial
for how things work and start doing missions. These are either story missions or
side missions. The game also distinguises between gigs and side missions with
the former being missions given to you by handlers as a way to earn money and
the latter being missions you come across while traversing the world. These can
be pretty much anything. The whole game is quite centered around doing missions.
Although you can of course roam around the open world, I usually find myself
going from mission to mission and looking at things in between with occasional
stops for upgrading gear.

I've never been a fan of first-person games. In my opinion the view feels too
restricted. Also, what's the point of spending an hour creating a cool character
without being able to see them except for a not-that-great reflection in a
mirror every now and then? This is also one of the reasons why I never warmed up
to The Elder Scrolls series.

On the other hand, this also opens up the possibility for more immersive
storytelling and they do make use of it so after a while I got used to it and I
think the perspective works well for this game. Combat is fine, I suppose but I
deliberately chose to play a hacker who takes out enemies via quick hacks. This
is quite a strong playstyle, it seems, and it also means that I don't have to
aim weapons with a controller under pressure as much which is something I'm not
good at. Aiming is for mouse and keyboard, not joysticks. It just doesn't work
as well. Did I mention that I never really owned a console?

Hacking is fun though. Granted, the number of useful quick hacks isn't large but
it was fun sneaking around, infecting everyone with diseases or shocking them to
unconsciousness without anyone knowing what was going on.

The game features an open world. I mean, all games nowadays have that although I
wish that weren't the case. The issue this usually brings with it that the world
needs to be filled with stuff so it doesn't feel empty and this is often done
with collectibles and fetch quests of some sort which create ambiguous feelings
in me. On the one hand I like to complete everything but on the other hand I'm
too lazy to complete 100% of a game nowadays because what with achievements and
collectibles and all that stuff that quickly adds up to 100+ hours of play time
which I simply don't have or rather don't want to spend.

Cyberpunk's take, however, is delightfully nice in this way, though. It does
have a lot of gigs and side quests, cars to buy, apartments to purchase,
cyberpsychos to hunt, races to win and the like but for some reason I didn't
feel compelled to do all of this. The game distinguishes quite clearly between
things that are optional and the ones that aren't. Also, it's always apparent
that, e.g., gigs don't contribute to the story (a little world building maybe)
so you can skip them in good conscience, if you just don't need the money
anymore. Consequently, I finished the game in ~45 hours which is less than most
people take but I don't feel like I missed out on much.

## Story

This is the most important part of a game for me, hands down. If the story is
boring, odds are I won't play the game, at least not for long. It's just how it
is for me. So the question is, what is Cyberpunk 2077 about? On the surface it's
about the player character V (which is actually short for Victoria in my case)
who returns to Night City after some time of absence. Before long she accepts a
job, gets caught, meets her new best buddy Jackie, is offered the gig of a
lifetime and before long is in deep. But I think the most important part of the
story is not so much what it's about but more **who** it's about. It's about V,
her desire to leave a mark on the world, about friendship, love, the will to
survive, it's about despair, about keeping going, about finding something that's
worth living for and maybe most of all: the game is about Night City. It's
frequently portrayed as a character of its own which I find fascinating. The
city is deeply corrupt, ugly, hungry for people to devour. It's shiny and
flashy, full of promises for money and a better life but in the end it's like
going to the casino: you have fun for a time but in the end the house always
wins and you lose everything, if you don't get out while you can. But almost no
one ever leaves Night City. City of dreams and shattered hopes.

This is exactly what you experience as a player and as V. You arrive full of
hopes and ambition to leave your mark, to be someone, become a legend. You
accept job after job, gig after gig to earn money, get better gear when finally
you get the opportunity of a lifetime. You accept but it all goes horribly
wrong. Your best friend dies, you're shot in the head, barely survive and spend
the rest of the game trying desperately to find a cure for the insanity that
lives in your head, slowly killing you. Especially during this period of the
game the first-person perspective really pays off and makes you feel like you're
V.

During your quest for help and survival you make new friends, complete even more
gigs and jobs, become more notorious, find unlikely allies and (if you so
choose) even love but it's all so frail and ephemeral. Nothing in Night City
ever lasts, everyone and everything in constantly fighting for more or just for
their lives. It's quite similar to portrayals of Gotham City from some of the
Batman media out there in my opinion.

I'm a fan of happy endings but it probably wouldn't be fitting for this game to
have one. They're not terrible but they're bittersweet at best. There are a few
different ones, depending on the choices you make and I haven't explored them
all yet. From what I experienced and read, however, they all leave some
questions open: what exactly happens to V afterwards? Will she survive? And if
you choose to not remain in the real world: what happens to her then? What about
the personality she has been sharing her mind with for so long?

One of my favorite moments was the ending where you leave the city with your
love interest Judy (in my case) and the Aldecaldos. You take one last,
bittersweet look at the city, alone and together with Judy and then turn your
back on it. You don't know how long you have until your body gives out but at
least you're free and with your loved ones. The powerful soundtrack makes this
an even stronger moment. At some point I might return and play the other endings
as well, see how they hold up in comparison.

The game definitely has a few great moments, good and bad. The River quest line
is nothing short of disturbing. Judy's quest line, especially the conclusion
under water is just mesmerizing. And looking at Night City's skyline, full of
promise and terror just never gets old.

## Conclusion

So what's left?

The gameplay is fine although it didn't exhilarate me. The setting is very
weird, if you're not used to it but it has its own charm.

On the technical side, the Steam Deck can deal with Cyberpunk just fine and I
didn't encounter any game-breaking bugs. There were a few but nothing to write
home about. Although there was one that was both fun and annoying. During a
mission with another character with both of you on a (separate) motorbike the
game apparently has some weird pathfinding or waypoint management built in. The
motorbike of my companion glitched through my own at high speed (probably to
place it at a scripted location) throwing me off and damaging me in the process.
But that happened once and was the most noticeable thing I remember.

These days a major upgrade and DLC for the game is coming out which will also
lift hardware requirements as far as I know. Maybe I'll revisit the game later
on, if it's still playable on the Deck after this.

All in all, I enjoyed this game. It took me a while to get sucked in and I spent
the first hours getting used to the setting, the controls and everything. After
that I got hooked on the story but even more so the characters. I wanted to see
what happened next, how V would survive (if at all), I wanted to spend more time
with Judy and River (although I did turn him down). Also, some of the Silverhand
side quests are just hilarious. The dude is such a stereotypical rocker and
rebel it's funny. All in all I had some very memorable moments which is the main
selling point of the game to me.
