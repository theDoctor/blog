---
title: "Review: Sea of Stars"
date: 2024-12-25T18:06:05+01:00
tags: ["Gaming"]
---

# Introduction

Over the past few weeks I've played an finished Sea of Stars and I decided to write a review for it.
I'll be going over some things but keep it mostly spoiler-free until the end of the review.
The spoiler section will be marked and the article will be concluded by a spoiler-free section
at the very end.

Sea of Stars is a fairly recent game in the style of a JRPG. It was inspired by Chronotrigger
(which I've played but never got very far) and adopted a few mechanics from it, as far
as I can tell. You play a party of people and control them from an isometric point of view.
Combat is turn-based and the way you travel over the map seems very reminiscent of Chronotrigger.
But can it compete with the original? We'll see.

# Gameplay

## Traversal

I'll begin with a strong point of the game: traversal of the world. Apart from running around on
flat surfaces, there's climbing, jumping across narrow gaps or down ledges, balancing on tightropes
(who installed those anyway?) and more ways to move that are unlocked later in the game. This
feels very immediate and intuitive, especially in comparison to other games of the same genre
where the level design can feel a bit contrived because the game doesn't let you simply
jump down this small ledge to get to your destination. Instead you have to walk a mile around it
to arrive there. I love how Sea of Stars does this and it also serves to realize some
clever level design which avoids backtracking a lot.

## Combat

The combat is turn-based without any sort of timer. You have several characters to control and
you get to choose the order in which they move. Also, if you have more characters than can
fit on the field at once (three) you can freely swap them in at no cost.

You can choose between standard attacks, skills, combos (hello Chronotrigger) and items.
Skills are unlocked by progressing in the story, combos can be found or become available
as new characters join the party. So far so standard. What Sea of Stars adds to this
is the Live Mana system, timed attacks and blocks and casting locks.

The former is a system where so-called Live Mana is generated with standard attacks
that can then be used to boost your other moves. Your attacks, skills and combos
can also be timed, meaning you have to press a button at the right moment to increase
their effectiveness (damage, amount healed etc.). The same goes for blocking enemy
moves which reduces their damage. This is fun overall and adds another tactical level
to the combat system. The timed blocks and attacks work fairly well overall after you
get the hang of them. However, especially when using skills or blocking enemies the
game gives you almost no indication when the right moment to press the button comes
so you have to try and repeat until you get it right. I found this annoying at times
and there were certain skills that I was never able to boost or block reliably.

The lock system displays a number of symbols representing different types of damage
on an enemy when they're charging up a spell. If you manage to apply damage of the
appropriate type and amount you break the lock and interrupt the spell. This is a
core mechanic, especially in boss fights.

## Balancing

On standard difficulty the game is pretty easy. There were only a handful of instances
where I lost a fight, even the boss fights are usually simple to beat on the first try.
I also found that you can usually go full ham in any fight because if you do, you
charge up your combos and ultimate skills which can in turn be used to heal your
party. That way you are rewarded for playing glass cannon-style and dealing as much
damage as you can. This goes even so far that you can often ignore the enemy's spell
locks after a certain point.

The game *does* offer a mechanism to change the difficulty granularily, though, which
is great. This is implemented with a set of relics. You start with a few and can find
or buy more of them during the game. These can be (de-)activated from the menu and
offer various changes to game mechanics to influence the difficulty. For example
you can get more health for your party, more (or less) damage dealt or received,
automatic blocks, easier minigames and more. So if you're looking for more of a challenge,
you can have it.

# Graphics

The games looks very pretty, in my opinion. I'm not a graphics nerd but the game does go
to some length to improve on the looks of classic JRPGs and I think it's safe to say
that it was successful in this. This is one of the strongest points of the game,
I think.

# Character Progression

The character progression in the RPG sense is rudimentary. You gain experience points
for enemies defeated and level up once a certain number is reached. On level up you
receive a stat boost and can choose to boost a certain stat further. That's it.
You periodically find or purchase new weapons and armor (of which there is one kind
per character each) which gives you a stat boost as well, no special properties.
As mentioned above skills are learned automatically and combos are found so the
progression there is limited.

There is absolutely no need for any sort of grinding which I appreciate.

Overall, there is very little customization or user choice involved, the characters
simply become stronger linearly, without any nuance to it. Depending on your
preferences, this may be good or bad. I didn't mind very much but compared to other
games of the same sort it does stand out to me.

# World Design

The world works similarly to Chronotrigger, meaning you traverse an area at a time
and move between areas on the world map which shows you a small icon where your
characters are. You then move around to enter the next area. After a while you
obtain other means of transportation to access new areas of the world.

I think the whole level and world design is pretty good. The game avoids
backtracking as much as it can, introduces shortcuts often or give you
ways to reach earlier areas faster. Also, the progression through the world
is very linear. At first it's literally linear, after about 10 hours (in my case)
you unlock a means of transportation that lets you choose your destination
but since the world is organized in islands and there's only a limited number of
them (with some inaccessible until the endgame), there are not many places
you can go apart from advancing in the story.

I thought this was refreshing. The game doesn't present you with loads of side
quests (there are almost none, in fact), collectibles (I hate whoever established
this concept in gaming!) and other such nonsense. There is almost never a point
where you don't know where to go or what to do.

# Miscellaneous

The soundtrack is nice. For the most part it's just fine but there are some
instances where it really shines. One or two boss battles come to mind and
I relistened to the soundtrack dozens of times because I liked it so much.

There's a fishing minigame. It's ... there.

The Wheels minigame is actually pretty fun (I thought). At least it was good
enough to motivate me to play all championship matches and unlock all of the
figures.

The storytelling mechanic with the travelling historian is pretty cool, I just
wish they'd made use of it more to flesh out the world more.

# Story

This is on of the weaker points of the game, maybe the weakest. The story isn't
bad pre sé, it's decent. But what's more important is that character writing
is bland, especially in comparison to other games. There is no character ark
or development for any of the characters in the game. The protagonists are
lawful good because of reasons and the antagonists are chaotic evil because
of reasons, broadly speaking. There's a little more to it but not much. And
yes, Garl is such a good boy, we get it.

There's no deeper theme, no romance, no big plot twists (there one, if it's big
depends on your point of view) it's mostly a save-the-world-from-evil tale,
case closed.

# Conclusion

I played the game for about 30 hours. I enjoyed almost all of
it with the exception of some of the design choices at the end of the game
(see upcoming spoiler section). I would recommend it with the caveat that it
makes some choices that won't appeal to everyone. It worked very well for me,
though. The replay value, however, is basically zero so there's little chance
of me playing it again but that's fine with me.

So is this better than Chronotrigger? Probably not but I wouldn't know. It's
a good game, though.

> **Caution! Here be spoilers**

And now we get to what I think is the most problematic part of this game: the
endgame. At one point in the game you unlock the ability to fly and are told
that new areas are now accessible but also that the end boss awaits you now.
From that point on you can do a number of side quests that are mostly
companion-specific or directly finish the game.

Once you do defeat the final boss you receive a credit roll and final cutscene
but are told in pretty certain terms that this is not the real ending. Also,
a new artifact has turned up in the world which might be worth a look (*wink wink*).

So now you get to reload your savegame from before the final boss and return to the
world to do... side quests. That newly unlocked artifact is a mechanism to gate the
true ending of the game behind side quests (the ones I mentioned above). This is
a weird choice, I think, but you could have done these before the final boss battle
(which I did). These quests aren't that obvious, though, because they are mostly
started by interacting with your companions in resting areas which I stopped doing
after a while because nobody has anything interesting to say. Oh well.

So I did all these side quests but the game doesn't only make you finish *all* of
them to get the true ending. You also have to finish a freaking collection quests.
You have to find all 60 Rainbow Conches which is bloody impossible on your own.
I opened a guide on my phone and went through the list one by one which was annoying
as **fuck**. Some of these things are located in hidden areas that you can really
only find, if you know they exist.

And once you've done all this you can return to the final deungon and defeat the
final boss. Only this time it's another boss that's actually harder.

The ending of the game is still a bit strange because it leaves quite a few questions
unanswered but maybe that's just me.

And if you're wondering: you can't get the true ending on your first try (I checked).
You *have* to do this dance of beating the final boss and then returning and doing
it all again.

> **Caution! Here be extreme spoilers**

The reward you get for finishing all the side quest stuff is that you get to save
Garl (who had sacrificed himself to save you) from death which cheapens his sacrifice
I think. But sure, who doesn't like a happy ending? He's also granted a wish and his
wish is ... dinner. Right. Moving on.
