---
title: "Choosing an Operating System"
date: 2021-09-03
tags: ["Software", "Privacy", "Linux"]
ShowToc: true
summary: "How to choose a (desktop) operating system and why you should care in the first place"
aliases:
  - "/posts/privacy/os"
---

### What?

For those who do not know: an operating system (OS) is the software that runs your device, the most
popular ones being Windows, MacOS, Android and iOS.  
It takes care of communicating with the hardware (that is hard drive, graphics card, processor etc.)
and starts it up, provides you with the nice graphical interface that you're (probably) used to
and much more. It is also the layer underneath all the applications you're using, like Office,
a browser or games.  
This makes it obvious that the operating system is an integral part of computer usage since it
basically is responsible for how everything is handled under (and on) the surface. 

### Why?

Most people probably never actively choose an OS, they just buy a device and use whatever's preinstalled.
For desktops and laptops, that's either MacOS or Windows, depending on whether they have an Apple device or not.
For mobile devices it's iOS or Android, depending on whether you're getting an iPhone or not. 
As of 2021, this is basically it. Everything else
is just a collection of niche products and corner cases that makes up a tiny portion of the 
[market](https://en.wikipedia.org/wiki/Usage_share_of_operating_systems).

It's just that there are a few issues with this situation:

#### Monopolization

If the worlds OS market is in the hands of just three giant tech companies (Apple, Google, Microsoft), that
creates a terrific opportunity for abuse of power over users and competitors. As a matter of fact,
this is precisely what happened and is still happening. Google has been accused of this and was 
[fined](https://en.wikipedia.org/wiki/European_Union_vs._Google) several billion Euros by the EU over
the course of the last ten years.

Apple was accused of [unfair restrictions](https://en.wikipedia.org/wiki/Epic_Games_v._Apple#Onset_of_legal_action) 
in their app store by Epic Games which is a still ongoing lawsuit that amounts to Apple being
accused of abusing their position to suppress competition.

Microsoft also faced more than one lawsuit of this kind, even as early as 
[1998](https://en.wikipedia.org/wiki/United_States_v._Microsoft_Corp.) where it was complained
that Microsoft had bundled their own web browser (Internet Explorer) with their operating system (Windows)
which was ruled to constitute monopolization. At the time this apparently seemed like a
genuine concern, from today's perspective it seems laughable.

This is just the tip of the iceberg but I think it serves to illustrate the point. These three
companies (and others with them) are in a prime position to abuse their power through monopolization and have
done so for years now.

#### Vendor lock-in

The concept of vendor lock-in means that once you buy a company's products, you're stuck with them. Some examples would be:

- Apple devices only supporting their charging cables, headphones etc. so you cannot use other or cheaper ones.
- Phone manufacturers not permitting the removal of the battery so as its capacity decreases you're forced to buy a new phone.
- DRM-protected media essentially binding you to a certain platform in order to get books, games or music.
- Printers not supporting no-name brand ink cartridges.

There are many cases of this practice and not restricted to the three companies I listed. However, since they have amassed so much power by now, it enables them to get away with this much easier.

#### Lack of Choice

This might not be as strong an argument as the previous points but I still stand by it. I'm the kind of person who wants to be able to choose. Granted, sometimes the sheer amount of options is overwhelming, but two options for something as ubiquitous
and fundamental for operating systems for computers? That seems odd to me.
If you're like me, you might like to tinker, tweak and configure stuff so it's just
right and pretty. You appreciate your freedom to do with your device exactly what
you want and feel in charge of it.  
Yeah, good luck with that. And even if *you* don't happen to care about this, *I* do. 
And plenty of others do too.

### How to choose

With all that being said about why one might care, what to do with this information?
Basically, I see two approaches here:

1. Figure out what you need and then choose from all available options what fits best
2. Rule out everything that doesn't suit your philosophy and use what's left

If you're like me, you'll take the second choice. Since you're probably not, let's talk about option one first. Also, phones are a bit different from desktops, so let's
do desktops (and laptops) first and have a look at phones in another post.

Most people need something to browse the web, occasionally write documents, print 
them, send mails and maybe video chat once in a while. If this sounds like you, any 
operating system out there will do the trick even the more obscure ones.
If you play games, Windows is the obvious choice, no questions asked. If you're 
willing to put in a little effort once in a while or accept that not everything will
work flawlessly, then Linux is a valid choice as well.

So it appears that this approach doesn't really narrow anything down and
the rest comes down to personal preference.

If you're like me, however, you care about your privacy and security. And if you do,
Windows and anything Apple are just not viable choices. Windows, as of version 10
is basically [spyware](https://privacytools.io/operating-systems/#win10) that 
collects a staggering amount of data. Apple, on the other hand, likes to pretend
it cares about you and your privacy but their recent plans of scanning all Apple
devices for images of child sexual abuse material (CSAM) revealed without any doubt 
that they really
don't. There has been an outcry by basically all privacy-minded people and 
organizations, among them Edwards Snowden who calls this a "tragedy" and a "
[disaster-in-the-making](https://edwardsnowden.substack.com/p/all-seeing-i)".
Any dictator's and surveillance state's wet dream. Hooray.

### Other Options

If Windows and MacOS are so bad, what other options are there? The most 
prominent one is **Linux**. There are some others but if you even know what something
like FreeBSD is, you don't need me to explain anything to you.
Linux is actually not one operating system but rather a kernel, that is a core around which many actual operating systems are built, that are, in practice, all called "Linux". These OSes which are based
on the Linux kernel are called distributions, distros in short.  

By now there are hundreds of them available with more being released every other day
or so (see [here](https://distrowatch.com/) for more information). This is somewhat
unfortunate because it raises the hurdle for people interested in trying Linux but don't worry: it's not as bad as it seems.  

#### Just try it

If you decide to actually give it a go, you are immediately faced with the question of what distro to actually use.
Several books could be filled with this topic but luckily Linux has one important
thing going for it: it's extremely easy to try it out. You can download basically
any distro on a USB stick, plug it into your machine, boot from it and **boom** 
you're running Linux on your computer without having to install anything.  
You can try out software, look at your files, surf the internet and everything else
you would normally do. Nothing you do here will leave persistent changes on your
machine (except when you go and edit files from your already existing OS) because
everything is run from the USB stick.

This was definitely an eye-opener for me and after I discovered this in 2018 I 
turned all my USB sticks into boot machines for all the distros I thought looked 
interesting. It was me shopping for the distros with the coolest interface, the 
most beautiful design, the prettiest icons and I admit I have a taste for slick design. Linux has plenty
of that to offer and this is one of the great things about it: there's a distro
for (almost) everyone:

| You want ...                            | Then try ...                                                                           |
|:--------------------------------------- | -------------------------------------------------------------------------------------- |
| Something similar to windows?           | [Linux Mint](https://linuxmint.com) or [Zorin OS](https://zorinos.com)                 |
| Established and easy to use?            | [Ubuntu](https://ubuntu.com) (and its [flavors](https://ubuntu.com/download/flavours)) |
| Something minimal and customizable?     | [Arch](https://archlinux.org)                                                          |
| To learn how it all works?              | [Linux from scratch](https://linuxfromscratch.org)                                     |
| Rock-solid and stable?                  | [Debian](https://www.debian.org)                                                       |
| Stability but also the latest software? | [Fedora](https://getfedora.org)                                                        |
| The look and feel of MacOS?             | [elementary OS](https://elementary.io)                                                 |
| Something but have no idea what?        | [Online tool](https://distrochooser.de) for choosing                                   |

#### Free for all

You can also try all of these and decide after you've gotten an overview! This is
another thing that's great about Linux: it's free! You can download and install it
right now without having to pay or worry about licenses and the like. You could do
this right now and be set up in 15 minutes!  
After having discovered this freedom for myself I did what is called distro-hopping.
I just tried many different distros without ever really settling for one permanently.
I started with Mint, tried Ubuntu, Manjaro, Solus, Debian, elementary, deepin and so
on. I used Manjaro for quite some time, went to Arch, which Manjaro is based on,
switched back and now migrated to Fedora. Probably for good.  
While this may sound like an exhausting list and like a lot of hassle it was really not. I enjoyed it all and learned a lot about Linux, computers in general,
file systems, networking and much more. You don't have to but it's easy because
Linux makes it easy.

Another thing that I like about Linux and that I probably enjoy the most is the 
freedom I gain by using it. I never realized what this might be like because I've
been on Windows for most of my life but now that I've made the switch the difference
is like night and day: I can do whatever the hell I want with my PC.  
Linux is not terribly opinionated in how I should use it. Of course there are distros
that are but if I don't like those, I can just switch until I've found the right one
for me. There are distros that just give you a bare terminal prompt and let you
figure out everything else yourself so you can set up your system *exactly* how you
like it, desktop environment, init system, boot loader, office suite, browser, text
editor, everything.  
If you don't like how something works or looks, make a change! Change distros,
change desktop environments, change configurations, software packages, you can
change and rewrite anything. Granted, that's not really an option for a complete
newcomer, but you could! And this can be felt when using Linux all the time.  
You can completely break the system and make it unbootable and Linux will let you
do it because you own the device, not the other way round. If you got worried just now that Linux will spontaneously break, take heart: this doesn't happen just by accident. It takes some determination or stupidity or both to achieve it.

#### Open-source

But how is all of this freedom even possible? Simply because Linux and all the 
distros are free and open-source software (FOSS). That means it costs nothing and
everyone can look at the source code, propose changes, discover problems or just
download it and make your own project out of it (this is called forking). This is
why there are so many distros: someone didn't like something and they decided to
just take the code and do their own thing with it. Thus another distro is born.  
Nowadays, almost all of my used software is FOSS and I'm still excited about this
fact.

Looking back, my personal journey to my current operating system  
is at its core a journey from proprietary software promoting vendor lock-in to
something that makes me feel in control of my machine and my data. My interest
in Linux and privacy have always gone hand in hand so I cannot really discuss one
without the other.

### So what did I choose?

Over time I realized what works for me and what doesn't. I
learned more about how things work and became somewhat opinionated about what I
wanted and needed. I enjoy having current and up-to-date software to use, it still
makes me a bit giddy when I install updates (and how many Windows users can say
that without lying?) but I also do not have the time and energy to spare to
do a lot of maintenance, tweaking and fixing (been there, done that). Also, I
have a strong view on FOSS software or the lack thereof and some distros take this
more seriously than others.  Furthermore I'm a sucker for pretty-looking software.  
Taken together, the distro that ticks pretty much all of my boxes and
is beautiful as well (a matter of taste of course) is Fedora which is what I'm using now. I don't expect this to change anytime soon but who knows?  

> All it takes is a download and an available USB stick...
