---
title: "Working efficiently with my MacBook Air"
date: 2022-06-03T15:27:22+02:00
tags: ["Personal", "Fiction"]
aliases:
  - "/posts/personal/macbook"
---

I've changed jobs recently so I no longer had access to my trusty ThinkPad T490
running Ubuntu anymore. Instead, I was provided with a brand new MacBook Air.
I've got to admit, I've always liked the look and feel of these machine (from
the outside that is) but I've never used one myself, barring messing around with
it in the local electronics shop.

With that said, I had some concerns with being able to work with this thing
efficiently because I was so used to my Linux setup. So in preparation I spent
hours and hours with watching YouTube tutorials and reading up on how to use the
systems. I set up custom folders and shortcuts in finder, I installed
[homebrew](https://brew.sh/) because I couldn't stomach the idea of not having a
decent package manager. I used this to install various things like alacritty,
zsh and a few other terminal tools.

Since macOS is also a UNIX-like OS, I can readily use my dotfiles (with some
tweaking here and there) so my shell aliases, shortcuts and the like still work.
I had already been using the command line a lot on Ubuntu so doing the same on
macOS felt only natural. In fact I got rid of as much of the clutter available
on the screen as possible, disabled desktop icons, changed to a very subdued
desktop wallpaper, disabled notifications and some of the system tray and
installed terminal-based apps whenever I could. Things like neomutt for mail,
newsboat for RSS feeds, neovim for coding (which still works with my previous
config, luckily).

With all this I feel like I can really be productive and concentrate on one task
at a time without being distracted by things grabbing my attention constantly. I
also feel like putting in the time to learn about how macOS operates was worth
it, although I've tried my best to apply my previous workflow to this machine as
well.

I know, what you think: if you're so keen on keeping your previous workflow, why
even go with a Mac? That's a valid point you raise there and I've asked myself
the very same question. Is it the looks? The performance? The cohesiveness of
the whole experience? It's difficult to pin down but for some reason it still
felt like the right thing to do, I went with it and don't look back.

With all that said: macOS annoys the hell out of me and after all this, I went
and bought a used laptop off ebay, slapped Ubuntu on it and am using that as my
daily driver. 

So how am I using my Mac efficiently now? As a wrist rest. Perfect height to go
with my keyboard. Highly recommended!

> ***Disclaimer***: *I stole this blog idea from
> [TheWK](https://fosstodon.org/@thewk) on Mastodon and turned it into a piece
> of fiction. So in case you didn't notice: nothing in this post is factual in
> any way.*

