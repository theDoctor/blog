---
title: "Keychron K8 - First Impression"
date: 2022-06-03T14:02:44+02:00
tags: ["Personal"]
aliases:
  - "/posts/personal/keyboard"
---

One fine day in May I switched on my desktop PC only to discover that my
keyboard would not process any key presses. The LED signaling that the num
block is active did not light up (which it usually does) and I figured that the
time had finally come to lay this fine keyboard to rest. Well, it wasn't all
that fine, in fact it was very cheap and came in an all-inclusive desktop PC
package from Aldi in 2009. I think almost 13 years is a decent track record for
such a thing.

I admit, I had secretly been waiting for something of the sort to happen because
ever since I had my employer buy me a mechanical keyboard I fell in love with
them and had wanted one for my personal use as well.

After some short deliberation and input from the usual suspects on Mastodon I
decided on a Keychron K8 with German layout because, you know, I'm German. Also,
it was on sale. Weirdly enough, it was cheaper to order it on the official
website by a decent margin that buying it from a local retailer, including
shipping costs. Go figure.

Quick facts about the thing:
- Tenkeyless
- Wired and wireless mode via Bluetooth (cable included)
- Windows and macOS mode available
- Mechanical, hot-swappable switches, Gateron brown
- RGB backlighting
- Key caps for Windows and macOS included

I did not do an unboxing video. I hate the thought of complying with any kind of
YouTube trend, also I know nothing about making and editing videos and it would
have turned out terrible. I liked unwrapping it, I swapped the key caps to the
Windows versions because it corresponds better with what I need, even though I'm
on Linux.

My desktop doesn't have Bluetooth support so I plugged it in and started using
it. Out of the box, I like the way it feels. The caps feel nice, it sounds quite
different from the keyboard I have at home but I wouldn't say I like it less. A
quick typing test on some random website online told me that my typing speed is
around 60-70 wpm (in easy mode, that is) so I'd say I get along quite well
without a lengthy adjustment period, although my previous keyboard had flat key
caps.

Some observations:
- Hooking the keyboard up to my laptop worked very well, no setup apart from
  pairing required
- Switching to macOS keyboard layout switches the position of the Alt and Meta
  keys. While it feels weird to me, I suppose this is only natural for Mac users
  because they just have different keys. I don't really know much about this,
  I've never owned or used such a machine.
- F keys didn't work out of the box, pressing the Fn key and hitting F keys also
  did nothing.

That last point was weird. It didn't come totally unexpected, I had seen
something like that somewhere online before. Still, a bit of a bummer. Keychron
apparently doesn't care too much about compatibility with or support for Linux.
They refer you to their Facebook Keychron Linux group. Seriously. This is the
single biggest gripe I have with this whole experience so far. I even made an
anonymous Facebook account via Tor and the Facebook onion address, signed-up for
with a specifically created Proton mail account (because, I you couldn't tell by
now, I hate Facebook with a passion and don't want to give them one piece of
data about me, if I can help it). 

After a short while I found a viable fix for it, though. You can just start a
root shell session and do:
```
echo 2 > /sys/module/hid_apple/parameters/fnmode
```

After this, one may have to press and hold `Fn + X + L` to switch default modes
(I honestly don't quite remember if I had to do this or not, I did it a couple
of times, regardless) and it works. Simple enough.

To make the change permanent, one has to add the following to `/etc/modprobe.d/hid_apple.conf`:
```
options hid_apple fnmode=2
```

Done.

One last thing that I stumbled over was the fact that this change is only
operational after the kernel is loaded, it doesn't help before that. So I had
trouble accessing the BIOS on my machine because it necessitates pressing F11 on
my machine. I freaked out a bit about this, asked on the Facebook group
(seriously, **fuck** that), only to realize that it actually works just like
that.

In hindsight, I have no idea what happened. I'm sure that I tried it a bunch of
times without success but when I sat down to try various combinations of
Windows/macOS mode and pressing/not pressing the Fn key, I found that it works
with the Windows setting without having to press the Fn key at all. No idea what
happened there, probably I was just stupid the first time I tried and gave up to
soon or something.

In conclusion: I like my new keyboard! It works as it should, I can use it for
multiple machines without cable management hassle (although the same thing can
be achieved with [barrier](https://github.com/debauchee/barrier)) and I like the
look and feel of it quite a lot. Haven't gotten too much use out of it yet
because it's sitting at home and I do most of my typing at work but in time
that'll change. When it does, I may post an update. 

Until then, cheers!
