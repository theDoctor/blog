---
title: "Introduction"
date: 2021-09-01
description: Introduction
tags: ["Introduction"]
summary: "Me saying hello"
---

### Hello world

Dear reader,
I finally made it! Although I have basically no experience with web development, here is
my personal website. I'm quite excited about this and it's going to be interesting to see
where it takes me. Of course I'd like to take all the credit but the truth is that there 
are software and services out there that make this kind of endeavor relatively easy and painless.   
With that said, a big shout-out to [Hugo](https://gohugo.io) and the creator of this particular theme, [PaperMod](https://github.com/adityatelange/hugo-PaperMod/)!  

### What this is

This blog and all the posts in it first and foremost reflect my thoughts and experiences on various topics. Most of them will be concerned with the software I try out and use since I'm passionate about that and need a place to share it. 

However, I'm also vocal about privacy, especially online. If some of my posts seem like doomsaying or fear-mongering, please forgive me but I do believe there is reason for concern when it comes to this.

If you have any suggestions or questions, feel free to reach out to me in any of the ways available on this site.

### What this isn't

This is not meant as a space for me to preach anything to anyone. If what you read here is interesting for you, that's great. If not, that's also fine. But please be aware that this page does not exist to convince you of anything and there's a reason why this site does not feature a comment section or the possibility for external people to post. 
