---
title: "How To: Fix duplicated characters (and other artifacts) in your shell"
date: 2023-12-05T16:22:00+01:00
tags: ["How To", "Linux", "Tech"]
---

A few days ago I started noticing strange behaviour of my shell. When typing
commands or working with pasted ones, especially with tab completion, 
duplicated characters started 
showing up at the beginning of the line or in the middle in the case of
working with a pasted command. Something like `hx` became `hxhx` or `docker`
became `dodocker`.
These duplicated characters were not editable,
I could only clear the line and start fresh but the same thing would occur again,
although not quite deterministically.

I had never encountered something like this and was at a loss what might be the
issue since there are many possible culprits. Fortunately, I was able to quickly find the problem. The gist of it
is this: I use a non-alphanumeric unicode character as my prompt. This led to
my shell not being able to tell how wide the prompt was and thus started displaying
the things described above when something needed to be redrawn. This in turn was
because some of the locales were not set properly so the shell apparently didn't
know what encoding to use. This manifests itself in something like the following 
when calling `locale`:

```
locale: Cannot set LC_CTYPE to default locale: No such file or directory
locale: Cannot set LC_MESSAGES to default locale: No such file or directory
locale: Cannot set LC_ALL to default locale: No such file or directory
LANG=en_US.UTF-8
LC_CTYPE="en_US.UTF-8"
LC_NUMERIC="en_US.UTF-8"
LC_TIME=es_ES.UTF-8
LC_COLLATE=C
LC_MONETARY="en_US.UTF-8"
LC_MESSAGES="en_US.UTF-8"
LC_PAPER="en_US.UTF-8"
LC_NAME="en_US.UTF-8"
LC_ADDRESS="en_US.UTF-8"
LC_TELEPHONE="en_US.UTF-8"
LC_MEASUREMENT="en_US.UTF-8"
LC_IDENTIFICATION="en_US.UTF-8"
LC_ALL=
```

As for why this happened... I'm not sure as it only started recently but I assume it
has to do with the fact that I'm running my shell within a podman (or distrobox, to be
more precise) container and I updated my host OS a couple of days ago. For some reason
the locale setting seems to have gotten messed up. However, the fix is rather easy.
Just add the relevant locale to `/etc/locale.conf` and regenerate them:

```
  echo "LANG=en_US.UTF-8 >> /etc/locale.conf"
  locale-gen
```

Since my distrobox container is rebuilt automatically every day, I just added these
commands to the `Containerfile` and done.

I thought it was interesting how something so weird could have a root cause that I 
would never have guessed myself but still has a very easy fix.
