---
title: "How it all started"
date: 2021-09-01
tags: ["Privacy", "Personal"]
summary: "How my journey into the world of online privacy began"
aliases:
  - "/posts/privacy/privacy_start"
  - "/posts/privacy/privacy-start"
  - "/posts/privacy_start"
---

#### Back then ...

I remember that I used to dismiss privacy-concerned people (of which I knew basically one)
as paranoid. I used to be one of the people who asked the rhetorical question of "But who 
would care about my data?" followed by "I don't have anything to hide". This was my point 
of view some three years ago.

#### ... and now

Fast forward to now: 

- I have banned everything Facebook, Google and Microsoft from my household.

  - All my desktops/laptops run Linux

  - All my phones run a degoogled version of Android

  - I never had any Apple devices but I had had some, I would've banned them too

- I basically don't use any mainstream social media anymore (though I was never an avid 
  user in the first place)

- I host my own data storage solution so I don't have to rely on third parties for my 
  files. 

- I filter all network traffic in my home network and my phone to get rid of tracking 

- For daily browsing I mostly rely on the [Tor browser](https://www.torproject.org/)

The list could go on for a while, especially considering the technical details but I think
the picture is clear. Many people would (and do) consider this extreme but I'm not the 
kind of person to go about such projects half-hearted and this is precisely what I
consider taking care of my online privacy to be: *a project*. One that I go about as 
meticulously as I can.

#### Beginning with passwords

So how did I get here in the last three (or so) years? It all started with the realization
that my approach to managing my passwords for the various services I had signed up for 
over the years was woefully inappropriate.  
Like many others out there I used two or three passwords for all the dozens of online logins I had. What's more, the passwords were all 
terrible unsafe and would not have stood up to brute-forcing them by even the most 
moderate computer hardware for any noticeable period of time.  

The solution to this particular problem is easy and one that I recommend to **everyone**: 
password managers. A program that stores all your passwords in one safe place, all you 
need to remember now is one master password.

I opted for [KeePassXC](https://keepassxc.org) which is a great program but it lives 
locally on your machine and is not automatically synced between devices.  
So what to do? 
Obvious answer: put it in a cloud and let that take care of syncing the password database 
between your devices.  
But which cloud to use? For some reason I was less inclined to trust
GoogleDrive, OneDrive or even Dropbox with my password database. Sure, it's encrypted but
still the thought of all my passwords sitting on someone else's computer didn't feel 
right to me. After all, if someone manages to break into this database, they get access 
to your whole online presence and can do whatever they want with it! And can I really 
trust companies like Google and Microsoft to keep my whole online life safe?

#### The slippery slope

This was how I started to think about this key concept of privacy and security: *trust*. The key question is then:

> **To whom can you entrust your data?**

This is how I started down the rabbit hole. Ever since then I've looked for answers to 
that question and ways to follow through with them. And since I'm the kind of person to 
explore all nooks and crannies of a hole I have committed to, I'm still running around in it.  
It has been a most interesting journey to say the least and I have both enjoyed and 
learned a lot from it. The only thing I resent is the fact that I had to do it in the 
first place to feel safe.

#### My bottom line

Obviously, there's a lot that could be said about this topic but my personal bottom line (so far) is this:

- Absolute digital data safety or privacy does not exist.

- Promises about trustworthiness are a dime a dozen (*"We care about your data"*).

- Transparency is key.
  - Transparency reports or similar help.

  - How a company responds to privacy concerns or similar is often telling.

  - Open source software is more likely to be trustworthy because it can be audited (i.e. 
    read and checked) by anyone.

- Trust in this area needs to be earned, not given without question.

- When feasible, do it yourself (can be a lot of fun, too).
  - Service hosting

  - Storing passwords

  - File syncing

- When in doubt, don't use it.

As of now, I'm happy with the place I'm in, privacy-wise, but I'm also curious what the 
future will bring. We'll see.
