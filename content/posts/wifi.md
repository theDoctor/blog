---
title: "How To: Choose a specific WiFi radio band on Linux"
date: 2023-11-29T21:07:03+01:00
tags: ["How To", "Linux", "Tech"]
aliases:
  - "/posts/howto/wifi"
---

Sometimes I don't work from home but at my family's or a friend's place.
Turns out the router at one of these places is terrible and can't reliably
provide WiFi for my laptop sitting some 4 meters away (with 2 doors in between).
I thought this was rather odd because my phone didn't struggle with this
so I asked around and thought about it some and the most obvious culprit
seems to be the WiFi radio band that's used.

Current routers will use either a radio band frequency of 2.4 or 5 GHz
and a client should be able to select the best choice for itself which
clearly isn't happening in this case. 
So I went and checked the frequency in use
when the WiFi was struggling and sure enough it was 5 GHz. Usually, this
would be the preferred one because it's more stable and allows more
throughput (as far as I know) but it also has less range. So I wanted to
try and select the 2.4 GHz frequency exclusively and see if that would do
the trick and what can I say? It did! Here's how:

I have to preface this solution with that it will only work, if you're using
NetworkManager to manage your WiFi. If you do, you should have an
application called `nm-connection-editor` installed. Then you have to:

* Start `nm-connection-editor` from a terminal
* Select the relevant SSID
* Click on the cog icon to edit the connection
* In the tab "Wi-Fi" find the "Band" option and select the one you want
* Click "Save"

Done!

