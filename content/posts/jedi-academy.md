---
title: "Playing Jedi Knight: Jedi Academy again"
date: 2023-12-07T08:24:23+01:00
tags: ["Gaming", "Linux"]
---

"Jedi Knight: Jedi Academy" celebrates its 20th anniversary this year!
Reason enough to dust it off and play it yet again. This time I went to
hunt for mods to make it more interesting this time around and I found
[JKHub](https://jkhub.org/mods/).

This has a list of recommended mods for the game [here](https://jkhub.org/mods/jediacademy/)
featuring cosmetic ones, new textures, new lightsabers, new character
models but also conversion mods, new missions and more!

But first things first: installation. I own the GOG version of the game
so I used Lutris to install the game with [this](https://lutris.net/games/install/8250/view)
script. It makes use of [OpenJK](https://github.com/JACoders/OpenJK), an open-source
reimplementation of the game engine which also has a Linux build so
you can now play this game natively on Linux! What a time to be alive!

I picked a few entries from the list of recommended mods which seemed
worthwhile, notably:

* [JAEnhanced](https://jkhub.org/files/file/2550-jedi-academy-enhanced) (includes other optional mods)
* [HQ Textures](https://jkhub.org/files/file/1528-the-jedi-academy-texture-overhaul-full/)
* [Jaden Remastered](https://jkhub.org/files/file/3622-jaden-korr-remastered-from-movie-duels-10/)
* [Better human female customization](https://jkhub.org/files/file/1770-scerendos-female-jedi-customisation/)
* [Coruscant](https://jkhub.org/files/file/2700-coruscant-sky/) and [Korriban](https://jkhub.org/files/file/2863-new-korriban-sky-music/?tab=comments) reimagined mods
* [Single player missions expansion](https://jkhub.org/files/file/3838-single-player-missions-expansion/)

I'm now jumping around as a female Chiss with a white-ish lightsaber featuring
the hilt of Darth Revan. Initially I wanted to go with one having a crossguard
Kylo Ren style but it keeps on logging errors to the screen which is somewhat off-putting.

I haven't gotten to the part where the missions expansion comes into play so
I'm looking forward to this. I'm also using most, if not all, of the mods optinally
included in JAEnhanced. So far I'm having a good time and it's quite close to the
original game that I've played so many times already. It'll be interesting to explore
more of the conversion mods. I might report on them at a later date.

One thing to note: I came across a bug where cutscenes would not play properly
which apparently has to do with screen refresh rates. To fix it I opened the menu and 
in "Setup -> Video" set "Video Sync" to "On". That did it.

Happy lightsabering everyone!
