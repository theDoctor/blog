---
title: "Integrating analytics into my website"
date: 2023-12-16T13:31:33+01:00
tags: ["Privacy", "Tech"]
---

I've been blogging (on and off) for a while now. Originally, I just
wanted a place to write down things I'd done, tutorials, idle thoughts,
stuff like that. But since I'm also active on Mastodon (or recently GoToSocial)
and link to my posts there I started wondering if anyone ever actually
reads my posts or if I'm shouting into the void here. You know,
idle curiosity. The obvious answer to that question comes in the
guise of website analytics.

Now if you know anything about me, you probably know that I'm not
the most ardent admirer of Google and its products so the pervasive
and ubiquitous Google Analytics is not an option for me. 
Also, it would necessitate to inform visitors of the fact that
they're being tracked and asking for constent since Google Analytics
is nor compliant with the [GDPR](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation)
which is a big no-no (in case you weren't aware).

There are alternatives, though, and I am aware of two: [Plausible](https://plausible.io/)
and [Umami](https://umami.is/). I knew of Plausible for a while now
and always admired the product, I only found out about Umami very
recently after a fellow Mastodonian suggested it to me.

Essentially, they offer pretty much the same service: You create an
account with either platform (both offer free plans, the one at 
Plausible is limited to 30 days) and insert a snippet of JavaScript
into your page headers. That's it.

A dashboard showing various metrics is now available to you on the
website of the respective service. Metrics such as:

* Number of page views
* Number of visitors
* Number of unique visitors
* Referrer
* Country, region and city of origin (based on IP, I presume)
* Entry and exit page
* Page view duration
* Bounce rate (i.e. rate of people looking at only one page and then leaving)

It's needless to say that all the date collected is anonymous which is why
it's not necessary to ask users for permission to do this. 

Both services provide mostly the same metrics with some slight differences.
For example only Plausible tells you the entry and exit page of a visitor.

Awesome! Since this was the very first time I'd ever done something of the
sort I found myself looking at the stats page quite a lot and clicking my
way through various aggregations. During the first two weeks after setting
this up my page was viewed ca. 100 times (not all unique visits, though).
Many of the visits were from Germany and the US but there was also the
odd Indian, Serbian, Polish. 

Something that was quite interesting to me was the uptick in page views
after I posted a link to [one of my posts]({{< ref "gothic.md" >}}) 
on Reddit but there was more. I noticed a referer coming from a specific
GitHub issue where someone had asked a question about Gothic 2 (a game
I like very much) and an answering post had linked to my blog post about
the game which the poster had presumably seen on Reddit. I was thus able
to pick up on it and offer assistance, if needed.

So far, so good but there's a caveat. Embedding a piece of JavaScript
into my web pages means that now there's third party script loaded on
every page view which is something many privacy-minded people (like myself)
automatically block. There's a dilemma here: Should you just accept that
the data is going to be incomplete and call it a day or try to do something
about it which would, however, mean tracking people who might not actually
be okay with this, even if it's all anonymous?

I decided to eliminate both issues as best I could by simply self-hosting.
Since both Plausible and Umami are open-source there's nothing stopping
anyone from just taking the code and running everything yourself. Of course
that's more hassle, maintenance work and so on. I still decided to try it
and see how it went.

I went with Plausible because I like the UI and the metrics a tad more.
They provide straightforward [documentation](https://plausible.io/docs/self-hosting)
for self-hosting so setting up should be easy enough. The next question is
where to host. In principle I could do it on my [home server]({{< ref "server.md" >}})
but I don't like to expose services to the internet with it and a
self-hosted instance of Plausible needs to be accessible by the website
you want to use it for.

So I went and rented a VPS. For the very first time in my life I decided to
spend some money on a virtual server. I went with netcup because I read
good things about them, they're located in Germany as are the servers
and a price of 3,50 € per month seemed quite reasonable for 2 CPUs, 
2 GB of RAM, 40 GB of storage and more traffic than I can ever expect to
need.

I got a fresh server running Debian 12 which suits me fine since it's the
distribution I'm most familiar with on servers anyway. I took care of ssh keys,
user management, firewall, installation of dependencies (namely docker and
docker-compose), the works. 
I then downloaded the files from Plausible's [GitHub](https://github.com/plausible/hosting),
looked at them, changed exactly nothing, created a systemd unit for running
the service and started it up. In order to access it via the web I created
a DNS A record pointing at the IP address of the VPS and, voilà, there
was an admin login page. 

Naturally, this was still via HTTP which is a bad idea to use in production
so I went ahead and installed Caddy and configured it as a reverse proxy
for the service. The nice thing about Caddy is that it does everything
automatically, you just have to tell it the (sub)domain you want to proxy and
where to send the traffic (see [here]({{< ref "self-hosting.md" >}}) 
for more detail).

Now I created an admin account and created an HTML snippet for the tracking script
which now pointed to my self-hosted install at a subdomain of `bfloeser.de` which
means that the script is now a first-party script and thus less likely to be 
blocked. A default installation of uBlock Origin (which I'd recommend to *everyone*)
now doesn't block the analytics anymore which it did previously. Mission accomplished!

So what now? I look at the number of people who visit my website from time to time
and browse through the countries they hail from. Quite diverse! At the end of the day
this was a fun exercise made easy by the great work of Plausible to provide
sane defaults for self-hosting their stuff. Also working with a VPS was a nice
change. Will I keep this around? I don't know. I'm probably not going to bother
backing the whole thing up because it's just not important enough to warrant the
work it would take. I might take everything down after a while because... in the
end page views don't mean much. I set out to answer the question whether anyone
reads what I write and as far as that's possible the question has been answered.
I hope what I write is instuctive, amusing, useful and/or entertaining to at 
least some of the people who come here and if that's actually the case, the effort
is worth it. This, however, is not something analytics can tell me. Only real
people can.

If you've made it here, I'd appreciate feedback, should you have some to give.
Do you mind being tracked like this? Do you care? Should I give the users a heads-up
even though I don't have to? Is all of this pointless number-crunching? I'd
love to hear from you!
