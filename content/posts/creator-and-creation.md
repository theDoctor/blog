---
title: "On Separating Creator from Creation"
date: 2025-01-31T13:47:38+01:00
tags: ["Personal"]
---

This post has been in the making for a long time now. Not so much on paper (metaphorically speaking) but rather in my head.
So now the time has come to finally write down my thoughts. They may be meandering or a bit stream-of-consciousness
but I'd rather have that than nothing. This may also end up quite verbose. You have been warned.

I was prompted by various events in the past, online discussions and, last but not least, one of Kev Quirk's [blog posts](https://kevquirk.com/blog/on-virtue-signalling)
about this subject. It is also related to a [post series](https://rldane.space/dont-use-what-works-for-you.html) of a dear fedifriend of mine.

As I see it, there are two basic schools of thought, on at either extreme of a spectrum:

1. *Always* separate creator from creation
2. *Never* separate creator from creation

This might seem trivial at first but I think, in practice, it is anything but. Let me explain what I mean
by "creator" and "creation". The creations can be all kinds of things.

* Video games
* Novels
* Science papers
* Movies
* Politics
* Nation states
* Furniture
* Houses
* Stand-up comedy
* Software
* ...

In analogy, creators can be all kinds of people:

* Developers
* Writers
* Politicians
* Family members
* Psychotherapists
* Religious leaders
* Craftspeople
* Comedians
* Actors/Actresses
* ...

You get the idea. As a simple thought experiment let's examine what it would mean to actually stand by one of the
extreme positions mentioned above. If you always separate creators from creation, that will naturally lead to
a lot of perceived freedom. You could vote for whoever, buy whatever from whomever, use whatever product, physical
or otherwise, you like, all based on personal preference, functionality or whatever criteria matter to you.
However, it also doesn't take much imagination to find flaws in this approach: it will also lead to interacting
with people and companies of questionable or objectionable morals and practices. What might that be?

* Child labour
* Voter suppression
* Worker exploitation
* Environmental pollution
* Persecution
* Segregation
* Racism
* Fascism
* Sexism
* ...

That sounds (and is) rather drastic but, again, it doesn't take much imagination to draw the line between these things and
a product or a person I have relations with. Many companies produce products in China, an authoritarian regime,
under conditions that are not in line with labour laws and regulation in my own country. Many companies have been
shown to lie, cheat, subvert, bribe, ignore, outsource and what not to their hearts content to maximize profits.
Is this your fault? No. But by buying from a company engaging in any of this you give them money and support them.
Does that make you complicit in their crimes? Isn't voting with my wallet the most meaningful signal I can send
in such contexts? In my opinion, continuing down this path relentlessly leads to an unethical, immoral and indifferent
lifestyle. One that does not care for the opinions, woes and injustices of others. One that never takes a stand
on anything of a moralistic nature.

Let's look at the other side of the medal and think about what it means to *never* separate creator from creation.
This would entail *a lot* of legwork to be done before you ever engage in relations with basically anyone or anything.
Does your coworker maybe believe climate change is a hoax?
Does your neighbor maybe vote for a far-right party?
Does the manufacturer of your hair dryer exploit their workers?
Does the car you'd like to buy unduly pollute the environment?
Has the developer of that application you like been accused of sexual assault?
Has the CEO of the place you buy groceries from evaded taxes? Molested a coworker? Beaten their child?

I could go on forever. Obviously, continuing along this line of thought is completely out of the question in practice.
Not only would it entail investing an amount of time and energy that no one has but would also, realistically speaking,
result in not being able to rely on anyone or anything. Because if you just dig deep and hard enough, you'll be able
to find something objectionable about anyone and anything. What's more, morals are subjective. What I might find
objectionable may be acceptable to another person. Also, how should I define the cutoff of what's acceptable and
what isn't? Who am I to moralize to others? Am I not just like them?

What does that mean? I think it means that both of the two extremes outlined above are impractical or untenable for
different reasons but just as absolutely. This leads to the conclusion that any real human being must find their way
somewhere in the middle. How? Now *that* is the actual crux of the matter. We need to navigate between two opposite
poles, one of morals and ethics and one of impracticality and unfeasability.

My assertion now is this:

> The path each person takes in this area of conflict between two extremes is, by necessity, individual.

I've mentioned this above and I will again: we're all different. No two people have the same opinions, values,
morals, and many other things. It would be deeply illogical to assume that, somehow, everyone should take the
same stance in this question.

Does that mean, we should just say "to each their own"? No, that is not what I think or believe. My second assertion
would be:

> Due to the individuality of people *and* of the issues that arise in this context, each person must decide what stance
> to take on the herein discussed separation (or lack thereof) on a case-by-case basis.

But doesn't that still imply that everyone should just do as they please, as long as we leave each other be? It does
but I'm not finished. Here is the third (and last assertion):

> Each case-by-case decision must be made in the proper context, taking into account individual factors, but also
> social ones as well as scientific ones.

What do I mean by that? I mean that not every case-by-case decision is made on equal footing. Sometimes there is a
larger context at play that should make the decision fairly obvious, sometimes there isn't. Let's walk through a few
examples to illustrate my point:

Buying from obviously evil companies is something that should clearly not be done, right? One of the few ways regular people
have to make their stance known in such cases is "voting with their wallets", i.e. not buying from someone they
cannot, in good conscience, support. Examples for evil companies? Nestlé, Monsanto, Exxon just to name a few.
There is clear, well-known evidence for child labour, climate change denialism, exploitation, deliberate poisoning,
monetizing basic human rights and goodness knows what else going on here. This should be a clear-cut case, right?
Well, no. Sometimes it's difficult to get around such companies because they're *everywhere*. Maybe you cannot
afford not to buy from them financially. Maybe you're unaware. Maybe their specific brand of evil only touches
topics you don't care about. I mean, no one has the capacity to care about *all* of the injustices done everywhere.

Then there's politics. What happens if you like the creation of a person that has completely different political views
than you do? What if they're an anarchist, socialist (whatever that means to you), marxist, extremist, nazist...?
Should you draw a line? If so, where? What is an acceptable difference in opinion and when does it become more
than that? Should you comsume the creation of politicians whose politics you find objectionable. Adolf Hitler was
a painter before he went into politics. Is it okay to enjoy his paintings? What about Elon Musk's feeble attempts
at poetry? The personal restaurant chain of that member of a far-right party?

Let's take it down a notch. What about companies or people you *don't* buy from but use their stuff "for free" (fun
fact: it's not actually free). You know, all those tech companies, Meta (aka Facebook), Amazon, Alphabet (aka Google),
Apple, Microsoft, OpenAI. The list is endless. But not only those, there are thousands or millions of projects out there,
e.g. software, that don't necessarily have a company behind them. What about those? Do you support them, if you use
their stuff but nothing else? No donations, no social interaction, nothing. Is that fine? How about all the
developers who have been branded as toxic, fascist, sexual predators, and all kinds of things. Should you use something
else out of principle? Sometimes that's difficult because the best-in-class product for your use case is made by
a maniac. Or a rapist. Or a conservative weirdo. Or a furry. Or a an autistic, ADHD, trans, poly (insert something else here)
person. Some might object to some of these people for various reasons, others won't.

Side note: Please note that I explicitly do not condemn, disapprove of or otherwise object to people with neurodivergences,
disabilities, LGBT(please add appropriate letter or characters here) people, furries or any sexual orientation. But God knows,
there are enough people (that are not me!) who do.

Feel like it's getting complicated and bogged down in details? But wait, there's more! What about companies that you
don't approve of but you happen to know good people who work there. What about companies that have an admirable mission
that you do approve of but their CEO posts weird and kinda icky stuff on Twitter (I mean being on there is bad enough, I suppse)?
What about movies that you enjoy but one of the actors is a scientologist (guess who I might have in mind)? What about
open-source projects that churn out amazing software but have a clearly toxic community? What about testing on animals?
Processing animals for eating? What about copyright infringement, gobbling up data for AI model training? What about...
What about... What about...?

The world is large and strange and loud and confusing. And there's just so *much* of it. There's so many people of all
shapes and sizes and all of them are different. Different values and different priorities. Personally I lean towards
the side of not separating creator and creation as much as I can or rather I try to make ethical decisions in what I
consume as much as I can but I won't lie: it can be exhausting, difficult, draining. But also necessary for me.
I think ethics matter, I think morals matter, I think ideals matter. We *need* those to have any chance of coexisting
reasonably peacefully. Because of that I

* ripped out anything connected to Facebook from my life
* avoid Google as much as I can
* try not to buy from Amazon
* *never* buy from Apple
* avoid the hell out of Microsoft
* try not to buy from Nestlé
* do not use traditional social media
* avoid watching movies with Tom Cruise

And a few more things. As you can plainly see I care about software I use. But on the other hand I

* eat meat
* sometimes *do* buy from Amazon
* bought Hogwarts Legacy (if you're interested, look for JK Rowling and controversy about trans people)
* read Harry Potter books (see above)
* use GrapheneOS (although the community appears to be toxic)
* consume YouTube (albeit via third party frontends and apps)
* use Proton (in spite of the recent controversy concerning their CEO)
* drive a car
* use plastic wrappings more than I need to
* buy things I don't necessarily need

So where does that leave me? You could make a reasonable case against me accusing me of duplicity. I think that'd be fair.
But I also believe the same is probably true of most, if not all, people. I'm trying to do the best I can with what time,
energy and resources I have available to be the best person I can reasonably be right now. Sometimes I'm not satisfied
with the result myself but it's what I've got.

And where does that leave you? That, dear reader (should you have made it this far), is entirely up to you. You are not me
and you will care about different things than I. Nonetheless let me encourage you to use and consume what is good. Good for
you, for others, for our planet. Sometimes it's obvious what that means, sometimes not so much but please make the effort
as much as you can. Take a stand *somewhere*. Find a hill to die on (figuratively speaking). Don't let convenience and
apathy burn it out of you. Strive to be better, whatever that means to you.

A final note to the people who firmly insist that you should *always* separate the creator from the creation: stop being
such a lazyass! Come out of your bubble of convenience and lack of responsibility! Because following that line of reasoning
to its logical conclusion will lead us all into the abyss. And no, that is not hyperbolic.
And if you still won't amend your point of view after considering all of this, allow me kindly to punch you in the face because,
by God, you deserve it.
