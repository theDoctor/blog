---
title: "My Gaming on Linux"
date: 2022-03-22T15:13:40+01:00
tags: ["Gaming", "Linux"]
aliases:
  - "/posts/gaming/onlinux"
---

A few years back I made the full switch to Linux for all of my personal
machines for a couple of reasons (which you can read about
[here](https://blog.bfloeser.de/posts/privacy/os/)). For the most part this was
a painless transition which came with many more upsides than downsides for me
and I was quite happy about it. 

However, one big deterrent for many people is the gaming situation on Linux.
Since I am a non-professional gamer myself and this is one of my primary hobbies
this was a potential issue for me and I want to write about the experience a bit.

I'm not going to go into all the details as there are plenty of great and
not-so-great resources to draw on out there but I will say that gaming on Linux
is actually a thing nowadays. I feel a bit silly writing this as I only
discovered Linux quite recently after Valve had already released a native client
for Steam and Proton to go along with it so I never experienced many of the
pains Linux users had to put up with in the past.

When I started looking into the matter there were already plenty of tools
available out there, like Proton and Lutris and more emerging like Gamehub, the
Heroic launcher, Gamebuntu and probably many more. Navigating this whole jungle
can be a bit daunting at times and identifying things that you need or would
want can be nontrivial. Also, despite being a gamer, I'm only a hobbyist and not
terribly familiar with all of the technical lingo more professional or
enthusiastic people throw around. Basically, I want my games to run and not
worry about much else.

This is somewhat at odds with my general computer use nowadays because, in
general, I don't mind having to tinker and configure stuff, I even like doing it
(to a point). But for me gaming is something I do in my free time (of which I
don't have an overabundance anymore) and I don't want to waste it
troubleshooting GPU driver problems or something. Speaking of GPU drivers, I
have an NVIDIA card which is (in)famously collectively hated on by the Linux
community but I bought the thing before knowing or caring about these issues.

Still, all things considered, my gaming experience is relatively smooth. In
hindsight, this seems a bit strange, even to me, but there are a few mitigating
factors that work in my favor:
- I usually don't play recent games
- I favor single-player games
- I tend to replay the games I already have a lot

These facts already circumvent a lot of the problems other people have with
gaming on Linux. Since I don't do multiplayer stuff anticheat software is no
issue for me. Since I (generally) don't play recent titles I don't have to wait
for driver support or updates for Wine/Proton to emerge so I can start playing.
So, generally, things like installation scripts for Lutris or patches to Proton
are already available for the games I own. Also, since I replay many of my games
I can set those up once and reuse any tweaks, configs or whatever was necessary
to get them running and be set.

With all that said, I still need to solve problems every now and again. I have
turned to Steam for most of my gaming because it (arguably) provides the most
frictionless experience for gaming on Linux but some games are just not
available on Steam or I already own them on a different platform (like GOG).
Others are so old that it's difficult to get them to run on a modern PC which is
a problem on Windows as well. On Linux you have to deal with that through the
added compatibility layer that is Wine/Proton and somehow make it work.

As of now I can play pretty much all of the games I own and want to play on
Linux. Some of them were a pain to get going but only very few. Doing it this
way provides me with an unlooked for benefit:
Getting games to run now feels like an achievement. I know it's not meant to be
this way but whenever I successfully run a game on Linux I feel great about it,
even more so when the process was difficult. I've even installed quite a few
games I didn't intend to even play just because I can and I love the feeling.

With the advent of the Steam Deck Linux gaming will probably see improvements in
terms of accessibility, compatibility, tooling and the like and I won't complain
if this leads to more developers targeting this platform or at least making it
less of a hassle to run Windows-only games with Wine.

I think, I'm in a good spot right now when it comes to my gaming resources. I
stopped distrohopping, I spend little time worrying about getting some specific
game to run and generally just fire something up and play. That's enough for
now.

