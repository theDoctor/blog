---
title: "Why I dislike Rockstar Games now"
date: 2023-09-15T17:06:19+02:00
tags: ["Gaming"]
aliases:
  - "/posts/gaming/rockstar/rockstar"
---

Dear reader,

I recently had an experience so bizarre that I feel like sharing it with the
world wide web for all the good that will do. Take it as my way of blowing off
some steam. Without further pretext here goes...

A while ago I bought Red Dead Redemption 2 (RDR2) on Steam. I own a Steam Deck
and read many good things about the game and that it runs pretty well on the
Deck. Also it was on sale for about 20 bucks so I figured I should go for it. It
sat in my library for a while because I had other games I wanted to finish
first. Recently I did so and proceeded to download and install RDR2. It quite a
chonker so it took me a while.

Upon launch I found that I needed a Rockstar Games Social Club (RGSC) account
for being able to play. I had forgotten about that since it's been a while since
I bought it. It annoys me quite a bit how so many game publishers now have their
own launchers (many of which suck, especially on Linux) and require you to have
yet another account no one actually wants. But I digress. Fine, I thought, I'll
create an account. I did so, entered the credentials in the launcher and wanted
to start playing when the launcher told me that my Steam account is already
linked with another RGSC account which in turn is linked to a specific email
address which it showed me. I did recognize that address as it used to belong to
me but I deleted it years ago. I'm also pretty sure that I never consciously
created a RGSC account and definitely never bought anything through it. My best
guess was that I either created it for some other Rockstar game and then
completely forgot about it or the data was somehow transferred there from
another service or something. Mysterious.

What to do then? I looked online for how to undo the linking of these accounts
since I don't have control over the existing RGSC account and can't log in to
it. Turns out that there's no way to unlink from a Steam account for the user,
you have to create a support ticket. It took me quite a while to find out how to
do that because the website is not great (to put it mildly) and you have to log
in to an existing account in order to create a ticket. Unfortunately, this is
not all that obvious so I wasted quite some time there. Finally, I figured out
how to do it when I saw the following in the list of preconfigured tickets:

![personal_info](/assets/personal_info.png)

See the item in the bottom left corner saying "Do Not See My Information"? Yeah,
so did I. I find it curious that they would include that there and just for fun
I created such a ticket. Haven't heard back from it. Wonder why.

Anyway, I then tried to create another support ticket only to realize that you
have to wait for three hours between tickets. Lovely. What a nice way to treat
customers.

After I waited for three hours the following conversation ensued over the course
of about two days. I will not include the attachments sent by me because it will
be made clear what they contain. Also, I'm not including opening and closing
formalities every time.
My messages are in code block format, the support messages in block quote
format.

```
Dear support team,

I am trying to play Red Dead Redemption 2 but have found that my Steam account
is already linked to a Rockstar Social Club account which in turn links to the
mail address "oldmail@domain.tld". I am quite sure that I never created that
account and this email address was mine but does not exist any longer.

Consequently, I would like for this account to be unlinked from my Steam
account. Instead I would like the account I'm writing this from to be linked to
my Steam account.

My Steam ID is: XXXXXX

As mentioned, the mail address of the currently linked Social Club account is
"oldmail@domain.tld" but I cannot access it and thus have no idea what the
nickname might be since I've never interacted with it.

I'm attaching a screenshot of my Steam profile which also shows the ID and my
ownership of the game.

Best
```

> Hello,
>  
> Thank you for contacting Rockstar Support.
>  
> The entitlements to play GTAV are tied to both the Social Club and Steam
> accounts. These accounts must remain linked or you will not be able to play
> GTAV or GTA Online with either of these accounts.
>  
> If we were to unlink your Steam account from your Social Club account, you
> will not be able to play GTA Online as well as GTAV with your Social Club
> account, even if another Steam account is linked to it afterwards.
>  
> However, if you would still like to proceed with the Steam unlink, please get
> back to us with the following information:
> 
> 1. The Social Club nickname you don’t want to be linked to Steam anymore.
> 2. Screenshot showing you logged into Steam, with your Steam Profile name
>    and Steam ID64. 
> 3. The Steam ID64 typed into the text body of the request. 
>    * Sign in to Steam Click on your Steam Profile name at the top right
>      corner of the window and select “Account Details." 
>    * Take a screenshot showing your Steam Profile and Steam ID64 clearly
>      visible on the screen Copy and paste the Steam ID64 in your reply 
>    * Confirmation that you understand that unlinking the accounts will
>      result in being unable to play GTA Online as well as GTAV with
>      either of these accounts.
> 
> Once we have this information, we can continue with your request. We greatly
> appreciate your patience and cooperation during this process.
>  
> Best regards, Rockstar Support
 
No idea what the deal with GTA V is. I don't own it, have never played it and it
shouldn't be associated with my account.

```
I do not understand why my account should be linked to GTA V or GTA Online
in any way since I have never purchased either. Also, as detailed in my original
request, I did not create the Social Club account myself and would like it to be
deleted or at least unlinked form my Steam account. 

I provided all the necessary information in my original request. The nickname
for the Social Club account is unknown to me as I did not create it, I only know
that it appears to be linked to "oldmail@domain.tld" which does not exist
anymore.
```

> We have reached out via email to the email address associated with the account
> that you would like to change the information on. Please respond directly from
> the email and reference this ticket number.
> 
> Once we receive a response from that email address, we can proceed with this
> request.

Well, good luck with that. That email account is dead, remember?

```
As detailed multiple times now I cannot do this because the associated email
address does not exist anymore. Or rather if it exists again, I do not own it.
The only proof I have is that I own the Steam account linked to it.
```

> Please provide the following information in order to verify ownership of the
> Social Club account.
> 
> 1. Approximately when did you purchase in-game currency for GTA Online or Red
>    Dead Online?
> 2. What was the full email address that was originally associated with the
>    account?
> 3. What was the original nickname of the account?
> 4. When was the email address changed on the account?
> 5. When was the account linked to PSN, Xbox Live, Facebook, etc.?
> 6. When was 2-Step Verification enabled on the account? Once we receive the
>    above information, we will investigate this further and get back to you as
>    soon as possible. 

Now things are starting to go off the rail.

``` 
Allow me to reiterate once more: I did NOT create a Rockstar Social Club
account before this one I'm using to send support requests. I have NEVER
previously interacted with Rockstar Social Club, did not link any accounts to it
or purchase any game or in-game content on it. It is a mystery to me how a
Social Club account can be linked to my Steam account but either there was a
mistake at some point or someone hijacked my old email address.

As for your questions:
1. Never
2. oldmail@domain.tld
3. Since I have no access and didn't create it, I don't know
4. See 3
5. See 3
6. See 3 
```

I started to get a bit snarky there, I think. I wonder if anyone could tell?

> We can help with this, but we need a one last information. Please provide us
> with the following information:
> 
> 1. Screenshot showing you logged into Steam, with your Steam Profile name and
>    Steam ID64.
> 2. The Steam ID64 typed into the text body of the request.
>    * Sign in to Steam
>    * Click on your Steam Profile name at the top right corner of the window
>      and select “Account Details.
>    * Take a screenshot showing your Steam Profile and Steam ID64 clearly
>      visible on the screen
>    * Copy and paste the Steam ID64 in your reply.
> 
> As soon as we receive this information, we will investigate and reply
> regarding the issue as soon as we can.

Progress? We'll see.

``` 
My Steam ID is XXXXXX. I'm also attaching a screenshot showing the requested
information. Please let me know, if anything is missing.
```

> We can help with this, but we need a few additional pieces of information
> first. Please provide us with the following information:
> 
> * Screenshot of the game purchase receipt received in your email account
>   showing the date and amount of purchase.
> 
> As soon as we receive this information, we will investigate and reply
> regarding the issue as soon as we can.

Who the hell keeps these? I certainly don't as my mail storage would fill up in
no time if I did.

```
Unfortunately, I did not keep the email I received after purchasing the
game. I'm attaching a screenshot of the purchase in my Steam account which has
the requested information
```

> We can help with this, but we need a few additional pieces of information
> first. Please provide us with the following information:
>  
> The below information request is applicable if you own Grand Theft Auto V.
> 
> 1. Screenshot of the Steam CD Key window clearly displaying the code.
> 2. The activation serial key typed into the text body of the request.
>    * Right-click on the game in your Steam Library, select "Manage" and select
>      "CD keys"
>    * Take a screenshot showing your Steam Profile and CD Key clearly visible
>      on the screen
>    * Copy and paste the CD Key in your reply
> 
> As soon as we receive this information, we will investigate and reply
> regarding the issue as soon as we can.

At this point I was beginning to wonder if I was talking to a chatbot or
something. Or maybe someone who doesn't actually speak English. Or maybe both.

```
As detailed multiple times I do not own GTA V and have never purchased it.
```

> Thank you for your response.
>  
> Upon investigation of your Steam linked account, there is no Red Dead
> Redemption game on the provided Steam account. Can you please provide us with
> the details of the Steam account that you have purchased the Red Dead
> Redemption game on.

You've gotta be kidding me... I definitely have it installed. Did they just try
and look up my profile in a web browser or something? I bet they did.

```
I can assure you that I own Red Dead Redemption 2 on the given Steam account. 
I've got it installed right now. 

Account nickname: mynickname 
Account ID: XXX 
Steam ID: XXXXXX

I'm attaching a screenshot showing the game in my library. 
```

> We can help with this, but we need a few additional pieces of information
> first. Please provide us with the following information:
> 
> * Screenshot of the game (Max Payne 3) purchase receipt received in your email
>   account showing the date and amount of purchase.
>  
> Note: All pictures must be uncropped and showing all 4 corners of the screen.
>  
> As soon as we receive this information, we will investigate and reply
> regarding the issue as soon as we can.

What the hell?

``` 
This is in no way related to my inquiry and I fail to see the point of this.
I have given ample proof that I own the Steam account in question and I request
that it be unlinked from any existing Social Club account immediately. 
```

> Thank you for contacting Rockstar Support.
>  
> We completely understand your concerns, but we still need the previously
> mentioned information in order to verify the Social Club account to apply the
> necessary changes. Please provide us with a screenshot of Max Payne 3 purchase
> receipt received in your email showing the date and amount of purchase.
>  
> As soon as we receive this information, we will investigate and reply
> regarding the issue as soon as we can.

Fine, have it your way...

``` 
I did not purchase Max Payne 3 on Steam, I bought a retail version and
activated it on Steam afterwards. I'm attaching a screenshot showing my Steam
licenses. The entry for Max Payne 3 is at the top. 
```

> Your case has been escalated to the proper department for further review.
> Please note, cases are addressed in the order they are received. We appreciate
> your patience.

Okay, I guess. Let's see what happens next...

> Please provide the following information in order to verify ownership of the
> Social Club account.
> 
> * When was the account linked to Steam?
>  
> Once we receive the above information, we will investigate this further and
> get back to you as soon as possible.

Jesus H. Christ... Not again...

``` 
As mentioned *multiple* times I did NOT create that Social Club or account,
nor did I link it. Thus I do not know when, why or how this happened. Please
unlink it since I have no control over it and it does not belong to me. 
```

> Your case has been escalated to the proper department for further review.
> Please note, cases are addressed in the order they are received. We appreciate
> your patience.

Again? What, is it climbing up the ladder of people who don't want to deal with
this or is this just a polite way of saying "Fuck you, I don't care"?

> At your request, the Steam account was successfully unlinked from your Social
> Club account. If you have any additional questions or concerns, please do not
> hesitate to contact us and we will be delighted to assist you.

Just like that? Oooookay then.

And sure enough, I logged into my new RGSC account on the game launcher, linked
it to my Steam account and was able to start playing within a few minutes. I
still have so many questions:

* What even happened?
* Was I just talking to ChatGPT for most of the time?
* Did anyone even read my messages?
* What kind of customer support is this?

Not even mobile phone providers have support this shitty and that says a lot, at
least where I live. For a while in the middle of things I was seriously
considering whether I was in the middle of an elaborate phishing setup (happened
to me before) but for all I could tell everything was legit and whoever or
whatever was on the other side was just plain stupid.

If you've made it this far: what do you think of this tale? Do you have similar
experiences? Maybe you've had it much worse? Is this what we should expect of
tech support? Was it all just a bad dream? And who took the cookies frokm the
cookie jar?

Get in touch and tell me your stories, if you want!
