---
title: "On Impostor Syndrome"
date: 2022-05-17T14:24:38+02:00
tags: ["Personal"]
aliases:
  - "/posts/personal/impostor"
---

I recently read a [blog post](https://kevq.uk/the-expert-vs-the-impostor/) about 
impostor syndrome which prompted me to write down my own musings on the subject.
I'll try and not repeat what Kev has written on the subject too much (see link
above).

I've been struggling with a feeling of inadequacy for many years now. Sometimes
I wonder where it comes from but I cannot think of anything specific in my past
that seems like a likely cause for this. I don't have a history of abusive
parents who would belittle me at every opportunity, quite the opposite. My
mother went out of her way to compliment me on my achievements.

Also, I feel justified in saying that I'm not a stupid person so, logically
speaking, feeling stupid does not make a whole lot of sense in many
circumstances. So what gives? 

I recently realized that many of my coworkers suffer from the same problem. I
only became aware of it a short while ago because I have started my current job
in the middle of the COVID pandemic and for a long time there was no opportunity
to get in touch with any of them on a personal level. So when we got to talking
about presentations we all attended I found out that it was a common theme for
all of us to feel bad about ourselves in a work context.

It occurred to me that this is the exact opposite of the Dunning-Kruger effect
which can be summed up as "Unskilled and unaware of it". For all those who don't
know: the authors of the report which introduced this reasoned that people who
are unskilled or of below-average intelligence are not able to recognize their
own lack of skill **because** of said lack of skill. 

Conversely, highly-skilled people tend to overthink, constantly compare
themselves to others or idealized versions of themselves and evaluate their own
performance. They then often come to the conclusion that others are better than
they are because the others only ever share successes and not failures, doubts
and insecurities. In truth, however, everyone feels the same behind the scenes
so everyone feels like an impostor while actually doing good work.

Side note: of course there are people who are genuine impostors or who are not
good at what they do but this post is not about this kind of person.

Please note that this is a very broad, generalized, personal take on the
matter. I'm not willing (or able) to argue this in court.

On a more personal level, I think much of my difficulty with assessing my own
aptitude is the lack of a benchmark, so to speak. I have spent the last few
years in unique, interdisciplinary roles where I was, in some sense, alone in
what I did and also did not get a whole lot of feedback. Now, when someone who
is not my colleague pays me a compliment I brush it off in my mind by thinking
that they just don't know what they're talking about. 

When I started to learn coding this was only compounded because now I know more
about it than most of my direct colleagues because they never bothered with
learning but on the other hand I never received formal training and don't have
much experience with actual projects.

I recently had a job interview where this became clear (not in an accusatory 
way, mind you) and it was a sobering experience to me. I tend to think that I'm
good at my job, not because I'm somehow brilliant or anything but because I want
to do the best I can and thus make it a point to learn more about what I'm
working with and trying to improve my skills and understanding. However, this
cannot replace years of experience in the software development industry. Duh.

I suppose what sets me apart is not that I'm an expert in anything particular,
at least not in anything I would get paid for (is Star Wars nerd lore
CV-relevant?) but rather the combination of skills and knowledge that is unique
to me. Naturally, the same is true for every other person on the planet, we're
all unique after all. I just have to find a niche that has a big-enough overlap
with what I can do and someone who is willing to pay me for it. 

How hard can that possibly be?
