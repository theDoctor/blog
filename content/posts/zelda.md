---
title: "How To: Play Zelda - Majora's Mask (and Ocarina of Time) with 4K Textures on the Steam Deck"
date: 2023-12-05T09:17:11+01:00
tags: ["How To", "Gaming"]
---

I recently decided to revisit the childhood memory of mine that is "Zelda - Majora's Mask"
on my Steam Deck. I've never owned a Nintendo console and was only ever able to play
at a friend's place or watch them play. I figured that after so many years it
was finally my turn to play, especially because I now have a console (sort of) that
is quite powerful at emulation.

This post is meant as documentation for future reference and closely follows 
[this post](https://overkill.wtf/how-to-play-nintendo-zelda-ocarina-of-time-and-majoras-mask-in-4k-on-steam-deck/).
There are a couple of ways you can do this, I'm gonna show mine. Also, I went with
the 3DS version of the game which looks very pretty and comes with a few very welcome
quality-of-life improvements but more on that later.

1. Install Citra. This can be done by hand or with something like [Emudeck](https://www.emudeck.com/). I chose the latter
2. Procure a totally, verifiably, absolutely legal version of the game
3. Copy the game ROM into `~/Emulation/roms/3DS`
4. Download textures from [here](https://www.patreon.com/collection/19146?view=expanded). You have to expand the post
   about the latest version and find the download link within. At the time of this writing, the latest version is 1.7.
5. Unpack the textures folder and copy the subfolder `user/load/textures/0004000000125500` into 
   `~/.var/app/org.citra_emu.citra/data/citra-emu/load/textures/0004000000125500`. If the target directories don't exist yet, create them. **Note**: The
   number in the directory name depends on the localization of the game. The one given here is valid for the US version. For the EU or Australian version
   you need to use `0004000000125600` instead and for the Japanese version you need `0004000000125400`.
6. (Optional but highly recommended!) Download the Project Restoration files from [here](https://restoration.zora.re/#setup). Do *not* download the HD HUD mod.
7. Unpack the Project restoration archive and copy the `code.bps` and `exheader.bin` files into `~/.var/app/org.citra_emu.citra/data/citra-emu/load/mods/0004000000125500`. 
   If the target directory does not exist, create it. You will note that there are three different versions of these files. You need the ones matching your
   game version, otherwise the game will crash. If the installation was successful, you will hear a sound effect right before the mask appears when you start the
   game. 
   **Note**: The same considerations about game localization as above apply here.
8. Start the ROM-Manager from inside Emudeck and add the game to Steam.
9. Start Citra in standalone mode (either from desktop mode or game mode) and select from the menu `Emulation -> Configure`. 
   Under `Graphics` toggle the `Use Custom Textures` option and set `Internal Resolution` to 3x native.
10. (Optional but highly recommended!) If you haven't already, you should set up the gyro controls for Citra. Install `SteamDeckGyroDSU` with this command:
   `bash <(curl -sL https://raw.githubusercontent.com/kmicki/SteamDeckGyroDSU/master/pkg/update.sh)`. Open up Citra, select `Emulation -> Configure` from the menu
   and go to `Controls`. At the bottom (This might be tricky on the Steam Deck screen, use an external one or drag the window around in desktop mode) select
   `Motion/Touch` and make sure that under `Motion Provider` `CemuHookUDP` is selected.

Now you're done and can enjoy a lovely-looking game! You will probably have to deal with
minor stutters as textures load but in my opinion it's worth it. You could also try to
enable preloading of textures before the game starts but this adds significantly to the
startup time and I haven't checked, if it changes anything in game.

It should also be noted that the same process can be applied for "Zelda - Ocarina of Time" which
is arguably the best game of all time. Since there is no restoration project for it, you only
need the textures from [here](https://www.patreon.com/collection/19135?view=expanded) and follow
the instructions for step 5 from above. The only difference is the directory name:

* US version: `0004000000033500`
* EU/Australian version: `0004000000033600`
* Japanese version: `0004000000033400`

Enjoy!
