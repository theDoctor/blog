---
title: "First Impression of GrapheneOS on New Phone"
date: 2022-08-19T21:10:48+02:00
tags: ["Privacy"]
aliases:
  - "/posts/privacy/grapheneos"
---

I recently decided to get a new phone. The old one (a
[ShiftPhone](https://www.shiftphones.com/en/) 6m) wasn't that old yet, I only
bought it 2.5 years ago but it was already running on Android 8.1 which is no
longer supported. I could have waited for the manufacturer to maybe provide more
support in the future but it seemed doubtful to me this would actually happen.
As I understand it, the biggest roadblock for this was the CPU, a Mediathek one,
for which the manufacturer apparently doesn't provide support or something of
the sort. From what I hear Snapdragon processors don't have this issue but what
can you do?

So I fretted a while about which phone to get. One one hand sustainability is
important to me and I want to contribute to the colossal electronic waste we are
piling up as little as possible but on the other hand I care about my privacy
and security and I don't appreciate vendor lock-in, or companies that drop
support for their devices after two years just so you can buy a new one.

The obvious choice for this would have been something like a
[FairPhone](https://www.fairphone.com/en/) and flash a custom ROM like
[LineageOS](https://lineageos.org/). The company has a good track record of
providing software support for a long time and generally doing a good job of
making a reasonably sustainable device. The thing is that, at the time of my
research, LineageOS had not been released yet for the latest FairPhone and I
wasn't sure how long that might take.

With all that said, I've got to admit that I also really like shiny, cool
things. I'd been reading about how [GrapheneOS](https://grapheneos.org/) is the
coolest kid on the block of custom ROMs and is supposed to be the best choice
for people who want a secure and private phone. Ironically, this is (almost)
only available for Google Pixel devices. Yeah, I know this feels weird to me as
well. Apparently, is has to do with hardware security and such. On top of this,
I found out that the latest generation of Pixels is getting five years of
software updates from Google and the GrapheneOS guys may even provide some
additional legacy support after this period of time (this is just speculation on
my part, though) so I figured I could get the latest generation budget variant
(the Pixel 6a) and get at least five years of usage out of this which seemed
reasonable to me. This is on par with IPhones which are usually praised for the
duration of security updates and software support. Given that I never planned on
running stock Android I wasn't worried about vendor lock-in in terms of software
but was slightly worried about repairability. Modern phone manufacturers don't
seem to care about this much (apart from a few), unfortunately and it seems to
be a question of higher priorities to me.

At the end of the day I decided to go with the Pixel. The fact that a free set
of wireless headphones was used as bait might have helped the decision. Who
knows?

The installation process can be done via web interface which I thought was
refreshing and it worked well enough apart from one caveat: the developers
insist on making users use a USB-C connection. Unfortunately, I have none on any
desktop or laptop in my household, the only device that has one is my previous
phone. So I tried various adapters and ports on my PCs followed by a direct
connection to my other phone but couldn't get it to work. Using adapters is just
not supported so I always got timeout errors of some sort. My phone should have
worked but for some reason any browser I used dropped the USB connection
repeatedly, apparently due to some issue with permissions.
I ended up borrowing a Windows Surface from a friend which, ironically, worked
without any issues. I installed drivers via the automatic update tool, plugged
my phone in, fired up Chrome and clicked a few buttons. That's it. I love how
simple it was, although you have to jump through a couple hoops until you can
start the actual installation process.

Initially the OS comes with seven preinstalled apps. This is really not a lot
but it covers the basic needs: most importantly, a browser, a file manager, a
gallery, a camera, a PDF viewer, a clock, contacts, a calculator, phone an SMS
app. Also, the auditor but I don't think I have a use case for that. There's
also a small app store for the specific GrapheneOS apps which includes Google
Play Store and its dependencies but more on that later.

The first order of business was then to customize my phone and get the apps I
need installed. All of this is pretty simple stuff. Download F-Droid and a
couple other apps I can't get there, install, make data backups on my previous
phone, transfer them, load them up, done. For good measure, arrange apps in
folders on my home screen and download a background image.

And just like that I have a fully functional phone just like before. Nothing
crazy going on here, I mean it's just like with any other Android phone, right?
I still feel like GrapheneOS has a different air from stock Android and with
good reason. It's quite barebones, like I said. It has sane defaults and much of
the amazing work of the devs happens in the background and stays out of your
way. For example the update mechanism is amazing. The system checks for OS
updates regularly and when one is available it's downloaded and applied in the
background. After everything is done you're prompted to reboot into the updatet
version and that's that. You also can't break the update or so because it's
installed into a separate installation that only becomes active after successful
installation and boot process. All of this makes a lot of sense because what
good is a "secure" OS if you don't apply updates to address vulnerabilities and
get fixes? Exactly.

There's a lot more, most of which I'm not aware of. I like that you can
granularly control app's permissions (which is not unique to GrapheneOS), you
can even restrict an app's of the file system to one or more specific folders.
GrapheneOS also features an implementation of sandboxed Google Play Store. This
means that you can install the app as a regular one in an unprivileged sandbox
and deny it any permissions. Network permissions would be useful (at some point
at least), though, otherwise you can't do anything with it, obviously. This
enables you to mitigate quite a but of the invasiveness of the Play Store and
still make use of its infrastructure, should you wish to. I, myself, chose not
to make use of this, however, because I don't want to rely on anything Google.
Instead I'm only using stuff from [F-Droid](https://f-droid.org/) (via the
[Droid-ify](https://github.com/Iamlooker/Droid-ify) app store frontend) because
I highly value my privacy.

I also quite like the camera. The hardware seems to be pretty good which could
be expected but the app also does a good job. The pictures are *much* prettier
than the one from my previous phone where I used OpenCamera from F-Droid. I
never got much use out of that so I'm happy to getting more snapshots in
nowadays.

All in all, I'm quite happy with my device. The experience feels more coherent
than what I've experienced on LineageOS so far and this goes as far as me using
the phone just for the sake of it because I like the OS so much. I also get more
battery life out of this one, although I'm not sure what the reason for that is,
might have been an issue with my old phone because I keep reading that the
battery of the 6a is actually not great but it suits me fine, I think. I can get
by for 1.5 days or so, more if I stretch it without using it too much.
Furthermore, my charger works much better on this phone compared to the last
one. I can get it fully charged in like an hour which feels blazingly fast to me
(yeah I know, this is Rust speak).

One small issue remains to me: the GrapheneOS team highly recommends using their
own browser, Vanadium. The thing is based on Chromium and I try to abide by such
recommendations. The browser does a good job but it doesn't provide content
filtering to get rid of ads. The recommendation is to address this with
DNS-based filtering but this doesn't get rid of ads entirely. I'm used to having
uBlock Origin installed on every device I use so having ads displayed when I use
DuckDuckGo (which is the default search engine) is a bit off-putting. This isn't
terribly important, though.

I appreciate how much care is taken with this OS and the apps bundled with it. I
feel as safe as can be expected for the uses I have for such a thing and I also
think it's quite pretty. 

Sometimes I wonder what will happen in five years when support for this device
ends. Probably, I'll reconsider my choices and hopefully make a good one. Maybe
Linux phones will be viable by then although I'm not holding my breath for that
to happen. We'll see. Until then I'm content with what I have.
