---
title: "How To: Rip BluRay discs on Linux"
date: 2023-06-13T10:18:38+02:00
tags: ["Tech", "Software", "Linux"]
aliases:
  - "/posts/howto/bluray"
---

> **Disclaimer**: Ripping BluRays with Handbrake is discouraged by
> its developers. They recommend using MakeMKV instead which is more
> robust in this respect since it's built for the task. By now I've
> adopted this workflow myself.

I recently gained a renewed interest in moving my media from discs to 
hard drives so I can integrate them with my media server. For music
and DVDs this is fairly straightforward, although there is a hoop to
jump through if you're on Fedora because it doesn't come with all the
proprietary codecs and whatnot preinstalled. However, all you need to
do is get your hands on the RPMFusion repo and install `libdvdcss`
and you're good.

BluRays, on the other hand, are a bit more involved because their 
encryption is more complicated. If you're like me and you want to 
access those discs on your Linux machine, here's how:

First you need to install a few libraries. Depending on what distro
you're on these will have different names. On Fedora these are:

* `libaacs`
* `libbluray`
* `libbluray-bdj`
* `libbdplus`

On Ubuntu the first two are called `libaacs0` and `libbluray2`, I think,
but I'm not quite sure. The point is, you might need to figure out the
exact name of the libraries yourself but it shouldn't be too difficult.

The other thing you need is the database with the AACS keys which can
be downloaded from [here](http://fvonline-db.bplaced.net/). Choose the
one for the appropriate language, extract it and put the file into
the `~/.config/aacs` folder (which you will probably have to create first).
Rename the file into `KEYDB.cfg` (because capitalization is important!)
and you're done. You should now be able to watch the BluRay (e.g. with VLC)
or rip it with Handbrake (which is what I'm doing). If the database doesn't
have a key for your disc it might be too new and you can try again by
redownloading the database at a later date.

Side note: you might be aware of MakeMKV which can rip BluRays without the
setup. However, this is proprietary and also doesn't give you as much control
over the transcoding process etc. so I prefer it this way.
