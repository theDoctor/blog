---
title: "How To: Set up NextcloudPi with Docker"
date: 2022-04-06T12:48:27+02:00
tags: ["How To", "Linux", "Software"]
aliases:
  - "/posts/howto/nextcloud"
---

I've been successfully running a Nextcloud instance for a while now and it's
quite useful. When I did the setup the first time I installed the entire stack
on bare metal by hand. Raspberry Pi with Raspbian (now Raspberry Pi OS), Apache,
MariaDB, PHP, the works. I closely followed tutorials on it because I knew very
little of what I was doing and there's a lot to keep in mind in terms of
performance, security, configuration of various things and so on.

Eventually it became clear to me that I wanted my instance to be as
low-maintenance as possible because I don't want to deal with breakage a lot and
also don't want to have extended downtimes.

At one point I discovered that there are several ways to run a Nextcloud server
apart from what I had done. There's a snap package for it, some distributions
like Arch have a dedicated package in their repos and then there's
[NextcloudPi](https://ownyourbits.com/nextcloudpi/).

I considered the former two but I had trouble with Ubuntu on my 32 bit ARM
architecture and I didn't want to run an Arch server. Just no.
NextcloudPi on the other hand seemed quite interesting because it promises to be
an all-inclusive package with basically the entire installation and
configuration being automated and/or easily accessible via a web interface. All
you need is a machine running a fresh install of Debian 10 (Buster) and you can
run a script taking care of everything for you.

I did this and ran it for quite some time without significant issues. Over time
though, I noticed a few things that bothered me:
- Debian 10 is oldstable by now and (reliable) support for Debian 11 is still
  not available at the time of writing
- NextcloudPi wants to be left alone. As soon as you use apt manually for
  anything, funny things happen. Like the Nextcloud app store not working
  properly or the trash bin not being accessible anymore.
- Even if support for a more recent Debian version becomes available, the
  updating process may very well break things.

A solution to these things is, you may have guessed it, using a container. I
realized that there's a dedicated NextcloudPi Docker image that you can just
download and run with very little setup required. This abstracts the application
away from the underlying operating system and makes migrations, backups and the
various update processes much easier. 

Here's a rundown of all the steps involved, as usual for anyone who might be
interested or for future me, who may want to retrace their steps.

- First of all, you of course need a machine to use as a server running a
  suitable OS. I'm using my Raspberry Pi 4 with the latest Raspberry Pi OS which
  is based on Debian 11.
- Next, you have to install Docker. Unfortunately you can't just add the repo
  and install the package from it if you're on a Raspberry. Instead you need to
  download an installation script:
  `curl -fsSL https://get.docker.com -o get-docker.sh`
  Inspect it if you like and run with:
  `sudo sh get-docker.sh`
- In order to be able to run docker commands without root privileges, you can
  add your standard user to the docker group:
  `sudo usermod -aG docker Pi`
- You can verify the docker installation like this:
  `docker run hello-world`
- As a final preparation step before setting up the actual Nextcloud
  installation you may want to add external drives to your Pi where the data
  will be stored. Another hard drive for backups may also be a good idea.
  Mount the drives you want to use:
  `sudo mount /dev/sdXX [/path/to/mountpoint]`
  To enable automatic mounting on boot edit `/etc/fstab` and add a line for each
  additional mount:
  `UUID=[UUID of the drive] [/path/to/mountpoint] ext4 rw,users,exec 0 0`
  Don't forget the `exec` part, the docker container will run into permission
  issues otherwise
- Now you can turn to the Nextcloud image. First download it:
  `docker pull ownyourbits/nextcloudpi-armhf`
  You can also use the `nextcloudpi`, they amount to the same thing.
- Finally, start the docker container:
  `docker run -d -p 4443:4443 -p 443:443 -p 80:80 -v
  [/path/to/external/data/drive]:/data --name nextcloudpi --ip "[IP of Nextcloud
  machine]" ownyourbits/nextcloudpi-armhf [Domain]`
- Make sure everything went smoothly by checking the logs:
  `docker logs -f nextcloudpi`
- If everything went well, you can reach your Nextcloud under the given domain
  now. Navigate there and activate the instance.
- For configuration you can either go to [domain]:4443 and do it via the web
  interface or run `docker exec -it nextcloudpi ncp-config`. This will run the
  `ncp-config` utility script within the container where you can do all
  necessary configuring via TUI. This way you can also restore a previous
  backup and NextcloudPi configuration. Note that for this to work, the
  container needs to have access to the backup files. The easiest way to achieve
  this is to move them into the data directory you mounted.

A nice thing about this whole setup is that updating the Nextcloud version is
quite easy with this. You just pull and run the updated container as soon as it
is available. Like so:
- `docker stop nextcloudpi`
- `docker rm nextcloudpi`
- `docker pull ownyourbits/nextcloudpi-armhf:latest`
- Run the updated container with the same command as above.

Enjoy!
