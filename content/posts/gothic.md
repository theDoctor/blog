---
title: "How to: Run Gothic and Gothic II on Linux"
date: 2021-12-14
tags: ["Gaming", "Linux", "How To"]
ShowToc: true
TocOpen: true
aliases:
  - "/posts/gaming/gothic"
---

*Last updated: 11.11.2024*

> **This post is updated regularly so it might be useful to return later on.
> If there's anything missing here, a mod you would like to see covered or
> something else that could be added, don't hesitate to contact me! I'll be
> glad to help out, if I can. Also, if you have any suggestions for
> improvements or know a better way for something, don't hesitate to reach out!**

There are many challenges when it comes to gaming on Linux but it's even more
difficult to get something up and running that's so old that it's a hassle to do
it under Windows as well.

In essence you have to deal with the setup you'd do in Windows (which is already
nontrivial) and apply all of it through the layer of abstraction that is running
Windows software on Linux. Fun.

Anyway, Gothic and Gothic II are two of my favorite games of all times and I recently
discovered that there are some very interesting mods for them out there which I've
never tried. I know I'm like 10 years late with this realization, so sue me.

As a note to others (and most of all myself), here's how to do it:

## The Tools

There are various ways to go about this whole endeavour. You can do everything
manually from the command line with wine and winetricks which I don't recommend.
The reason is that there are great tools available for helping you set severything
up that work very well. These are mainly [Lutris](https://lutris.net/) and [Bottles](https://usebottles.com/).
Which one you use is up to you, I have used both successfully to install these games.
Bottles has a nicer UI/UX, especially when it comes to managing wine dependencies
and setting DLL overrides. Lutris on the other hand is scriptable which is a very
attractive feature.

### Bottles

The setup in Bottles is fairly straightforward. You add a new bottle (you can select "gaming"
but I didn't test whether it makes a difference) and start using it. You can find
the necessary wine dependencies in the menu of the newly created bottle under
"Dependencies". DLL overrides can be set up under "Settings" in the section
"Compatibility" near the bottom of the available options. For installing Windows
applications, just select "Run Executable" from the bottle menu.

### Lutris

You can choose to use an existing Lutris installer but in order to have more control
over the installation you should choose to install a game fresh from a Windows executable.
One nice feature of Lutris is its capability to run custom install scripts which can
be used to automate the otherwise sometimes tedious install process. To do this click on
"Add Game" in the top right corner and select "Install from a local install script".
Custom Lutris scripts for installing various versions of both games can be found [below](#installation-scripts).

For a manual game installation you will need to consider a few things.
In order to install wine dependencies you need to open up `winetricks` within the wine
prefix. When you select an installed game that runs with wine you can click on the wine menu
(arrow symbol next to the wine glass button) and select "Winetricks". Then you need to
select "Select the default wineprefix" and in the next menu "Install a Windows DLL or
component"

This can also be done from the command line like so (which necessitates `winetricks` being installed):

```
  WINEPREFIX=/path/to/prefix winetricks
```

DLL overrides can be added in two places. In the game settings (after you right-click a
game and select "Configure") in the "Runner options" tab. Scroll to "DLL overrides" and
click on "Add". The values follow the syntax of "n,b" for "native, then builtin" and
vice versa accordingly.

The second place you can do this is when selecting "Wine configuration" from the wine
menu (see above how to open winetricks). Select the "Libraries" tab and in the field
"New override for library" enter or choose the appropriate value. After clicking
"Add" you can then select "Edit" to select the value you need.

### Steam

On Steam you can use [`protontricks`](https://github.com/Matoking/protontricks) to install wine dependencies.
Depending on how you install it, it is either called as `protontricks` or as
`flatpak run com.github.Matoking.protontricks` in the case of a flatpak install. The latter
is recommended on Steam Decks. You can, of course, create a shell alias to make typing
the command less cumbersome.

To install a dependency into a specific wine prefix you need the Steam ID of the game. For Gothic this is
`65540` for Gothic II it's `39510`. You can now do

```
  protontricks 39510 directmusic
```

to, for example, install `directmusic` to the Gothic II wine prefix.

To add DLL overrides the most reliable method I found is to manually edit the `user.reg` config
file sitting in `~/.steam/steam/steamapps/compatdata/$STEAM_ID/pfx/user.reg`. You
look for the section called `[Software\Wine\DllOverrides]` and edit the following
lines according to your needs. To Add a new override you add, e.g.

```
  "ddraw"="native,builtin"
```

Of course you need to adapt the values to your needs. To disable an existing override
simply delete the corresponding line. Don't forget to save the file!

Installing mods is, in principle, quite straightforward. You open up the Steam Workshop
page for the relevant game, look for mods you want to install and hit "Subscribe". Done.
The caveat here is that installing different, possibly incompatible, mods is not possible
this way.

Once you've done this you need to go to "Properties" on the game, then "Betas" and then
select the Steam Workshop Beta. This will enable a launcher where you can select the
order in which mods are loaded, the language of the mod (if applicable) and more.

The biggest issue by far I had with going through Steam is that when I installed mods
on top of the vanilla game (especially the D3D11 renderer and conversion mods) I
consistently ended up with framerates of 15-20. I have not yet figured out why, if
you have an idea, please reach out!

### Spine

[Spine](https://clockwork-origins.com/spine/) is a tool that makes installing and managing mods very easy. However, if
you use it to install several extension or conversion mods,
things can get a bit dicey, because they can easily end up breaking each other.

In principle, you could use Spine to set up just one extension or conversion mod
to not have it interfere with others. In my experience, however, this does not
work with the D3D11 renderer ([see below](#recommended-functional-Mods)) which makes it far less attractive.

### OpenGothic

I would be remiss if I didn't mention [this project](https://github.com/Try/OpenGothic).
It is an open-source reimplementation of the
game engine which means that you provide the game files yourself (still have to buy it, duh)
but the engine does the rest. The best part is that it has a Linux build so you don't have to
go through wine to play (just once for installation).

There are several things to note:

* The project is still in a pre-release stage so don't expect a feature-complete or fully stable product
* Gothic 1 is not officially supported but might run as well
* Some behavior of the original game engine is not 100% reproduced
* Most of the functional mods will *not* work because Ninja or Union are not supported (see [here](https://github.com/Try/OpenGothic#modifications))
* Not all of the extension/conversion mods will work (see [here](https://github.com/Try/OpenGothic#modifications))
* D3D11 renderers don't work (although the game already looks nice OOTB)

If that doesn't deter you, here's what you need to do:

1. Install the base game with wine (either using the command line or any of the ways below)
2. Download a Linux build from [here](https://ci.appveyor.com/project/Try/opengothic/history)
    * Alternatively, you can build the engine yourself, see [here](https://github.com/Try/OpenGothic#build-instructions) for instructions
3. Run the game by executing the given script and providing the path to your install of Gothic II:
   `Gothic2Notr.sh -g /path/to/Gothic_2`
    * See [here](https://github.com/Try/OpenGothic#command-line-arguments) for additional options like directly
    loading a savegame or enabling ray tracing (if your hardware is capable of it)

## The absolute minimum

These instructions assume that you know where the game is installed. I used the
GOG version throughout but I assume the same approach should work with the
Steam version but I haven't tested it.

1. Install the game in a wine prefix
2. Add necessary dependencies:
    * `directmusic`
    * MS Visual C++ Redistributable 2008 and 2010 (these are called `vcrun` in winetricks and `vcredist` in Bottles)
    * For the Union patch (see below) you also need .NET 4.0 (found under `dotnet40`) which is highly recommended
3. Add DLL override for `dsound` and set it to "builtin, then native" (the exact wording or naming of the option depends on the tool)

This is sufficient to get the base game going. However, there are a few issues due to the
game being old and a bit buggy, e.g. widescreen resolutions not being supported and
the like. To fix this, it is recommended to at least install these patches:

### Gothic

- [Primary Universal patch](https://worldofplayers.ru/attachments/103682/)
- [Union1.0](https://www.worldofgothic.de/dl/download_651.htm)
- (Optional)[Community Patch](https://github.com/AmProsius/gothic-1-community-patch/)

The community patch is marked as optional because, although it focuses on fixing story and script
bugs, it is dependent on Ninja [see below](#recommended-functional-mods). Nevertheless it is
recommended to install it due to its modularity and benefit.

### Gothic II

- [fix-2.6.0.0-rev2](https://www.worldofgothic.de/dl/download_278.htm)
- [playerkit-2.6f](https://www.worldofgothic.de/dl/download_168.htm)
- [Union1.0](https://www.worldofgothic.de/dl/download_651.htm)

The playerkit is needed to create the file `GothicStarter.exe` which is a launcher
used to start the base game or any installed mod. Some mods incorporate this themselves, some
don't. To be on the safe side you can always install it unless you know for a fact that you
won't need to.

Enjoy one of the coolest RPGs ever created!

## Modding

The great thing about these games is that there's still a very active modding community
with many awesome mods still being developed (especially for Gothic II).
The challenge here is that many mods have specific requirements, rely on certain
installation orders of dependencies or are not compatible with each other.

The surest way to get everything running smoothly is to keep different mods in
separate installations (i.e. wine prefixes) and follow specific installation
orders/instructions to make sure you get reproducible builds, so to speak.
I know this is tedious, that's why I created a number of custom Lutris install
scripts ([see below](#installation-scripts)). Since such automated scripts are
prone to breaking when new versions are released or the URLs used are not
available anymore (or other reasons) I will also record the steps to install everything
manually.

### List of mod installation instructions

Here is a list of the mods covered in this blog post:

#### Gothic / Gothic II

* [Functional mods](#recommended-functional-mods)

#### Gothic

* [Balancing Mod](#balancing-mod)
* [Definitive Edition](#definitive-edition)
* [Die Welt der Verurteilten](#die-welt-der-verurteilten)
* [Othello](#othello)
* [Quentins Bande](#quentins-bande)
* [Revised Edition](#revised-edition)

#### Gothic II

* [Generic extension/conversion mod](#generic-extensionconversion-mod-installation)
* [Atarier Edition](#generic-extensionconversion-mod-installation)
* [Chronicles of Myrtana: Archolos](#chronicles-of-myrtana-archolos)
* [Die Bruderschaft: Der Weg des Schläfers](#die-bruderschaft)
* [Dirty Swamp](#dirty-swamp)
* [Gold Remaster](#gold-remaster)
* [Legend of Ahssûn](#legend-of-ahssûn)
* [L'Hiver - English Edition](#lhiver---english-edition)
* [L'Hiver - German Edition](#generic-extensionconversion-mod-installation)
* [MiniMod](#generic-extensionconversion-mod-installation)
* [Nostalgic Mod](#nostalgic-mod)
* [Odyssee](#odyssee)
* [Original L'Hiver Edition - German Version](#generic-extensionconversion-mod-installation)
* [Returning - New Balance](#returning-new-balance)
* [Velaya](#generic-extensionconversion-mod-installation)

The extension/conversion mods can be complemented with a selection of functional mods (see next section), if desired.

### Recommended functional Mods

Here is a non-exhaustive list of mods I found useful or nice to have to varying degrees. These
can usually be installed regardless of what other extension/conversion mod is installed.

The installation process depends on the mod in question. Some have a dedicated executable that needs
to be run at some point during the whole installation process. In case of Union mods (e.g. zNoFriendlyFire)
this needs to be done after installation of the Union mod itself. I usually install these mods after
everything else is done which works fine.

The mods that are provided as `.vdf` files are installed by simply dropping the file inside the
`$BASE_DIRECTORY/data` directory.

* [D3D11 Renderer](https://github.com/kirides/GD3D11): This looks beautiful and is highly recommended.

  **IMPORTANT 1**: If you use this,
  you need to add an additional DLL override of `ddraw` and set it to "builtin, then native".

  **IMPORTANT 2**: If you use this, it is recommended to apply the LargeAddressAware hack to improve
  performance. This can be done with [this tool](https://www.worldofgothic.de/dl/download_552.htm).
  Just open it, select the appropriate Gothic executable and close it again.
* [Ninja](https://www.worldofgothic.de/dl/download_652.htm): Mod that enables other mods (see below)
* [BonusIndependentTraining](https://github.com/elsky42/ninja-bonus_independent_training): Binds the cost of training attributes and skills to how much points you've invested so far
  and not the effective value. This lets you consume permanent bonus consumables whenever you wish. One of my absolute favourite changes! (needs Ninja)
* [zNoFriendlyFire](https://worldofplayers.ru/threads/42019/): This mod disables friendly fire which makes using companions *much* nicer
  (this requires Union)
* [zGamePad](https://worldofplayers.ru/threads/42005/): Adds gamepad support with configurable UI glyphs. Pretty much mandatory for playing on Steam Deck.
  (this requires Union)
* [PatchMenu](https://www.worldofgothic.de/dl/download_674.htm): Adds menu entries to configure other mod options (see below)
* [Quickloot](https://worldofgothic.de/dl/download_679.htm): Adds the capability to pick up
  items and loot unlocked chests without an animation. By default bound to secondary mouse button (needs Ninja).
* [QuicksaveSlots](https://www.worldofgothic.de/dl/download_658.htm): Adds support for several configurable quick save slots (needs Ninja)
* [Sprint](https://worldofgothic.de/dl/download_630.htm), Adds possibility to sprint constantly, configurable (needs Ninja and Patchmenu)
* [OrderedLogs](https://www.worldofgothic.de/dl/download_650.htm): Patches log entries to be ordered and coloured, if desired (needs Ninja)
* [PickLockHelper](https://www.worldofgothic.de/dl/download_626.htm): Shows used combinations when picking locks so you
  don't have to memorize everything (needs Ninja)
* [BetterTorches](https://www.worldofgothic.de/dl/download_625.htm): Adds a hotkey for torches and makes them nicer to use (needs Ninja)
* [Item Map](https://www.worldofgothic.de/dl/download_634.htm): Adds collectible items to in-game maps, very configurable. Highly useful, especially when playing mods that change the position of valuable items around. (needs Ninja)
* [Trader Weapon Fix](https://www.worldofgothic.de/dl/download_619.htm): Fixes the issue of traders always equipping the strongest weapon they sell (needs Ninja)
* [Item Locator](https://www.worldofgothic.de/dl/download_661.htm): Add hotkey to highlight collectible items in the vicinity, very configurable and highly useful, especially when playing with texture or vegetation mods that make plants harder to spot. (needs Ninja)
* [PercentPotions](https://www.worldofgothic.de/dl/download_644.htm): Changes potions to regenerate relative, not absolute, values (needs Ninja)
* [CategorizedInventory](https://www.worldofgothic.de/dl/download_617.htm): Introduced a categorized inventory that can be tabbed through (needs Ninja)
* [Calligryphy I](https://www.worldofgothic.de/dl/download_533.htm) and [Calligraphy II](https://www.worldofgothic.de/modifikation/download_536.htm): Beautiful renditions
  of letters, books and other written texts in Gothic I and II. Only compatible with German versions of the games.

A Lutris installation script snippet automating installation of functional mods can be found [below](#installation-scripts)

### Generic extension/conversion mod installation

This is a general blueprint for installing such mods that should work for most or maybe all
cases.

Tested with:

* [Atarier Edition](https://www.worldofgothic.de/dl/download_543.htm)
* [L'Hiver - German Edition](https://www.worldofgothic.de/dl/download_556.htm)
* [MiniMod](https://www.worldofgothic.de/dl/download_208.htm) (This is compatible to the Gold Remaster ([see below](#gold-remaster)))
* [Original L'Hiver Edition - German Version](https://www.worldofgothic.de/dl/download_721.htm)
* [Velaya](https://www.worldofgothic.de/dl/download_312.htm) (download and execute both executables for voice acting)

1. Install base game with patches as [described above](#the-absolute-minimum)
2. Install [Ninja](#recommended-functional-mods)
3. Run mod executable
4. (Optional) Install D3D11 Renderer (with DLL override)
5. (Optional) Install [functional mods](#recommended-functional-mods) you like
6. Start the game with `$BASE_DIR/system/GothicStarter.exe`

A Lutris installation script automating this procedure can be found [below](#installation-scripts)

Next is a list of instructions for mods that need specific setup steps or other special
treatment.

### Individual mod installation

#### Gothic

##### Balancing Mod

As the name suggests this is a mod that does not add additional content but rebalances the game to
make it a bit more challenging and provide a better balance between the various available skill paths.
A list of changes can be found [here](https://www.worldofgothic.de/dl/download_646.htm).

1. Install [Primary Universal patch](https://worldofplayers.ru/attachments/103682/)
2. Install [Union1.0](https://www.worldofgothic.de/dl/download_651.htm)
3. (Optional) Install [D3D11 Renderer](#recommended-functional-mods) (with DLL override)
4. Install Balancing Mod
5. (Optional) Install [Ninja](#recommended-functional-mods)
6. Install [Community Patch](https://github.com/AmProsius/gothic-1-community-patch/)
7. (Optional) Install [functional mods](#recommended-functional-mods) you like

A Lutris installation script automating this procedure can be found [below](#installation-scripts)

##### Definitive Edition

This is another extension mod which aims to implement sensible but not too extensive additions. Formerly
empty areas are now filled, new textures and other such fine tuning has bee performed. However,
there are also additional NPCs and some use of TTS, although not much.
If you want an experience closer to the vanilla game, the [Revised Edition](#revised-edition) is probably
a better choice. More info and installation instructions can be found [here](https://www.worldofgothic.de/dl/download_635.htm)

Adapted installation steps are as follows:

1. Install [Primary Universal patch](https://worldofplayers.ru/attachments/103682/)
2. Execute [LAA hack](#recommended-functional-mods)
3. Install [Union1.0](https://www.worldofgothic.de/dl/download_651.htm)
4. (Optional) Install [D3D11 Renderer](#recommended-functional-mods) (with DLL override)
5. Install Definitive Edition
6. (Optional) Install [Ninja](#recommended-functional-mods)
7. Install [Community Patch](https://github.com/AmProsius/gothic-1-community-patch/)
8. (Optional) Install [functional mods](#recommended-functional-mods) you like

A Lutris installation script automating this procedure can be found [below](#installation-scripts)

##### Die Welt der Verurteilten

This is an extension mod that aims to make sensible additions and changes to the base game to fill in the gaps. In spirit
similar to the [Definitive](#definitive-edition) or [Revised](#revised-edition) Edition. Inceases difficulty to a similar
level than Gothic 2 with addon. Makes use of TTS. Details can be found [here](https://www.worldofgothic.de/dl/download_462.htm).

Adapted installation steps are as follows:

1. Install [Primary Universal patch](https://worldofplayers.ru/attachments/103682/)
2. Install [Union1.0](https://www.worldofgothic.de/dl/download_651.htm)
3. (Optional) Install [Ninja](#recommended-functional-mods)
4. Install Die Welt der Verurteilten
5. (Optional) Install TTS
6. (Optional) Install [Community Patch](https://github.com/AmProsius/gothic-1-community-patch/)
7. (Optional) Install [D3D11 Renderer](#recommended-functional-mods) (with DLL override)
8. (Optional) Install [functional mods](#recommended-functional-mods) you like

A Lutris installation script automating this procedure can be found [below](#installation-scripts)

##### Othello

Another extension mod adding new areas, NPCs, some new quests and the like. Overall more extensive changes than the previous two
entries, also with changes to previously existing areas in the game. It does not add new dialog lines but makes do with what's
already available in game or from cut content. More info can be found [here](https://www.worldofgothic.de/dl/download_682.htm)
The installation is simple:

0. Do **NOT** install any patches, mods or fixes
1. Install base game (Steam or GOG version are the correct version already)
2. Install Othello
4. (Optional) Install [D3D11 Renderer](#recommended-functional-mods) (with DLL override)
5. (Optional) Install [functional mods](#recommended-functional-mods) you like

**Note**: This mod is incompatible with Ninja so many of the functional mods will not work. Also, Union must not be installed prior
to mod installation because the mod uses a modified version of Union internally.

A Lutris installation script automating this procedure can be found [below](#installation-scripts)

##### Quentins Bande

A big extension mod, changing balancing, adding a new guild, NPCs, quests, textures and more. Details can be found
[here](https://www.worldofgothic.de/dl/download_728.htm).

Adapted installation steps are as follows:

1. Install [Primary Universal patch](https://worldofplayers.ru/attachments/103682/)
2. Install [Union1.0](https://www.worldofgothic.de/dl/download_651.htm)
3. (Optional) Install [Ninja](#recommended-functional-mods)
4. Install Quentins Bande
5. Install patch 1031
5. (Optional) Install TTS
6. (Optional) Install [Community Patch](https://github.com/AmProsius/gothic-1-community-patch/)
7. (Optional) Install [D3D11 Renderer](#recommended-functional-mods) (with DLL override)
8. (Optional) Install [functional mods](#recommended-functional-mods) you like

A Lutris installation script automating this procedure can be found [below](#installation-scripts)

##### Revised Edition

This is an extension mod with some rebalancing, an improved thieving system, plenty of bug fixes but
almost no additional content (which is the point). More info can be found [here](https://www.worldofgothic.de/dl/download_720.htm)
The installation is quite simple:

1. Install [Union1.0](https://www.worldofgothic.de/dl/download_651.htm)
2. Install [Primary Universal patch](https://worldofplayers.ru/attachments/103682/)
3. Install Revised Edition (Unpack files to game directory)
4. (Optional) Install [D3D11 Renderer](#recommended-functional-mods) (with DLL override)
5. (Optional) Install [functional mods](#recommended-functional-mods) you like

A Lutris installation script automating this procedure can be found [below](#installation-scripts)

#### Gothic II

##### Chronicles of Myrtana: Archolos

This is a total conversion mod that was released fairly recently. It's
an absolutely amazing experience and I highly recommend playing it. It's the Gothic 3
we wanted but never got. It's available with Polish voice acting (which is *incredible*
although I don't speak Polish) or with German TTS. I prefer the former.

So far, I have not been able to install and run it in the same way as the other mods
described here because of an access violation error cropping up right after starting the game.
However, the Steam version of this works out of the box without any
tweaking necessary (which is why I played it there). The mod is available [as its
own game](https://store.steampowered.com/app/1467450/The_Chronicles_Of_Myrtana_Archolos/)
but free of charge. Also note that the Polish voice acting has to be [added
to the library separately](https://store.steampowered.com/app/1495950/The_Chronicles_Of_Myrtana_Archolos__Polish_VoiceOver_Pack/).
I have not tried modding the game when run in this way, though.
There is a collection of mods in the Steam Workshop for Gothic II which presumably work
and should make it fairly easy to get up and running but this isn't comprehensive as far
as I know. To install mods from the Steam Workshop just open up the page [here](https://steamcommunity.com/app/39510/workshop/),
select your mods of choice and click the "Subscribe" button. Steam will do the rest.

##### Die Bruderschaft

This is a total conversion mod for Gothic II taking place in the world of Gothic I. Details
can be found [here](https://www.worldofgothic.de/dl/download_666.htm). Installation is fairly
generic except for the lack of a dedicated installer.

1. Install base game with patches as [described above](#the-absolute-minimum)
2. Install [Ninja](#recommended-functional-mods)
3. Extract mod files into respective folders
4. (Optional) Install D3D11 Renderer (with DLL override and LAA hack)
5. (Optional) Install [functional mods](#recommended-functional-mods) you like
6. Start the game with `$BASE_DIR/system/GothicStarter.exe`

A Lutris installation script automating this procedure can be found [below](#installation-scripts)

##### Dirty Swamp

This is a total conversion mod using the world of Jharkendar for the most part. It adds a new
story and makes a lot of changes to combat and especially magic. More details can be found
[here](https://www.worldofgothic.de/dl/download_480.htm).

1. Install base game with patches as described above
2. (Optional) Install [D3D11 Renderer](#recommended-functional-mods) (with DLL override and LAA hack)
3. Install mod version 3.0.1
4. Install patch for version 3.0.18
5. (Optional) Install [Ninja](#recommended-functional-mods)
6. (Optional) Install [functional mods](#recommended-functional-mods) you like

It should be noted that the mod makes several gameplay changes which may not work well or at all
with some of the functional mods (e.g. the sprint mod should not be installed). The mod itself
recommends [Better Torches](https://www.worldofgothic.de/dl/download_625.htm) and [Categorized Inventory](https://www.worldofgothic.de/dl/download_617.htm).

A Lutris installation script automating this procedure can be found [below](#installation-scripts)

##### Gold Remaster

This is mostly a graphics mod which takes a lot of stuff and textures from other mods, like
L'Hiver, and packages it up to one beautiful thing. It also integrates with the MiniMod ([see
above](#generic-extensionconversion-mod-installation)) so this is ideal, if you want a more
beautiful version of the vanilla game and, optionally, some sensible changes to balancing.
The release announcement can be found [here](https://forum.worldofplayers.de/forum/threads/1580812-RELEASE-Gothic-II-Gold-Remaster)
which also included installation instructions:

1. Start with a fresh unpatched installation of the game
2. Install [fix-2.6.0.0-rev2](https://www.worldofgothic.de/dl/download_278.htm) (necessary to be able to install Union)
3. Install [Union1.0](#the-absolute-minimum)
4. (Optional) Install [Ninja](#recommended-functional-mods)
5. (Optional) Install [D3D11 renderer](#recommended-functional-mods) (with DLL override and LAA hack)
6. Execute [Remaster executable](https://www.worldofgothic.de/download.php?id=1647)
7. (Optional, if D3D11 renderer was installed) Install [NormalMaps](https://www.worldofgothic.de/download.php?id=1657)
8. (Optional) Install [MiniMod](https://www.worldofgothic.de/download.php?id=1658) (version adapted to Remaster)
9. (Optional) Install [functional mods](#recommended-functional-mods) you like
10. Start the game with `$BASE_DIR/system/GothicStarter.exe`

A Lutris installation script automating this procedure can be found [below](#installation-scripts)

Note that the patches for the report version and player kit are
already included in the installer. The report version needs to be installed nonetheless because Union cannot
be installed to the vanilla GOG version of the game.

##### Legend of Ahssûn

This is a total conversion mod which nonetheless offers a Gothic-like experience. It
differs from other projects of the sort in that it offers voice acting for every dialog
in the game without making use of TTS or other synthetic methods.
The necessary files are available [here](https://www.worldofgothic.de/dl/download_551.htm).
Apart from the voice acting the installation process is similar to the other mods:

1. Install base game with patches as described above
2. (Optional) Install [Ninja](#recommended-functional-mods)
3. Install LoA
4. Download `LoA-1.1.0_Speech_Single.zip` and merge the `_work` folder within with
   the existing one in the base game directory
5. (Optional) Install [D3D11 Renderer](#recommended-functional-mods) (with DLL override and LAA hack)
6. (Optional) Install [functional mods](#recommended-functional-mods) you like
7. Start the game with `$BASE_DIR/system/GothicStarter.exe`

A Lutris installation script automating this procedure can be found [below](#installation-scripts)

##### L'Hiver - English Edition

L'Hiver is a very popular mod with a number of different editions, versions and translations which makes
matters a bit murky. The English edition is very feature-rich and implements numerous extensions on top
of the base game while looking quite beautiful. Note that, despite the name, this can be played in German,
if appropriate steps are taken.
Like all L'Hiver editions it features an extended world with new areas, many new models
and textures as well as a redesign of previously existing areas. If you're a fan of the vanilla game and
do not appreciate too many changes to it, I advise you to steer clear of this. The release post with
instructions and download links can be found [here](https://forum.worldofplayers.de/forum/threads/1524640-Release-Gothic-2-L-Hiver-1-1-English-Edition-2).

1. Install base game with patches as described above
2. Install [Ninja](#recommended-functional-mods)
3. (Optional) Install [D3D11 Renderer](#recommended-functional-mods) (with DLL override and LAA hack)
4. (Optional) Install German translation (text only)
5. **Important** Run the game once so that Union can create its files (this can, unfortunately, not be automated)
6. Install the mod itself

It should be noted that this mod incorporates numerous Union and Ninja mods, all of which can be disabled and/or configured.
For a detailed list of everything that's included and how to configure it you can look at the
[installation guide](https://cloud.bfloeser.de/s/WnL97pagLH3Zs5c).

A Lutris installation script automating most of this procedure can be found [below](#installation-scripts)

##### Nostalgic Mod

This mod aims to preserve the original Gothic II feeling while adding a few quality-of-life improvements
and additions, especially a full version of the Valley of the Mines.
It provides installation instructions [here](https://forum.worldofplayers.de/forum/threads/1570328-RELEASE-Gothic-II-Nostalgic-Edition-2-8-7)
which I adapted for my personal taste and added a few other mods.

1. Install base game with patches as described above
2. (Optional) Install [Ninja](#recommended-functional-mods)
3. (Optional) Install [D3D11 Renderer](#recommended-functional-mods) (with DLL override and LAA hack)
4. Install Nostalgic mod (optionally with additional textures and dialog animations)
5. (Optional) Install [functional mods](#recommended-functional-mods) you like
6. In `Systempack.ini` set `InteractionCollision=0` and `EnableShields=1`
7. Start the game with `$BASE_DIR/system/GothicStarter.exe`

A Lutris installation script automating this procedure can be found [below](#installation-scripts)

Since this mod already includes some of the features other mods give you, you don't
need to add much. Personally, I added the Sprint mod (which needs PatchMenu) for
more control over this and OrderedLogs. Sprinting is supposedly already included in
the mod but I think it's not configurable so I added it myself. Quicklooting and
quicksave slots are already included.

Finally, start the game and change the settings to your liking of which there are
quite a few. Specifically the options around friendly focus and friendly fire are
worth considering because this makes having companions much nicer. Enjoy!

##### Odyssee

This is the to date largest total conversion mod for Gothic II. It takes place in the same world but adds
new story, quests, characters, worlds and so on.
More information can be found [here](https://www.worldofgothic.de/dl/download_522.htm).

1. Install base game *without* any patches.
2. Download executable and all the provided `.bin` files and put them in the same folder
3. Install Odyssee by executing the `.exe` file
4. (Optional) Install [Ninja](#recommended-functional-mods)
5. (Optional) Install [functional mods](#recommended-functional-mods) you like
6. Start the game with `$BASE_DIR/system/GothicStarter.exe`

This game already includes Union by default so there's no need to install it. The mod supports
Union plugins out of the box.

A Lutris installation script automating this procedure can be found [below](#installation-scripts)

##### Returning - New Balance

This is an adaptation of the Returning mod available in German and Russian and builds on top of the original
Returning 2.0 mod. It adds a *lot* of new content, settings, gameplay features, quests, characters and so on.
It's huge. More details can be found [here](https://forum.worldofplayers.de/forum/threads/1610826-RELEASE-Gothic-II-Returning-New-Balance)

1. Install base game *without* any patches.
2. Download executable and all the provided `.bin` files and put them in the same folder
3. Install Returning by executing the `.exe` file
4. Start the game with `$BASE_DIR/system/Gothic2.exe`

Note that this game already includes a list of modifications and plugins so adding any additional mods
is very likely unnecessary or even detrimental. Proceed at your own risk.

A Lutris installation script automating this procedure can be found [below](#installation-scripts)

## Installation scripts

Below is a list of Lutris installation scripts with various fixes and mods installed. Note that, unless otherwise
stated, the scripts don't include installation of any of the functional mods. These can be installed by hand afterwards
or the installation script modified, if desired.

To use a script, copy its content into a file with a `.yaml` file ending. Then open up Lutris, press the button to
add a new game, select "Install from a local install script", in the opening window, select "Browse..." and select
the script you just saved.

**Note 1**: The scripts can be used as-is but I highly recommend adapting them to your liking. Especially in terms
of functional mods which add a lot of QoL features. In some cases I also added texture packs which may not be to
your liking so it's generally a good idea to at least skim through to see what a script does before you use it.

**Note 2**: The scripts will install German versions of mods, if available, by default. If you prefer a different
language you need to provide the respective executable files yourself.

**Note 3**: The scripts assume that the GOG version of the game is used. If you don't have this version, you will
need to adapt them accordingly.

**Note 4**: If you link Lutris to your GOG account, the scripts will download the game from GOG by default. It will
try to choose a language matching your locale. If this is not what you want (as is the case with me), I recommend
you *unlink* your GOG account from Lutris, download the game version of your choice and provide it to the installer
by hand when prompted. **If you try this with your GOG account still linked, the installation won't work** for reasons
I don't understand.

{{< includecode desc="Gothic - Vanilla with patches and Union mod" path="assets/install_scripts/g1_vanilla.yaml" lang="yaml">}}

{{< includecode desc="Gothic - Balancing Mod" path="assets/install_scripts/balancing.yaml" lang="yaml">}}

{{< includecode desc="Gothic - Definitive Edition" path="assets/install_scripts/definitive.yaml" lang="yaml" expl=`
  This includes the Unions and Ninja mods, the D3D11 renderer, better quality cutscenes as well as the LAA hack.
  **Important**: When selecting the installation location for the mod, the installer will frequently still not
  display the chosen directory correctly. Make sure to double-check the displayed installation location after
  choosing it from the picker and make corrections, if necessary.
`>}}

{{< includecode desc="Gothic - Die Welt der Verurteilten" path="assets/install_scripts/verurteilten.yaml" lang="yaml" expl=`
  **Important**: During the installation the directory for the TTS files needs to be selected. These
  need to be extracted into the "Data" folder within the base game directory. Make sure to choose this!
`>}}

{{< includecode desc="Gothic - Othello 3.6.10" path="assets/install_scripts/othello.yaml" lang="yaml" expl=`
  This includes the Othello mod and a D3D11 renderer.

  **NOTE 1**: This installation script is optimized for the Steam Deck. This includes using non-default health,
  mana and stamina bars as well as resizing and repositioning them. Also, the hotbar is disabled in favor of
  the zGamePad quick-access menus. Furthermore, the showing of items outside of the inventory is disabled because
  it also obscures the quick-access menu from zGamePad.
  If you don't like this (or play on a bigger screen),
  you can (and should) adapt or delete the respective section in the script (the section modifying "GOTHIC.INI").
  The UI elements can also be manually configured by
  pressing LCtrl + LAlt + V in game and following the instructions. For this a keyboard is necessary.

  **Note 2**: This mod does not support any other mods so this installer does not contain Ninja
  or any other mods.
`>}}

{{< includecode desc="Gothic - Quentins Bande" path="assets/install_scripts/quentin.yaml" lang="yaml">}}

{{< includecode desc="Gothic - Revised Edition" path="assets/install_scripts/revised.yaml" lang="yaml" expl=`
  This includes Riisis texture mix, higher resolution cutscenes and a D3D11 renderer as well as
  Ninja for mod support. If you find the texture pack to be too different from the original for your
  taste, another popular choice is a [pack by Artemiano](https://disk.yandex.ru/d/5zhGf6G86dcRkA) which is closer to vanilla.
`>}}

{{< includecode desc="Gothic II - Vanilla with patches and Union mod" path="assets/install_scripts/g2_vanilla.yaml" lang="yaml">}}

{{< includecode desc="Gothic II - Generic extension/conversion mod" path="assets/install_scripts/g2.yaml" lang="yaml" expl=`
  The mod executable must be downloaded manually and provided to the installer at install time.
`>}}

{{< includecode desc="Gothic II - Die Bruderschaft" path="assets/install_scripts/bruderschaft.yaml" lang="yaml">}}

{{< includecode desc="Gothic II - Dirty Swamp" path="assets/install_scripts/swamp.yaml" lang="yaml">}}

{{< includecode desc="Gothic II - Gold Remaster" path="assets/install_scripts/remaster.yaml" lang="yaml" expl=`
  This includes the MiniMod balancing mod in the German version. Note that after the installation of the mod itself,
  the normalmaps and the MiniMod the game launcher will open up and needs to be exited manually for the installation
  to proceed and conclude successfully.
`>}}

{{< includecode desc="Gothic II - Legend of Ahssûn" path="assets/install_scripts/ahssun.yaml" lang="yaml">}}

{{< includecode desc="Gothic II: L'Hiver - English Edition" path="assets/install_scripts/hiver_english.yaml" lang="yaml" expl=`
  **Important**: This installation cannot be fully automated, unfortunately, so this script gets you only
  most of the way there. These are the installation steps:

  1. Run this script with Lutris as normal
  2. Start the game with "GothicStarter.exe" once to have the game create the necessary Union files. Starting a new game is not needed.
  3. Run the mod installer manually from within Lutris, overwrite files when prompted.
  4. (Optional) Modify "Gothic.ini" as you wish and/or disable preinstalled mods by removing the respective file from the "Data" directory.

  **Note**: This script includes German translations of in-game text. If you don't want this, remove the relevant sections from
  the script. You can then also omit the Playerkit and start the game with "Gothic2.exe".
`>}}

{{< includecode desc="Gothic II: L'Hiver - German Edition" path="assets/install_scripts/hiver_german.yaml" lang="yaml">}}

{{< includecode desc="Gothic II: Original L'Hiver Edition - German Version" path="assets/install_scripts/hiver_original.yaml" lang="yaml">}}

{{< includecode desc="Gothic II - Nostalgic mod" path="assets/install_scripts/nostalgic.yaml" lang="yaml">}}

{{< includecode desc="Gothic II - Odyssee" path="assets/install_scripts/odyssee.yaml" lang="yaml" expl=`
  **NOTE**: The mod includes Union by default so all Unions plugins can be used without further steps.
`>}}

{{< includecode desc="Gothic II: Returning - New Balance" path="assets/install_scripts/returning.yaml" lang="yaml">}}

{{< includecode desc="Functional mods snippet" path="assets/install_scripts/functional_mods.yaml" lang="yaml" expl=`
  This snippet contains a selection of my favorite functional mods.
  To use it add it to the appropriate sections in the installation scripts.
  Note that YAML is indentation-sensitive so make sure you get that right.
`>}}

## Playing on the Steam Deck

### Launcher

How you start the games depends on how you installed the game. I
recommend the Lutris route with custom install scripts in which case you need
to add the game(s) to your library as a non-Steam game.

The easiest way to achieve this is by using [BoilR](https://flathub.org/apps/io.github.philipk.boilr).
You simply install it, open it up, select the game(s) you want to import
(which usually happens automatically) and hit the "import" button.
This adds the game as non-Steam game in your Steam library and enters `flatpak` as
target command to run with specific startup arguments. This way the whole command looks
something like this:
```bash
flatpak run net.lutris.Lutris lutris:rungame/gothic-ii-gold-edition
```
If you're like me, you can then spend some time to add proper artworks, of course.

***Note**: Adding two instances of the same game (e.g. with different mods installed)
will not work out of the box with this method because BoilR uses the GOG-internal game ID
to create a run command which is then executed by Steam. To work around this you need to
change this identifier. This can either be done when using an installation script (
edit the value of `game_slug` at the top of the YAML file) or after installation. Just
select the game, click "Configure" in the context menu (right-click menu), go down to the
"Identifier", click the change button and enter something else. Don't forget to hit "Apply"
when you're done.*

If you installed a mod, you may find yourself facing a launcher in which you need to select
the game (or mod) again to actually start. Luckily there's a solution for this. You need
to determine the name of the game or mod you're playing from the base name of the `.ini`
file that's used. This is located in the `system` subfolder and may be named something like
`NostalgicEdition.ini` for the Nostalgic mod. Once you have this you can can open up
Lutris, right-click on the game, select "Configure", and in the "Game options" tab you
add the following to the "Arguments" field:

```
-game:$MOD_NAME -start
```

So for the Nostalgic mod this would become `-game:NostalgicEdition -start`. This will start
the selected game or mod directly. You may still encounter the mod starter after starting
the game but it will automatically disappear again.

### Controls

#### zGamePad

> ***NOTE**: As of October 2024 this mod doesn't work properly out of the box. I don't know yet
> why that is but somewhere between the in-game keybindings, Steam Input and zGamePad something
> happens so that button presses will trigger different reactions (e.g. jumping and opening
> the character menu) at the same time. I haven't found a way to fix this yet but I may at some
> point.*

In order to run the games smoothly on the Steam Deck or on a regular device with a gamepad
you'll want to install the "zGamePad" mod (see [above](#recommended-functional-mods)).
If you're playing via Steam, it's of course possible to map the inputs yourself which
gets you most of the way to what zGamePad provides out of the box. I would
not recommend this, though, because it's a *lot* of hassle and still doesn't provide
some of the features the mod does, like quick access menus, seamless transition between
strafing and forward movement (which is *very* nice to have) and situational keybindings
(i.e. keybindings that depend on whether you are in a menu, in combat etc.).

Installation of the mod can be achieved by copying the following snippet into an installation script:

{{< includecode desc="Controller snippet" path="assets/install_scripts/controller_snippet.yaml" lang="yaml" >}}

Alternatively, you can download and run the installer manually. Note that
this mod requires the Union mod.

Now you need to enter game mode and select your newly added game. In
order for zGamePad to be utilized you need to select "Gamepad with Mouse Trackpad"
as input method. You can configure zGamePad from the menu by selecting
"Settings" and "Union & Plugins".

If you installed via Steam directly and use the Workshop version of the mod,
the zGamePad controller scheme should be preselected and you're good to go
out of the box.

I recommend familiarizing yourself with
the controls (if you're not already familiar) and then switching the control
hints off in the menu since I find them to be pretty jarring (YMMV of course).

All the basic game functionality is covered by the gamepad inputs, however, if
you installed mods that require additional inputs (like sprinting, torches,
the item finder etc.) these need to be addressed separately. On the Steam
Deck this is easily done by just using the back grip buttons. If you're
using a regular gamepad, you'll need to consult the documentation for the mod.

#### Manual Mapping

If you're not using zGamePad (see above) you'll need to map the keys manually. Thankfully
Steam Input is very powerful so it's actually doable (for the most part). I won't tell you
what exactly you need to map where because it comes down to preference but I will give you
an overview about the mappings I have in place. Note that some buttons are bound to two
keys so you can perform different actions, e.g. depending on whether you have a weapon drawn.

|In-game keybinding | Steam Deck button | Function|
|-------------------|-------------------|---------|
|Right mouse button | B & Left trigger|"back" button, quicklooting, blocking |
|Left mouse button | Right trigger |used for fighting|
|Alt | X |jump and drop item from inventory|
|Ctrl | A |interact, in Gothic 1 I have a dual input of Ctrl and Return bound to this because Ctrl doesn't work to choose menu items or dialog options|
|Space | Y |draw (and change) weapon|
|Delete | Left bumper |strike to the left and strafe left|
|PageDown | Right bumper |strike to the right and strafe right|
|Start | Esc|
|Left stick press | F5 |quicksave|
|Right stick press | F9 |quickload|

You may need to ensure that the in-game keybindings correspond to the desired actions from
within the in-game settings menu.
For fighting I only use the triggers and bumpers because it suits me best. A few other
things to note:

* The right stick serves as mouse, the left stick is for running forward, backward and
strafing left and right. This makes movement fairly intuitive.
* The DPad is bound to the arrow keys. When I touch and hold the right stick an action
layer is activated that changes the DPad bindings to Tab, N, B and T for inventory, quest log,
character sheet and torch (see BetterTorches mod). That way I can use the DPad to select menu
items and dialog options as well as navigate trade windows but also have a way to access
inventory etc.
* The left trackpad is a quick menu bound to the number keys from 1 to 0.
* Back grip buttons are bound to sprinting, sneaking, item locator and such things.

This setup is quite satisfactory for my personal playstyle with Gothic 2. Since the controls
of Gothic 1 are a bit more wonky (combat system and trading) a few adjustements need to be made
there. I experimented with subcommands for Ctrl + arrow key for fighting but that always had
drawbacks so I just went with pressing right trigger + DPad button. Trading in Gothic 1 is a
mess and I didn't even bother to figure out a good solution to keybindings here, I just accepted
that it was weird.

### Performance

As you'd expect the vanilla game can basically
be run on a potato. With the D3D11 renderer, however, the hardware demand is
actually somewhat noticeable and you need a decent GPU to handle the load.
The Steam Deck is mostly up to the challenge, I did notice a bit of choppiness
in certain locations in Gothic II (so far in Khorinis and the castle in the
Valley of the Mines). You're free to tinker with the settings to improve
performance, of course.
For extra battery life I recommend capping the FPS to
40 and setting TDP to 7 which also helps with frame pacing.

When playing more demanding mods like the various L'Hiver editions the TDP
restriction won't be playable so I recommend not turning that on.

**NOTE**: On several occasions I noticed that after playing a while (a couple
of hours, I think) that the framerate tanked and wouldn't go up again. This
only occurred when playing with the renderer. Restarting the game resolved the
issue and unfortunately I haven't been able to pin down the issue yet.

Now all that's left to do is start the game and enjoy!
