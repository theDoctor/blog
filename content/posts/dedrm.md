---
title: "How To: Remove DRM from eBooks"
date: 2022-08-20T14:50:16+02:00
tags: ["How To", "Privacy"]
aliases:
  - "/posts/howto/dedrm"
---

A while ago I bought an eBook reader. I figured it's a smart investment because
the shelves at my place are almost full. Also, this would let me carry my whole
library with me at all times so I have to carry less. Buying new books should
also be easier because I don't have to order them and wait but can just download
them instantly. What's not to like? Sure, there's something to be said for
holding an actual paperback in your hands but I think the benefits outweigh the
disadvantages, at least for me.

So I got a used one off the internet and started buying and downloading stuff.
What I didn't realize back then was the fact that many eBooks come with DRM. A
form of encryption provided, in my case, by Adobe which ties the eBooks in
question to specific software to download and consume the book. I hate DRM in
almost all its forms. It's hard to overstate how much. In this case, Adobe needs
me to make an account and either login to it on my eBook reader and download
everything directly to it or install Adobe Digital Editions (ADE) on my PC, download
the book and read it within this software. 

Problem number one: I try to keep my eBook reader off the network for various
reasons. Also, it's a device meant for displaying text. Web browsing and
shopping is a tedious exercise at best.

Problem number two: ADE doesn't have a Linux versions. Of course it doesn't.
Have you ever seen an Adobe product on Linux without using Wine? Me neither. 

So what's the solution? Obviously we need to peel away the DRM from our books in
order to obtain and use them as we see fit. Please note that this is not (or
should not be) illegal as I buy my books regularly and don't distribute them
further.

It took me a long time to find a viable solution to the problem. Reportedly,
there's a plugin for [Calibre](https://calibre-ebook.com/) which can do this but
either it actually can't or I'm too stupid to make it work. Instead I came
across [this](https://github.com/bcliang/docker-libgourou/), thanks to a pointer
from a friendly fellow Fosstodonian.
It provides a docker container with a libre implementation of the Adept protocol
which can use existing Adobe credentials (still need those unfortunately) to
download a book and strip away its DRM so it can henceforth be used as a regular
book without any shenanigans.

Here's how to do it:

1. Get the container with `docker pull bcliang/docker-libgourou:latest`
2. Run it once with `docker run -v $(pwd):/home/libgourou/files -it --entrypoint
  /bin/bash bcliang/docker-libgourou`
3. Login with your Adobe credentials in the now open shell: `adept_activate -u
  <Username>`
4. Enter password when prompted
5. The config files will now be saved to `/home/libgourou/.adept`. Move them to a
  mounted volume (like `/home/libgourou/files`) so you can reuse them later.
6. Now you can move your `.acsm` file (the Adobe DRM file format) to the mounted
  `files` folder (if you hadn't already) and proceed with it.
7. To download the actual `.epub` do `acsm_download -f <Name of .acsm>`. This
  will place the downloaded file in `/home/libgourou`
8. Now strip DRM with `adept_remove -f <Name of .epub>`.
9. Finally, the stripped `.epub` needs to be moved to a mounted folder to be
  persistent, e.g. the `/home/libgourou/files` one.

That's basically it. You may note that this is pretty finicky and a bit messy.
The owner of the project wrote a bash script to automate all of this and make it
more ergonomic but either due to my stupidity or issues with the script itself,
I was never able to make this work. So what I do is the following:

- Define a shell alias for opening the docker container:
`alias drm="docker run -v {path to credentials}:/home/libgourou/.adept:Z -v {path to saved .acsm files}:/home/libgourou/files:Z -it --entrypoint /bin/bash bcliang/docker-libgourou"`
- Proceed with steps 7 to 9 from above. Done.

Note: depending on how docker is set up on your system, you may need to run the
respective commands as root.

Another note: if you're like me and run Fedora (or some other system that has
SELinux rules for docker in place), you may need to configure your system such
that mounting persistent volumes to your container works. I've done this via
adding the `Z` options (see above).

This could probably be a bit more streamlined but I feel this would cost more
time than I'd save so I'm happy. After I discovered this I bought a bunch of
books and rediscovered my eBook reader and reading once again. A win, I think.
