---
title: "On Open Worlds in Gaming"
date: 2022-03-23T10:23:32+01:00
tags: ["Gaming"]
aliases:
  - "/posts/gaming/open_world"
  - "/posts/gaming/open-world"
  - "/posts/open_world"
---

So I have a bone to pick with how some games approach their world design. The
concept of an open world is not new and often provides gamers with an immersive
experience where you can go where you want and do as you please (within the
game's boundaries of course).

There are so many great examples for this of which I have played only a small
subset but the Assassin's Creed franchise comes to mind here. I love the
experience of a historic settings of real-world places to some of which I've
been personally. Especially part two and Syndicate stood out to me in that
department because both Renaissance Italy and Victorian London are beautiful
environments on screen.

However, building an open world just for the sake of it is a serious flaw in
game design and in this post I want to write about why that is.

You see, an open world comes at a cost. And I don't mean an increased demand for
computing resources. No, I'm talking about story and pacing. The inherent appeal
of an open world is to give more control to the player and let them do things in
a way, order and time they please. Also, such a world can usually be explored
freely by just moving around in it.

Here lies a potential problem: if the player gets to decide when to do what and
how, the developer can't. If the game has a dense and immersive story and
memorable characters but also features an open world, the pacing cannot be
guaranteed to be fast anymore. The player decides the pace so the developer has
to accommodate for that fact.

Gameplay mechanics and world design have to take into account that players will
spend a lot of time exploring and not doing any quests. An open world should
thus not be empty or sparse to be interesting. It needs to reward exploration,
contain interesting sights, have chance encounters, things like that. A
believable atmosphere already goes a long way in keeping the player interested.

Thing is, some games just don't do this. They craft an intricate story line but
embed it into a world which is so vast and can be explored for hours and hours
that story events become rare interludes between long sessions of running around
and gathering resources and the like.

My favorite example for this is the Dragon Age franchise. The first
installment has brilliant storytelling, all the character variety I've come to
expect from BioWare games, a very detailed character development system, a
thoughtful combat system and it features a closed world. All things considered
it's just not very big but I'm fine with that.

On the other hand we have Dragon Age: Inquisition which has all the things part
one of the series had (with a dumbed-down character development) as well as
beautiful looks buuuut an open world! And a rather large one at that. 
In principle, this wouldn't be so bad but they just designed it quite poorly.
The world is large but mostly devoid of content. There are a few side quests,
sure, and lots of collecting quests to be had but by and large you just run from
point to point on your map and tick off boxes on your to-do list. Much of the
world is actually detached from the game's story and there's not really much
incentive to explore it all. In my first playthrough I didn't and there's not
really a downside to it, except if you're a completionist at heart.

What's even worse is that if you do explore the part of the world open to you at
a given time before continuing with the story, you're vastly overleveled for it.
That way, the game even disincentivizes doing that which I think is just a bad
design choice. All in all, the game suffers from serious pacing issues. The main
story is amazing but in between main quests there's just so much slog that you
have to be stout of heart to be able to finish it. Also, you have to bring time.
Lots of it.
The main story doesn't actually have that many quests to it, most of the game is
unnaturally stretched by the open world which blows up the time you need to
finish the game. As a matter of fact, I started the game four times or so but
was only able to finish it once. The first time. I think this is telling.
If the developers had chosen to not make this game an open world one (or did a
lot of things differently with the world they did make), I think it would have
been a much improved experience.

Of course this is just one example but there are other less problematic ones.
The Witcher 3. I love the game and it does many things right. The open world
seems like a good idea to me, mostly. Still, in order to explore it all, you
have to go from one question mark on your map to another and kill or collect
whatever's there. While this does not accumulate to be a majority of the game,
much of the world is littered with these rather pointless events. Every once
in a while you encounter something actually interesting in this process but most
of the time you don't. I'm not a fan of this.
What's more, if you do explore the world available to you before continuing the
main story, you're again overleveled. And this is even punished by the game
because if your level is more than five levels higher than the recommended one
for a given quest, you don't receive experience points. This leads to be
constantly stressing about which order to do things in so I don't miss out on
XP. Not a fan of this either.

Then there are games where the open world itself is the content. Sandbox games,
for example, that just drop you in some given setting and then leave you to it.
Things like No Man's Sky come to mind which boast the largest procedurally
generated world ever. Fun fact: when the thing was released people discovered
that this wasn't really all that much fun because it got tedious over time. Go
figure. As far as I know, after many patches and updates, the game is fun to
play now because changes were made.
If you don't provide much or any of a compelling story, it's even more important
to keep players happy by giving them things to do and find in your world.
Endless repetition, farming and crafting is usually not the answer to this.

Then there are some games that are kind of like sandbox games and also pretend
to have a story to follow. This has "Skyrim" written all over it. I know people
who spent hundreds of hours on it without ever finishing the story.
People seem to love this game but I couldn't bring myself to like it. It feels
too much like running from one point of interest on my map to the next and
looking at whatever's there, killing hundreds in the process. Also, I don't like
to be bombarded with tons of generic quests that lead me all over the known
world and have little rhyme or reason to them.

By this point you have probably guessed that I enjoy story-driven games the
most. It's the most enticing aspect of gaming to me although there are others of
course. And I don't like having that watered down by some haphazard open-world
design just because "that's what you do nowadays". Open worlds can be lovely,
breathtaking even but they have to be fit into the game's mechanics to make
sense. You have to make the world look alive, contain interesting objects or
characters, make the player explore it more, reward them for doing so, connect
the world itself to the story of the game (if present).

One game that has integrated the open world elegantly into the game itself is
Gothic (the original one). Its boundaries are set by a magic barrier within
which the main character is imprisoned. The prison is not large by today's
standards (the game is over 20 years old now) but it was an open world. There is
no built-in map, minimap or compass to help with orientation, you have to find
stuff by searching for it and following NPC's directions. This makes you pay
much more attention to landmarks and details in order to find your way. The game
also rewards exploration by items you will find scattered in the wild all over
the place but it doesn't give those to you for free. Exploration is dangerous,
especially in the beginning and if you go into the woods unprepared, you often
end up eaten by wolves. 

To this day, I know every path and tree in Gothic (and its first sequel) by
heart which is not something I can say about any other game. As soon as you have
a map to rely on and directions shown to you on a minimap, you stop trying to
find your way without them. Also, as worlds become larger and somewhat more
generic finding your way on your own becomes more difficult and even impossible.
This was proved by the same franchise with the third game of the series. The
world was too big for the player to find their way around easily without any
helpers. It tried to make this work just as in the previous two games but it
turned out worse.
Sure, some games let you disable these in-game helpers to have
a more traditional gaming experience and you don't *have* to use a map but in
most recent open-world games this would just lead to a lot of frustration, just
as it did in Gothic 3.

Of course I'm well aware that much of this is highly subjective. People are
different and so are their preferences. Things I find irritating might be
enjoyable for someone else and vice versa. Part of my stance here is also due to
the fact that I appreciate my time being wasted much less than I used to.
However, I still stand by the fact that open worlds should just be a means to
and end, not the end itself. And some game developers seem to understand that
better than others.
