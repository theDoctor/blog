---
title: "Degoogling my life"
date: 2022-03-08T10:09:35+01:00
tags: ["Software", "Privacy"]
aliases:
  - "/posts/privacy/degoogle"
---

Nowadays, I get the impression that most people realize how dependent we all
are on the tech giants. Those few companies that are worth more than some 
countries and which shaped the internet of today to the point that, without
them, most of it would simply break down.

Most people also seem to know, at least to some degree, that companies like
Google, Amazon and especially Facebook are not totally benign and do some
pretty shady stuff with people's data (to put it mildly).

At this point I seem to diverge from most people around me. 
I'm definitely not free of double-standards and hypocrisy, but I try to be
consistent in what I believe, what I say and what I do. After everything I've
learned about FAANG (Facebook, Amazon, Apple, Netflix, Google), 
how they run their businesses, how they violate the user's privacy and 
freedom and, often enough, tolerate hate, violence, antidemocratic 
speech and actions, how they put profit before everything else, 
I saw the need for action.

*Begin rant*

I won't go into full rant mode and find all the scandals and stories
that would be worth reading and refer to them here. I may do that in the 
future but for now let's just say: the information is readily available
and literally only a web search away.

It baffles me that people around me often make it sound like I'm
a conspiracy ideologist because of my stance in this matter which some 
consider to be extreme. I can't agree because the obvious difference here is
that nothing of this is conspiratorial. It's not even secret. The facts are
easy to find and verify. It is a conspiracy all right, but hidden in plain sight.

If you sit down with any device that connects to the internet, you'll be able
to find ample evidence of everything I wrote here (and much more) within
a few minutes.
Most people just can't seem to be bothered or don't seem to care.

And if I hear one more person give me the old "But I have nothing to hide!"
adage, I swear, I'm gonna punch them in the face, or at least wish that
I could.

My favorite response to this comes from Edward Snowden (remember him?):
> *"Arguing that you don't care about the right to privacy because you have 
> nothing to hide is no different than saying you don't care about free 
> speech because you have nothing to say."*

Enough said. 

*End rant*

The obvious place to start for any kind of action is my own life. It's the
only place, really, where I have a decent amount of control and can see
immediate effects. So this is where I started.

But how exactly should I go about this? There are a few thoughts to apply to a
situation like this but that are not exclusively valid here:
- You can't do everything at once
- Start with one thing, then go from there one thing at a time
- Start with something simple, then work your way up the food chain
- If possible, start with the item that has the biggest impact for maximum effect

This may seem obvious but these thoughts help me organize my thoughts and prioritizing
work to be done. I was well motivated but just unsure where to start.

Next up, assess the workload. It seemed like a safe bet to just get rid of everything
that ties me to the aforementioned companies. So I made a (mental) list of software 
and services provided by them to me:
- Google Search
- Google account for Android
- Google Maps
- Microsoft Windows
- Microsoft Office
- Facebook account
- WhatsApp
- Netflix account
- Amazon Prime

You may notice that this list is not nearly as long as it might have been. For example there are no social
media apps here, only that account I once made at Facebook. I presume most people use more
of these services and are thus more entangled in them. This, of course, makes a switch much more difficult.
Also, as we shall see, Google is far more embedded in my life than everything else which is why I gave the
article its name. You could say that Google is the final boss of this whole endeavor.

Another observation is that some things on this list are simply convenient, but not necessary, like Netflix
and Amazon Prime. I could just quit those and, eventually, I did. I wasn't using Facebook anymore anyway
so I "deactivated" my account. I'm quite sure that the data has not been deleted, regardless of whatever
it is Facebook claims because I only trust them as far as I can throw the sun.

Okay, looking good so far. Next up, we have things that have alternatives which
work just as well (or at least reasonably well) but actually don't abuse your
privacy. These would be Google Search, Maps, Microsoft Office and WhatsApp. For
searching there's a bunch of other search engines with various differences in
how they work but here's a short list:
- [Startpage](https://www.startpage.com/)
- [DuckDuckGo](https://duckduckgo.com/)
- [Qwant](https://www.qwant.com/)
- [Whoogle](https://github.com/benbusby/whoogle-search)
- [Searx](https://searx.me/)
- [Metager](https://metager.de/)

Personally, I use Startpage because it provides very good results (it uses Google's) sans the tracking. 
I know others find this unsuitable for a number of reasons. Do your own research and then
make a choice. There are plenty of alternatives to choose from.
Also note that Whoogle and Searx do not provide a central instance of the search engine but rather
provide the code so you can self-host them. You can either do that or find a public instance you can use.
In terms of convenience these are not ideal, though. I went with Searx for a while but I found it to
be more hassle than I was comfortable with.

For maps the obvious alternative would be OpenStreetMap. There are several ways to use it:
- The original [website](https://www.openstreetmap.org/)
- Third party sites, e.g. [Qwant Maps](https://www.qwant.com/maps)
- Desktop apps like [GNOME maps](https://wiki.gnome.org/Apps/Maps)
- Mobile apps like [OsmAnd](https://osmand.net/)

The biggest issue here is that these only provide map data and not traffic data
so they won't help you avoid traffic jams. I don't own a car so this isn't a
problem for me but for you it may be. Maybe there is already a tool out there
that can help with this, though. I've used all of these solutions in the past
but I got the most use out of OsmAnd, with it being a mobile app and all. Its
navigation works well, it works offline (which is a big deal for me) and it has
tons of features (most of which I've never touched).

For Office tools there also are plenty of alternatives, the most notable of which would be 
[LibreOffice](https://libreoffice.org). If you want something that looks more like the Microsoft version,
you could also go with [OnlyOffice](https://www.onlyoffice.com/). I haven't used it much but I like the
look of it. For cloud-based solutions there are things like [Collabora](https://www.collaboraoffice.com/)
but you would, of course, need a provider for that. Or self-host.
A popular solution would be to integrate an office suite with a [Nextcloud](https://nextcloud.com/)
instance, a free and open-source cloud software, and use that. There are professional providers for this
as well or you could host that yourself or have your employer do it. I know doing this yourself is
nontrivial but it beats relinquishing control of your sensitive data to companies that monetize it
and may do God-knows-what with it. At least in my book.
I mostly use LibreOffice when I have a need for office tasks which, interestingly, doesn't happen often
these days. It's fine for my use cases. Although I do have a self-hosted Nextcloud, I didn't install
a Collabora server. At the end of the day, it just wouldn't be of much use and just eat up resources.

Next up we have WhatsApp. I hear that this is not actually such a big deal in the US but where I'm from
(guess where that might be...) **everyone** has WhatsApp. This has led to it being an integral part
of society where people, social groups, companies, dance clubs, whatever, coordinate and plan via WhatsApp.
This is terrible because, one, it violates the privacy of everyone involved, even the people who don't even
have WhatsApp but who are in the contact list of someone who does, and, two, it ostracizes people who don't
have WhatsApp.
The biggest Problem here is the network effect which makes switching away from WhatsApp nontrivial. There
are alternatives though:
- [Signal](https://signal.org)
- [Threema](https://threema.ch/)
- [Matrix](https://matrix.org)
- [XMPP](https://xmpp.org/)

I'll say it right away: there is no "best" messenger, it depends on your use
case. For people used to WhatsApp, though, the first two entries in the list
should be very familiar and usable, except that they honor your privacy. I did
not include Telegram in this list because it isn't end-to-end encrypted by
default and (as far as I know) does not support this feature for group chats.
This is a deal breaker for me. If you just need something easy to use and not
worry about anything, these are solid choices.

Matrix and XMPP are actually messaging protocols you can, in principle, use with
any app that implements this protocol. The special thing about these is that
they don't require a central server to work, they are federated. Everyone can
self-host their own server and start using it. Of course you can also find
public servers to use and this is what I would recommend to newcomers.

I use Signal and Threema myself. Signal has had a big influx of users after
WhatsApp changed their terms of service for the worse. I had already been on
Signal by that time but many of my contacts weren't. Now, a sizable chunk of the
people I interact with also use this app which makes the experience so much
better. Threema is also nice but I only have it to accommodate a few people who
wouldn't install Signal. I wouldn't mind using this as my main messenger, it
just doesn't have the user base for this to make sense for me at the moment. My
favorite part about using Signal is [this](https://signal.org/bigbrother/): a
list of government requests for data on Signal users in connection to various
investigations. Each article includes the answer Signal gives each time. They
provide the time of account creation and the time when the user last connected
to Signal's servers (which for a reasonably active user happens every day).
That's it. That's all they can provide because it's all they have. Signal has been
setting the standard in messaging security for years and as long as they do I
feel safe here.

So far, I have made quite some headway into making my life more private.
Unfortunately, the two biggest chunks still remain: the operating systems
running on my PC and my phone. Since I've never been an Apple person, I don't
have to deal with any nonsense from them. Instead I have the nonsense of
Google's Android and Microsoft Windows to contend with, but one step at a time.

Starting with my desktop OS is, by far, the easier task so that's where I started. I got curious
about Linux some time in 2018 and started experimenting with it. I really enjoyed how easy it was to
try it out since you can just slap it onto a spare USB stick and boot from it without the need to make
any changes on your actual machine. I also loved the feeling of control and all the choices I was given
all of a sudden. Sure, it also was a bit overwhelming because there was so much I didn't understand.
I suppose most people don't know and don't care how their computer works as long as it does, me included.
But Linux made me do just that and also made me want to learn more. And I did. 
It took some time and a lot of trying but at some point I decided to dual-boot Windows and Linux on my
personal computer. After I had figured out how to play games on Linux as well, I got rid of Windows entirely.
I remember how great this felt.

I proceeded to wipe all the desktop and laptop machines in my household and install Linux on them. There
were a couple of iterations of distros but I settled on something in the end and never looked back. Bye bye,
Microsoft.

Now for the final boss: Google. You see, the mobile phone market is not terribly diverse. You either own
an iPhone or you don't. If you don't, your phone runs some variant of Android which Google makes. That's it.
Every other choice has been destroyed or never took off in the first place.
There are some niche alternatives but these are neither easily accessible nor well-known so for the average
end-user it's the choice between two massive companies, Apple and Google. Both suck. Dammit.

This is even more of a problem if you consider how pervasive smartphone usage
has become. Basically everyone has one, many have more than one and if, by some
strange twist of fate, you don't have one, you're some sort of prehistoric relic
which is banned from society. Have you ever stopped to realize how dependent on
phone we are nowadays? We don't only rely on them for convenience but also for
basic functionality that is either completely inaccessible or quite cumbersome
to achieve otherwise. Many services, ranging from bonus points in supermarkets
over buying public transport tickets to online banking *require* you to install
an app. And not any app, the provider's app and all you can do is hope it's not
terrible.

There's some pretty heavy power imbalance going on here, isn't it? And Google
makes use of it. They had already been collecting data about us but with the
advent of the smartphone everything changed. Now we have phones running a Google
operating system, browse with Google's Chrome browser, navigate with Google
maps, translate with Google Translate, talk via Google Hangouts, save out data
in Google Cloud, write mails with Gmail and so on. All these apps are present on
our phones without the possibility to delete them (unless you're a tech wizard
that is).

I always laugh a bitter laugh at the people who believe the COVID-19 pandemic
was just a plan to get us all vaccinated so they could insert a microchip into
our bloodstreams and track us. I know the people who think this are not in the
habit of using their brain for its intended purpose, offense intended, but
what's hilarious about this (and all other conspiracy ideologies that go in that
direction) is that there's just no need. We're all wired already. We carry with
us devices that are able to spy on our every query, thought, conversation, our
location at all times, who we meet, where we live and work and so. Much. More.
And we do this willingly. No one forced these devices on us, we actually pay for
them. Quite a lot even. What's more, not only Google has this data, the US
government can access them whenever they please without notifying anyone. And
Google has to comply. By law. Isn't that great? What's even more, your data is
not only collected but stored indefinitely because who knows what it might be
useful for in the future. If all of this doesn't make you uneasy, I don't know
what will.

For me this is ample reason to try and escape
but the sad truth is: you can't. Not really. The smartphone is the future, there's nothing for it.
I didn't get one until 2014 or 2015, I had been living off cast-off devices from others but I finally caved
and got a Samsung Galaxy S4 Mini. Ironically, I wanted to use it for WhatsApp of all things. 

These devices are, of course, even worse because they not only contain all the means of tracking that Google
put on there, they also contain another layer of apps and tracking provided by Samsung. Shocking, I know.

So what to do? Get rid of all that crap and install a different OS. You see, Android, like Chrome, is based
on an open-source project called the Android open source project (AOSP). This is freely available and
can be forked and used as-is by other people.

Consequently, there is a community of people taking this and making custom operating systems for phones (ROMs)
out of it which don't (necessarily) include all of Google's stuff.

I had read about the possibility of putting a custom ROM on my phone so I could get rid of Google for good,
the thing is this is not an easy process. Installing a different OS on a smartphone is much more involved
than doing this for a PC and there's more that can go wrong. If you're really unlucky you can brick your
phone permanently.

What to do then? I decided to go ahead and buy a used phone on Ebay (the same model I already had) and use
it as a testing device. I had chosen to install [LineageOS](https://lineageos.org/), the most popular
ROM out there which is available for many devices (including mine). This requires to:
- Unlock your bootloader (what is that again?)
- Install a custom recovery (a what now?)
- Install LineageOS from recovery

For this you need to use `fastboot` and `adb`. I'd never heard of any of these terms but fortunately there
are good guides for each device on the Lineage wiki page. I installed what I needed to (readily available
for Linux, who would've thought?) and just followed instructions. I admit, I was confused quite a bit and
had to start over a couple times but in the end I got it to work.
I felt like I had just achieved something tremendous and I felt quite smug. I had beaten the final boss
and was a big step closer to my vision of being Google-free and in control of my data. Awesome!

The next thing I did after this was to delete my Google account. I absolutely relished it. It
hadn't accumulated a lot of data (compared to what could have been) because I had never used most 
of Google's apps or services (things like Gmail of Hangouts). I felt like I was giving them the virtual 
finger. I'm sure Google could hardly care less about me but that doesn't matter to me.

What was left to do? Apply the same to the other phones in my household of course! I only managed to 
convert that pesky Xiaomi phone recently because Xiaomi makes you jump through a bunch of extra hoops to
unlock the bootloader. Good riddance I say.

After that was done, what else could I do? I had gotten rid of pretty much every service or device that
directly connected to any FAANG company by now except one: my TV.

I only got this after I started to degoogle everything and only afterwards did I find out how chatty these
things are. They constantly phone home, especially the Samsung device I have. I already cut the power
connection whenever I'm not using it but that's clearly not enough. What to do then? I couldn't
really wipe the thing and install another OS on it (I think) so I had to find another solution.

The solution presented itself in the guise of network-wide DNS blocking. Basically, you set up a device
that receives all DNS requests made from within your home network and filter them based on a set of rules.
Anything that doesn't fulfill these doesn't get past the device and is never sent out. I liked the concept
and got my first Raspberry Pi to see it through. I installed [PiHole](https://pi-hole.net/), which is
software designed for just this purpose, and set it up with appropriate rules and blocklists.
Now my TV screams into the void a lot when it's on but it won't get an answer. Serves it right.

Another upside of this approach is that I can get rid of a lot of spurious connections to Google and Facebook
servers that no one knowingly initiated but which happen anyway, things like embedded resources on websites,
cookies, tracking pixels and the like. Unfortunately, this also leads to breakage from time to time
because many websites rely on stuff like this to function properly. This is another symptom of how
dependent on companies like Google we all are but that's just how it is.

So that's my story, at least so far. The journey doesn't end here and maybe never will. It feels like
a constant battle sometimes but I feel like I've reached a place where I can be (mostly) happy with
my setup and more importantly: I have reasonable peace of mind. I value my freedom of choice and freedom
from tracking. I don't want to pay with my data for things I don't use or don't like and have no control
over. Granted, I also don't often pay with money for the software I use but I'm getting better at that.
