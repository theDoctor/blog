---
title: "How To: Disable Origin Overlay for Steam Games"
date: 2022-03-23T09:23:41+01:00
tags: ["Gaming", "Linux", "How To"]
aliases:
  - "/posts/gaming/origin_overlay"
  - "/posts/gaming/origin-overlay"
  - "/posts/origin_overlay"
---

The Origin launcher is a blight upon the galaxy. Period. It just sucks.
Unfortunately the company making it and forcing it on people happens to be the
publisher of some of my favorite game franchises, namely all of the current
Star Wars games, Mass Effect and Dragon Age. I'm not willing to refrain from
playing any of them because of ideological reasons.

An added difficulty is that Origin doesn't play too nicely with Linux. I have
difficulty entering my passwords and having them recognized properly, it's
fairly slow to load, it starts multiple Origin instances of processes when I
start a game for some reason and it tries to force yet another overlay on me
that results in stutter, window minimization and sometimes even crashes. I'm not
a fan of overlays in general, they break my immersion, but Steam lets me disable
theirs easily enough at least.

In principle, the Origin overlay can be disabled from within the application but
if you run an EA game via Steam that's not possible as this option is not
exposed to the end user.

Luckily, there's a fix for this:
- Locate the folder holding the Wine prefix of your game. If you're running
  Steam natively, this is in `$HOME/.steam/steam/steamapps/compatdata/[Game
  ID]/pfx`.
  The Game ID can be determined in various ways, e.g. by looking it up [here](https://steamdb.info/apps/).
- Delete or rename the following file: `C:\Program Files
  (x86)\Origin\igo64.dll`which is located within the prefix of the game.

Done. Unfortunately, you have to repeat this process for every game but since
this is Linux you can also just write a script to automate this.

Happy, Origin-overlay-free gaming!
