---
title: "About"
hideMeta: true
ShowBreadCrumbs: false
searchHidden: true
layout: "single"
disableReply: true
aliases:
  - "/de"
---

I'm a cloud engineer, former software developer, former computational chemist, free software
enthusiast and small-time Linux nerd.

I started my professional career as a chemist, moved over to computational chemistry because I
discovered the fun of high performance computing, started learning my first programming language,
realized how much I enjoy it and finally decided to make it my full-time career. Let me assure you,
it's a slippery slope. I'll give the word one I've found the bottom of it all.

Apart from that, I'm a huge Star Wars geek, I love the Tolkien univserse (the stuff that Amazon
pumped out not so much, though) and like to spend my free time, such as it is, with hopelessly
complex board games, a good book or a PC game.
