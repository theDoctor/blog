---
title: "Uses"
hideMeta: true
ShowBreadCrumbs: false
ShowToc: true
TocOpen: true
searchHidden: true
layout: "single"
disableReply: true
---

Here is an overview of the technology I use on a regular basis. Feel free to
contact me if you have questions or would like a recommendation.

# Hardware

## Laptop
Work machine for development, Dell Latitude 5450
- CPU: 13th Gen Intel i7-1370P
- GPU: Intel Raptor Lake-P
- RAM: 32 GB DDR4
- Storage: 500 GB NVMe
- Screen resolution: 1920 x 1080
- OS: Fedora 40

## Desktop PC
Custom built PC, mostly for gaming, which is not really in use anymore.
- CPU: Intel i5-4690K
- GPU: NVIDIA GeForce GTX 970
- RAM: 8 GB DDR3
- Storage: 120 GB SSD + 2 TB HDD
- Monitor: Asus, 1080p
- OS: Fedora 37

## Server
I currently run one Raspberry Pi device in my local network which is used as a digital picture frame. It runs on
Raspberry Pi OS which is basically Debian tailored to these devices.
- Raspberry Pi 3B+, 1 GB RAM

My home serve is an HP Elitedesk 800 G3 SFF with:
- CPU: Intel i5-6500
- RAM: 16 GB DDR4
- Storage: 250 GB NVMe + 2x 4 TB HDD
- OS: Ubuntu Server 22.04

The server serves in part as a NAS and the data ist stored
on two SeaGate IronWolf Pro 4 TB drives running in a zfs mirror.

## Steam Deck
Gaming machine

For specs, see [here](https://www.steamdeck.com/en/tech/)

## Peripherals
- Gamepad: XBox One wireless controller
- Work Headset: Sony WH-1000XM4
- Keyboard: Keychron K8, TKL, QWERTZ Layout

## Phone
I own a Google Pixel 7a with GrapheneOS as operating systems. I realize that
buying a Google phone is somewhat ironic for a person like me but GrapheneOS is
only available for Pixel devices so I decided to accept this tradeoff.

# Software
Here is a non-comprehensive list of the major pieces of software I use, apart from my OS.

## Desktop & Laptop
- [Alacritty](https://github.com/alacritty/alacritty): Terminal, a Linux minimalist's best friend
- [helix](https://github.com/helix-editor/helix): Coding, text editing and note keeping
- [zed](https://github.com/zed-industries/zed): Code editor, currently I build from source because application is not officially distributed to Linux yet
- [Firefox](https://www.mozilla.org/de/firefox/new/): 'nuff said
    - uBlock origin: Ad blocking
- [Ungoogled Chromium](https://github.com/Eloston/ungoogled-chromium): When I need a Chromium-based browser for some reason
- [LibreOffice](https://www.libreoffice.org/): Office suite

## Server
- [Immich](https://github.com/immich-app/immich): Image backup and management
- [Mealie](https://github.com/mealie-recipes/mealie): Recipe management
- [Jellyfin](https://github.com/jellyfin/jellyfin): Media streaming and management
- [AdGuard Home](https://github.com/AdguardTeam/AdGuardHome): DNS request filtering
- [Uptime Kuma](https://github.com/louislam/uptime-kuma): Uptime monitoring of my services
- [Nginx proxy manager](https://nginxproxymanager.com/): Reverse proxy management for my home network (including SSL certificates)

## Mobile
- [F-Droid](https://f-droid.org/): App store for FOSS-only apps
- [Signal](https://signal.org/): Instant messaging
- [Threema](https://threema.ch/): Instant messaging
- [Tusky](https://tusky.app/): Mastodon client
- [NewPipe](https://newpipe.net/): Privacy-friendly YouTube frontend
- [Proton Suite](https://protonapps.com/): Mail, calendar, cloud storage, VPN, password manager
- [OsmAnd](https://osmand.net/): Navigation. I use the F-Droid version, it allows for unlimited map downloads
- [AntennaPod](https://antennapod.org/): Podcast app
- [Nextcloud](https://nextcloud.com/clients/): Client for my Nextcloud server
- [KDE Connect](https://f-droid.org/packages/org.kde.kdeconnect_tp/): Share
  clipboard, files and more between phone and desktop PCs

# Services
- [Proton](https://proton.me/): Provider for Mail, calendar, password manager
- [Hetzner](https://www.hetzner.com/storage/storage-share): Managed Nextcloud provider
- [Porkbun](https://porkbun.com/https://porkbun.com/): Domain registrar
- [Codeberg](https://codeberg.org): Code hosting including this website
- [GitHub](https://github.com): Collaboration
- [Tailscale](https://tailscale.com): VPN provider
